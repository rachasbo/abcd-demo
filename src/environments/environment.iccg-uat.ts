export const environment = {
    title: 'ICCG (UAT)',
    appVersion: '1.0',
    production: true,
    uri: "https://icmg-dev.itmx.com/",
    api: "https://api-dev.itmx.com",
    isICMG: false
};
