export const environment = {
    title: 'ICMG (Prod)',
    appVersion: '1.0',
    production: true,
    uri: "https://icmg.itmx.com/",
    api: "https://api.itmx.com",
    isICMG: true
};
