import { SharedModule } from 'src/app/shared/shared.module';
// import { CommonLabelComponent } from './shared/components/common-label/common-label.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialog, MatIconModule, MatButtonModule, MatFormFieldModule, MatDatepickerModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { BsDropdownModule } from 'ngx-bootstrap';

import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { MatBadgeModule } from '@angular/material';


// ##### Initial MSAL #####
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
//import { MsalModule, MsalInterceptor } from "@azure/msal-angular";
import { HomeComponent, DialogWarning } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ChangePwdComponent } from './pages/change-pwd/change-pwd.component';
import { HttpServiceHelper } from './shared/helper/HttpServiceHelper';
import { environment } from 'src/environments/environment';
import { DialogErrorComponent } from './shared/components/dialog-error/dialog-error.component';
import { DialogSessionTimeoutComponent } from './shared/components/dialog-session-timeout/dialog-session-timeout.component';
import { DialogSuccessComponent } from './shared/components/dialog-success/dialog-success.component';
import { DialogConfirmComponent } from './shared/components/dialog-confirm/dialog-confirm.component';
import { DialogDeleteComponent } from './shared/components/dialog-delete/dialog-delete.component';
import { DialogInformComponent } from './shared/components/dialog-inform/dialog-inform.component';
import { DialogWarningComponent } from './shared/components/dialog-warning/dialog-warning.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';

export function loggerCallback(logLevel, message, piiEnabled) {
  console.log("client logging" + message);
}
/*
export const protectedResourceMap: [string, string[]][] = [
  ['https://buildtodoservice.azurewebsites.net/api/todolist', ['api://a88bb933-319c-41b5-9f04-eff36d985612/access_as_user']],
  ['https://graph.microsoft.com/v1.0/me', ['user.read']]
];

const isIE = window.navigator.userAgent.indexOf("MSIE ") > -1 || window.navigator.userAgent.indexOf("Trident/") > -1;
*/
// #########################


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    ChangePwdComponent,
    DialogErrorComponent,
    DialogSessionTimeoutComponent,
    DialogSuccessComponent,
    DialogConfirmComponent,
    DialogDeleteComponent,
    DialogInformComponent,
    DialogWarningComponent,
    DialogWarning,
    // CommonLabelComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatBadgeModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    //BsDropdownModule.forRoot(),
    /*
    MsalModule.forRoot({
      clientID: environment.adClientID,
      authority: environment.adAuthority,
      validateAuthority: true,
      redirectUri: environment.uri,
      cacheLocation: "localStorage",
      storeAuthStateInCookie: isIE, // set to true for IE 11
      postLogoutRedirectUri: environment.uri,
      navigateToLoginRequestUrl: true,
      popUp: !isIE,
      consentScopes: environment.adScope,
      unprotectedResources: ["https://www.microsoft.com/en-us/"],
      protectedResourceMap: protectedResourceMap,
      logger: loggerCallback,
      correlationId: environment.adStateId,
      piiLoggingEnabled: true
    }),
    */
    MatDatepickerModule,
    NgxMatTimepickerModule,
    NgxMatDatetimePickerModule,
    SharedModule
  ],
  providers: [
    HttpServiceHelper,
    /*
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true
    },
    */
    { provide: LocationStrategy, useClass: PathLocationStrategy }
  ],
  entryComponents: [
    DialogErrorComponent,
    DialogSuccessComponent,
    DialogSessionTimeoutComponent,
    DialogConfirmComponent,
    DialogDeleteComponent,
    DialogInformComponent,
    DialogWarningComponent,
    DialogWarning],
  bootstrap: [AppComponent]
})
export class AppModule { }
