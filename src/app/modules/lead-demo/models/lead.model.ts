import { CoreModel } from './../../../shared/models/core.model';

export class LeadModel extends CoreModel {
    public leadID: number = 0;
    public leadCode: string = "";
    public contactCode: string = "";
    public contactName: string = "";
    public leadStatusCode: string = "";
    public leadStatusName: string = "";
    public leadCompany: string = "";
    public leadCreateDate: Date;
}
