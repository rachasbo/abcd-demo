import { MatIconModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { LeadDemoRoutingModule } from './lead-demo-routing.module';
import { LeadFormComponent } from './components/lead-form/lead-form.component';
import { LeadListComponent } from './components/lead-list/lead-list.component';
import { LeadViewComponent } from './components/lead-view/lead-view.component';

@NgModule({
  declarations: [LeadFormComponent, LeadListComponent, LeadViewComponent],
  imports: [
    CommonModule,
    LeadDemoRoutingModule,
    SharedModule
  ]
})
export class LeadDemoModule { }
