import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeadListComponent } from './components/lead-list/lead-list.component';
import { LeadViewComponent } from './components/lead-view/lead-view.component';
import { LeadFormComponent } from './components/lead-form/lead-form.component';
const routes: Routes = [
  {
    path: '',
    component: LeadListComponent
  },
  {
    path: 'create',
    component: LeadFormComponent
  },
  {
    path: ':id',
    component: LeadViewComponent
  },
  {
    path: ':id/edit',
    component: LeadFormComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeadDemoRoutingModule { }
