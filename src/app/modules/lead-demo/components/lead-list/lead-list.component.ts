import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { CoreComponent } from 'src/app/shared/components/core_component';

import { LeadModel } from './../../models/lead.model';

@Component({
  selector: 'app-lead-list',
  templateUrl: './lead-list.component.html',
  styleUrls: ['./lead-list.component.scss']
})
// export class LeadListComponent extends CoreComponent implements OnInit {
export class LeadListComponent implements OnInit {

  datas: LeadModel[] = []

  constructor(
    private router: Router
  ) {
    // super();
  }

  ngOnInit() {
    this.genSampleDatas();
  }

  private genSampleDatas() {
    for (var i=1; i<=15; i++) {
      var data = new LeadModel();
      data.deserialize({
        leadID: i,
        leadCode: "L19-" + ("000000" + i).slice(-6),
        contactCode: "Contact " + i,
        contactName: "Firstname Lastname " + i,
        //leadStatusCode: "warning",
        leadStatusCode: "#ff9900",
        leadStatusName: "Warm",
        leadCompany: "Company " + i,
        leadCreateDate: new Date()
      });
      this.datas.push(data);
    }
  }

  onCreate() {
    this.router.navigate(['lead-demo/create']);
  }

  onView(id: number) {
    this.router.navigate(['lead-demo', id]);
  }

}
