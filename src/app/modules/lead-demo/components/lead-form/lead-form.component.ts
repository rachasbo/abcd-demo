import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';

import { LeadModel } from '../../models/lead.model';

@Component({
  selector: 'app-lead-form',
  templateUrl: './lead-form.component.html',
  styleUrls: ['./lead-form.component.scss']
})
export class LeadFormComponent implements OnInit {

  data: LeadModel = new LeadModel();
  id: number = 0;

  form: FormGroup;

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private builder:FormBuilder
  ) {
    const params = this.activateRoute.snapshot.params;
    this.id = params.id;

    if (this.router.url != '/lead-demo/create') {
      this.genSampleData();
    }

    this.createFormData();
  }

  ngOnInit() {

  }

  genSampleData() {
    this.data = new LeadModel();
    this.data.deserialize({
      leadID: this.id,
      //leadCode: "L19-" + ("000000" + this.id).slice(-6),
      leadCode: "",
      contactCode: "Contact " + this.id,
      //contactName: "Firstname Lastname " + this.id,
      contactName: "",
      //leadStatusCode: "warm",
      leadStatusCode: "",
      leadStatusName: "Warm",
      leadCompany: "Company " + this.id
    })
  }

  private createFormData() {
    this.form = this.builder.group({
      //leadID: [this.data.leadID, Validators.required],
      leadCode: [this.data.leadCode, [Validators.required, Validators.pattern(/^L[0-9]{2}-[0-9]{6}$/)]],
      contactCode: [this.data.contactCode, Validators.required],
      contactName: [this.data.contactName, Validators.required],
      leadStatusCode: [this.data.leadStatusCode, this.customValidator],
      //leadStatusName: [this.data.leadStatusName, Validators.required],
      leadCompany: [this.data.leadCompany, Validators.required],
      data1: [[2, 3], Validators.required]
      //data1: [[1, 2, 3, 4], Validators.required]
    })
  }

  onSubmit() {
    this.form.get('leadCode').markAsTouched();
    this.form.get('contactCode').markAsTouched();
    this.form.get('contactName').markAsTouched();
    this.form.get('leadStatusCode').markAsTouched();
    //this.form.get('leadStatusName').markAsTouched();
    this.form.get('leadCompany').markAsTouched();
    this.form.get('data1').markAsTouched();

    //if (this.form.invalid) return;

    console.log(this.form.value);

    //this.router.navigate(['lead', this.id]);
  }

  customValidator(control: AbstractControl) {
    if (control.value == '') {
      return { required: true }
    }
  }

}
