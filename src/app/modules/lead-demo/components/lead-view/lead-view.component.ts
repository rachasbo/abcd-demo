import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LeadModel } from './../../models/lead.model';

@Component({
  selector: 'app-lead-view',
  templateUrl: './lead-view.component.html',
  styleUrls: ['./lead-view.component.scss']
})
export class LeadViewComponent implements OnInit {

  data: LeadModel;
  id: number = 0;

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {
    const params = this.activateRoute.snapshot.params;
    this.id = params.id;
  }

  ngOnInit() {
    this.genSampleData();
  }

  genSampleData() {
    this.data = new LeadModel();
    this.data.deserialize({
      leadID: this.id,
      leadCode: "L19-" + ("000000" + this.id).slice(-6),
      contactCode: "Contact " + this.id,
      contactName: "Firstname Lastname " + this.id,
      leadStatusCode: "warning",
      leadStatusName: "Warm",
      leadCompany: "Company " + this.id,
      leadCreateDate: new Date()
    })
  }

  onEdit() {
    this.router.navigate(['lead-demo', this.id, 'edit']);
  }

}
