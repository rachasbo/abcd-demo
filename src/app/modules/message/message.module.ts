import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { MessageRoutingModule } from './message-routing.module';
import { ClearingListComponent } from './components/clearing-list/clearing-list.component';
import { InClearingListComponent } from './components/in-clearing-list/in-clearing-list.component';
import { NonClearingListComponent } from './components/non-clearing-list/non-clearing-list.component';
import { OutClearingListComponent } from './components/out-clearing-list/out-clearing-list.component';

// import { MatIconModule } from '@angular/material';
import { MatFormFieldModule, MatBadgeModule ,MatAutocompleteModule, MatProgressSpinnerModule,MatSelectModule, MatOptionModule, MatCardModule, MatInputModule, MatIconModule, MatButtonModule, MatCheckboxModule,MatTableModule, MatDatepickerModule, MatNativeDateModule, MatRadioModule, MatChipsModule } from '@angular/material';

@NgModule({
  declarations: [
    ClearingListComponent,
    InClearingListComponent,
    NonClearingListComponent,
    OutClearingListComponent,
  ],
  imports: [
    CommonModule,
    MessageRoutingModule,
    SharedModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatChipsModule
  ]
})
export class MessageModule { }
