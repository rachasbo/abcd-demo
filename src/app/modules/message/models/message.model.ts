import { Files } from 'src/app/shared/models/upload-file.model';
import { CoreModel } from './../../../shared/models/core.model';

// export class MsgClearingModel extends CoreModel {
//     public id: number = 0;
//     public bankCode: string = "";
//     public dataType: string = "";
//     public processTime: Date;
//     public period: string = "";
//     public status: MsgStatus;
//     public file: MsgFile;
// }
export class MsgClearingModel extends CoreModel {
    public id: number = 0;
    public bankCode: string = "";
    public dataType: string = "";
    public createdAt: Date;
    public settlement: string = "";
    public status: MsgStatus;
    // public status: string;
    public file: Files;
    // public fileName: string = "";
}

export class MsgInClearingModel extends CoreModel {
    public id: number = 0;
    public bankCode: string = "";
    public dataType: string = "";
    public processTime: Date;
    public period: string = "";
    public status: MsgStatus;
    public file: Files;
}

export class MsgNonClearingModel extends CoreModel {
    public id: number = 0;
    public bankCode: string = "";
    public dataType: string = "";
    public file: Files;
    public txtFile: Files;
    // public fileName: string = "";
    public businessDate: Date;
    public clearingDate: Date;
}

export class MsgOutClearingModel extends CoreModel {
    public id: number = 0;
    public bankCode: string = "";
    public dataType: string = "";
    public reportStatus: MsgStatus;
    public icxpStatus: MsgStatus;
    // public clearingSession: string = "";
    public file: Files;
    public businessDate: Date;
    public clearingDate: Date;
}

export class MsgStatus extends CoreModel {
    // public code: string = "";
    public label: string = "";
    public color: string = "";
}

// export class MsgFile extends CoreModel {
//     public id: number = 0;
//     public name: string = "";
//     public url: string = "";
// }

export class MsgResponse extends CoreModel {
    public total: number = 0;
    public limit: number = 0;
    public page: number = 0;
    public pageNum: number = 0;
    public sort: string = "";
    public sortDirection: string = "";
}
export class MsgClearingResponse extends MsgResponse {
    // public items: Array<MsgClearingModel> = [];
    public items: MsgClearingModel[] = [];
}
export class MsgNonClearingResponse extends MsgResponse {
    // public items: Array<MsgNonClearingModel> = [];
    public items: MsgNonClearingModel[] = [];
}

export class MsgOutClearingResponse extends MsgResponse {
    // public items: Array<MsgOutClearingModel> = [];
    public items: MsgOutClearingModel[] = [];
}
