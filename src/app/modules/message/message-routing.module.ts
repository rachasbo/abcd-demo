import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClearingListComponent } from './components/clearing-list/clearing-list.component';
import { InClearingListComponent } from './components/in-clearing-list/in-clearing-list.component';
import { NonClearingListComponent } from './components/non-clearing-list/non-clearing-list.component';
import { OutClearingListComponent } from './components/out-clearing-list/out-clearing-list.component';

const routes: Routes = [
  {
    path: 'clearing',
    component: ClearingListComponent
  },
  {
    path: 'in-clearing',
    component: InClearingListComponent
  },
  {
    path: 'non-clearing',
    component: NonClearingListComponent
  },
  {
    path: 'out-clearing',
    component: OutClearingListComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageRoutingModule { }
