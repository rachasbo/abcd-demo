import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutClearingListComponent } from './out-clearing-list.component';

describe('OutClearingListComponent', () => {
  let component: OutClearingListComponent;
  let fixture: ComponentFixture<OutClearingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutClearingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutClearingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
