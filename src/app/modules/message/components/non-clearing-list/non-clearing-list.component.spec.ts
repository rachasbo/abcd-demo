import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonClearingListComponent } from './non-clearing-list.component';

describe('NonClearingListComponent', () => {
  let component: NonClearingListComponent;
  let fixture: ComponentFixture<NonClearingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonClearingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonClearingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
