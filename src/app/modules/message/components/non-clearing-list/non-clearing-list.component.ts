import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Service } from 'src/app/shared/service';
import { environment } from 'src/environments/environment';
// import { CoreComponent } from 'src/app/shared/components/core_component';

import { MsgClearingModel, MsgStatus, MsgNonClearingResponse } from './../../models/message.model';

@Component({
  selector: 'app-non-clearing-list',
  templateUrl: './non-clearing-list.component.html',
  styleUrls: ['./non-clearing-list.component.scss']
})
// export class NonClearingListComponent extends CoreComponent implements OnInit {
export class NonClearingListComponent implements OnInit {

  form: FormGroup;

  // datas: MsgClearingModel[] = [];

  response: MsgNonClearingResponse;

  isLoading: boolean = false;

  constructor(
    // private router: Router,
    private formBuilder: FormBuilder,
    private service: Service
  ) {
    
  }

  ngOnInit() {
    this.createFormData();
    
    // this.genSampleContent();
    this.loadContent();
  }

  private createFormData() {
    this.form = this.formBuilder.group({
      page: [1],
      limit: [0],
      bankCode: [''],
      dataType: [''],
      fileName: [''],
      clearingStart: [''],
      clearingEnd: ['']
    })
  }

  // private genSampleContent() {
  //   for (var i=1; i<=15; i++) {
  //     var data = new MsgClearingModel();
  //     data.deserialize({
  //       id: i,
  //       bankCode: "002",
  //       dataType: "Normal Out-Clearing",
  //       processTime: new Date(),
  //       period: "Same Day",
  //       status: new MsgStatus().deserialize({
  //         code: "upload",
  //         name: "Upload",
  //         color: "009427"
  //       }),
  //       file: new MsgFile().deserialize({
  //         id: i,
  //         name: "ICAS_20210324_002_0001_000030_NIS.zip",
  //         url: "ICAS_20210324_002_0001_000030_NIS.zip"
  //       })
  //     });
  //     this.datas.push(data);
  //   }
  // }

  private loadContent() {
    this.isLoading = true;

    // var body = {};
    var body = this.form.value;

    this.service.sendPostRequest("/v1/messages/non-clearing", body, this, function(main, response) {
      main.isLoading = false;

      console.log(response);

      if (response.data != null) {
        main.response = new MsgNonClearingResponse().deserialize(response.data);
        
        main.form.get('limit').setValue(main.response.limit);
      }
    }, true)
  }

  // onView(id: number) {
  //   this.router.navigate(['message', 'clearing', id]);
  // }

  public onValueUpdate(column, result) {
    // console.log(column, result);

    if (result) {
      if (result.value) {
        this.form.get(column).setValue(result.value);
      } else {
        this.form.get(column).setValue(result);
      }


      const regexDateFormat: RegExp = /[0-9]{4}-[0-9]{2}-[0-9]{2}/;
      const regexDateTimeFormat: RegExp = /[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}/;

      if (column=='createdStart') {
        if (regexDateFormat.test(this.form.get(column).value) && !regexDateTimeFormat.test(this.form.get(column).value)) {
          this.form.get(column).setValue(this.form.get(column).value + "T00:00:00");
        }
      } else if (column=='createdEnd') {
        if (regexDateFormat.test(this.form.get(column).value) && !regexDateTimeFormat.test(this.form.get(column).value)) {
          this.form.get(column).setValue(this.form.get(column).value + "T23:59:59");
        }
      } else if (column=='limit') {
        this.loadContent();
      }
    }
  }

  public onDownload(fileId: number) {
    this.service.sendDownloadRequest("/v1/files/" + fileId + "/download", this, function(main, response) {
      
    });
  }

  public onClear() {
    this.form.reset();

    this.form.get('page').setValue(1);
    this.loadContent();
  }

  public onSearch() {
    console.log(this.form.value);

    this.form.get('page').setValue(1);
    this.loadContent();
  }

  public onFirstPage() {
    if (this.response.page > 1) {
      this.form.get('page').setValue(1);
      this.loadContent();
    }
  }
  public onPrevPage() {
    if (this.response.page > 1) {
      this.form.get('page').setValue(this.response.page - 1);
      this.loadContent();
    }
  }
  public onNextPage() {
    if (this.response.page < this.response.pageNum) {
      this.form.get('page').setValue(this.response.page + 1);
      this.loadContent();
    }
  }
  public onLastPage() {
    if (this.response.page < this.response.pageNum) {
      this.form.get('page').setValue(this.response.pageNum);
      this.loadContent();
    }
  }

}
