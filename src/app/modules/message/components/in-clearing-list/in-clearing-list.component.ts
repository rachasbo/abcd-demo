import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Files } from 'src/app/shared/models/upload-file.model';
// import { CoreComponent } from 'src/app/shared/components/core_component';

import { MsgInClearingModel, MsgStatus } from './../../models/message.model';

@Component({
  selector: 'app-in-clearing-list',
  templateUrl: './in-clearing-list.component.html',
  styleUrls: ['./in-clearing-list.component.scss']
})
// export class InClearingListComponent extends CoreComponent implements OnInit {
export class InClearingListComponent implements OnInit {

  datas: MsgInClearingModel[] = []

  searchForm: FormGroup;

  constructor(
    private router: Router
  ) {
    // super();
  }

  ngOnInit() {
    this.genSampleDatas();
  }

  private genSampleDatas() {
    for (var i=1; i<=15; i++) {
      var data = new MsgInClearingModel();
      data.deserialize({
        id: i,
        bankCode: "002",
        dataType: "Normal Out-Clearing",
        processTime: new Date(),
        period: "Same Day",
        status: new MsgStatus().deserialize({
          code: "completed",
          name: "Completed",
          color: "009427"
        }),
        file: new Files().deserialize({
          fileId: i,
          fileName: "ICAS_20210324_002_0001_000030_NIS.zip"
        })
      });
      this.datas.push(data);
    }
  }

  // onView(id: number) {
  //   this.router.navigate(['message', 'in-clearing', id]);
  // }

  onClear() {

  }

  onSearch() {

  }

}
