import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InClearingListComponent } from './in-clearing-list.component';

describe('InClearingListComponent', () => {
  let component: InClearingListComponent;
  let fixture: ComponentFixture<InClearingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InClearingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InClearingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
