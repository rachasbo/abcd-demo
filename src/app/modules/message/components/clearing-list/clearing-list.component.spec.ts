import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearingListComponent } from './clearing-list.component';

describe('ClearingListComponent', () => {
  let component: ClearingListComponent;
  let fixture: ComponentFixture<ClearingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
