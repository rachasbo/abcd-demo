import { CoreModel } from 'src/app/shared/models/core.model';

/*
export class UserListModel extends CoreModel {
  public userCode: string="";
  public nameEn: string ="";
  public userEmail: string="";
  public rolesName: string="";
  public positionName: string="";
  public buName: string="";
  public teamName: string="";
  public updateDate: Date;
  public updateByName: string="";
  public status:boolean;
}

export class AccountDetailModel extends CoreModel {
  public accountId:number;
  public accountName: string="";
  public rolesName: string ="";
  public statusMember: string="";
  public statusPosition: string="";
  public statusTeam: string="";
  public isSales:boolean = false;
  public isBuo: boolean = false;
  public isDelete: boolean = false;
  public memberList: Array<number> = [];
  public positionList:  Array<number> = [];
  public roleList: Array<number> = [];
  public teamList:  Array<number> = [];
}

export class RoleInfoModel extends CoreModel{
  public roleId: number = 0;
}
export class PositionInfoModel extends CoreModel{
  public positionId: number = 0;
}
export class TeamInfoModel extends CoreModel{
  public teamId: number = 0;
}
export class MemberInfoModel extends CoreModel{
  public memberId: number = 0;
}


export class UserModel extends CoreModel {
  public userCode: string="";
  public userId:number =0;
  public userName:string="";
  public userEmail: string="";
  public oid: string ="";
  public firstNameEn: string="";
  public lastNameEn: string="";
  public firstNameTh: string="";
  public lastNameTh: string="";
  public nickName: string="";
  public telephone: string="";
  public positionId: number=0;
  public positionName: string="";
  public teamId: number=0;
  public teamName: string="";
  public profitCenterId:  number=0;
  public profitCenterName: string="";
  public costCenterId: number=0;
  public costCenterName: string="";
  public isActive: boolean = true;
  public resignDate: Date;
  public signatureFileName: string="";
  public userCenterModel: Array<UserCenterModel> = [];
  public accountModel: Array<AccountDetailModel> = [];

}


export class FillterSearchModel extends CoreModel {
  public statusValue: number = 0;
  public roleValue: number = 0;
  public positionValue: number = 0;
  public buValue: number = 0;
  public teamValue: number = 0;
}

export class UserCenterModel{
  public teamId : number = 0;
  public teamName: string = "";
  public profitCenterId: number = 0;
  public costCenterId: number = 0;
  public profitCenterCode: string = "";
  public costCenterCode: string = "";
}

export class StatusList {
  value: string;
  viewValue: string;
}
*/


export class UserModel extends CoreModel {
  public id: string = "";
  public email: string = "";
  public userName: string = "";
  public name: string = "";
  public roleId: string = "";
  public roleName: string = "";
}
