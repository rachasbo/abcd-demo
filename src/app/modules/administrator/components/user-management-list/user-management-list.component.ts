import { Component, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { Service } from 'src/app/shared/service';
import { UserManagementSearchFormModel } from '../../models/user-management.models';

@Component({
  selector: 'app-user-management-list',
  templateUrl: './user-management-list.component.html',
  styleUrls: ['./user-management-list.component.scss']
})
export class UserManagementListComponent implements OnInit {
  public dataSource: any[] = [];
  public defaultUrl: string = '/v1/user-management/list?';
  public url: string = '/v1/user-management/list?';
  public isLocked: boolean = true;
  public loading: boolean = true;
  public lookUpStatus: any[] = [];
  public pageIndex: number = 0;
  public pageSize: number = 20;
  public length: number = 0;
  public currentItem: number = 1;
  public pageSizeOptions: number[] = [20, 40, 60, 80, 100]
  public searchForm: UserManagementSearchFormModel = new UserManagementSearchFormModel();
  constructor(
    public router: Router,
    public service: Service
  ) {
    this.loading = false
  }
  public displayedColumns: string[] = [
    'userId',
    'username',
    'administratorId',
    'roleName',
    'status',
    'menu'
  ];
  ngOnInit() {
    this.lookUpStatus = [
      { value: 'Active', label: 'Active', },
      { value: 'Inactive', label: 'Inactive' }]
  }

  ngDocheck(): void {
    this.checkLocked();
  }

  onRedirectEdit(id: any): void {
    this.router.navigate(['admin', 'user', 'edit', id])
  }

  onPaginate(paginator: MatPaginator): void {
    this.pageIndex = paginator.pageIndex
    this.pageSize = paginator.pageSize
    let url = this.setUrl()
    url += `page=${this.pageIndex}`
    this.loadData(url);
  }

  checkLocked(): void {
    const {
      userId,
      usernameId,
      administratorId,
      status,
      roleId } = this.searchForm;
    if (
      userId ||
      usernameId ||
      roleId ||
      administratorId ||
      status) {
      this.isLocked = false;
    } else {
      this.isLocked = true
    }
  }

  onSearch(): void {
    this.currentItem = 1;
    this.loading = true;
    this.url = this.setUrl();
    this.loadData(this.url);
  }

  setUrl(): string {
    let url = this.defaultUrl;
    const {
      userId,
      usernameId,
      usernameLabel,
      administratorId,
      status,
      roleId } = this.searchForm;
    if (userId) {
      url += `userId=${userId}&`
    }
    if (usernameId) {
      url += `username=${usernameLabel}&`
    }
    if (administratorId) {
      url += `administratorId=${administratorId}&`
    }
    if (status) {
      url += `status=${status}&`
    }
    if (roleId) {
      url += `roleId=${roleId}&`
    }
    url += `size=${this.pageSize}&`
    return url
  }

  loadData(url: string): void {
    this.loading = true;
    this.service.sendGetRequest(url, this, (_, response) => {
      const { data, statusCode } = response;
      if (statusCode === '200') {
        this.dataSource = data.items;
        this.length = data.total;
        this.loading = false;
      }
    })
  }

  onRedirect(id: any): void {
    this.router.navigate(['admin', 'user', 'view', id])
  }

  onSelectUsername(event): void {
    this.searchForm.usernameId = event.value
    this.searchForm.usernameLabel = event.label;
  }

  onSelectUserId(event): void {
    this.searchForm.userId = event.value
  }

  onSelectAdminId(event): void {
    this.searchForm.userId = event.value
  }


  onClear(): void {
    this.searchForm = new UserManagementSearchFormModel();
  }

}
