import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Service } from 'src/app/shared/service';
import { UserManagementViewModels } from '../../models/user-management.models';
import * as moment from 'moment'
@Component({
  selector: 'app-user-management-view',
  templateUrl: './user-management-view.component.html',
  styleUrls: ['./user-management-view.component.scss']
})
export class UserManagementViewComponent implements OnInit {
  public view: UserManagementViewModels = new UserManagementViewModels();
  public statusTxt: string;
  public loading: boolean;
  public userId: string;
  constructor(
    public router: Router,
    public service: Service,
    public activateRoute: ActivatedRoute
  ) {
    const params = this.activateRoute.snapshot.params
    this.userId = params.id
    this.loading = true;
    this.loadData();
    // Wait suchat for checking
  }


  setDateFormat(value): string {
    return value ? moment(value).format('DD-MM-YYYY') : null;
  }


  loadData(): void {
    this.loading = true
    this.service.sendGetRequest(`/v1/user-management/view-user/${this.userId}`, this, (main, response) => {
      const { statusCode, data } = response;
      if (statusCode === '200') {
        this.view = data;

        if (this.view.isActive) {
          this.statusTxt = 'Active'
        } else {
          this.statusTxt = 'Inactive'
        }
        this.loading = false;
      }
    })
  }

  ngOnInit() {
  }

}
