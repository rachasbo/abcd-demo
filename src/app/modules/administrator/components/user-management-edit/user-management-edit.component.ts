import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { DialogActionStatusComponent } from 'src/app/shared/components/dialog-action-status/dialog-action-status.component';
import { DialogDynamicConfirmComponent } from 'src/app/shared/components/dialog-dynamic-confirm/dialog-dynamic-confirm.component';
import { DropdownComponent } from 'src/app/shared/components/dropdown/dropdown.component';
import { InputFieldComponent } from 'src/app/shared/components/input-field/input-field.component';
import { Service } from 'src/app/shared/service';
import { UserManagementCreateFormModels, UserManagementEditFormModels, UserManagementViewModels } from '../../models/user-management.models';

@Component({
  selector: 'app-user-management-edit',
  templateUrl: './user-management-edit.component.html',
  styleUrls: ['./user-management-edit.component.scss']
})
export class UserManagementEditComponent implements OnInit {
  public lookUpStatus: any[] = [];
  public userId: string;
  public bodyEdit: UserManagementEditFormModels = new UserManagementEditFormModels();
  public view: UserManagementViewModels = new UserManagementViewModels()
  public loading: boolean = false;
  public isValid: boolean = true;
  public statusTxt: string;
  public regUser: any = /^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$/;
  public formBuilder: FormGroup;
  public form: UserManagementCreateFormModels = new UserManagementCreateFormModels();
  public isShowPwd: boolean;
  public inputHintMsg: string = '8 characters or longer, with at least one lowercase and one uppercase letter.';
  @ViewChild('username') public username: InputFieldComponent;
  @ViewChild('role') public role: DropdownComponent;
  constructor(
    public router: Router,
    public dialog: MatDialog,
    public service: Service,
    private builder: FormBuilder,
    public activateRoute: ActivatedRoute
  ) {
    const params = this.activateRoute.snapshot.params
    this.userId = params.id
    this.loading = true;
    this.loadData();
  }

  ngOnInit() {
    this.setLookUp();
    this.createFormData();
  }

  loadData(): void {
    this.loading = true
    this.service.sendGetRequest(`/v1/user-management/view-user/${this.userId}`, this, (main, response) => {
      const { statusCode, data } = response;
      if (statusCode === '200') {
        this.view = data;
        this.form.roleId = this.view.roleId;
        this.form.userId = this.view.userCode;
        this.form.username = this.view.username;
        this.form.status = this.view.isActive;
        if (this.view.isActive) {
          this.statusTxt = 'Active'
        } else {
          this.statusTxt = 'Inactive'
        }
        this.loading = false;
      }
    })
  }

  setLookUp(): void {
    this.lookUpStatus = [
      { label: 'Active', value: true },
      { label: 'Inactive', value: false }]
  }

  onGeneratePassword(): void {
    this.loading = true;
    this.service.sendGetRequest('/v1/user-management/generate-password', this,
      (main, response) => {
        const { data, statusCode } = response
        if (statusCode === '200') {
          this.isValid = true;
          this.form.password = data.password;
          this.form.passwordChangeDate = moment(data.lastChangeDate).format('YYYY-MM-DD');
          this.loading = false;
        }
      })
  }

  createFormData() {
    this.form.userId = '-'
    this.form.status = true;
    this.formBuilder = this.builder.group({
      password: [{ value: null, disabled: true }, [Validators.required]],
    })
  }

  validatePassword(): void {
    if (this.form.password) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  onSubmit(e): void {
    e.preventDefault();
    this.validatePassword()
    if (this.formBuilder.invalid) {
      // check validate form builder here
      this.username.checkValidate();
      this.role.checkValidate();
      for (let i in this.formBuilder.controls) {
        this.formBuilder.controls[i].markAsTouched();
      }
    } else {
      this.username.checkValidate();
      this.role.checkValidate();
      // when passed by form builder will do this process 
      // because shared component cannot use with form builder
      if (
        this.username.checkValidate() &&
        this.role.checkValidate() &&
        this.isValid
      ) {
        this.loading = true;
        this.bodyEdit.isActive = this.form.status;
        this.bodyEdit.password = this.form.password;
        this.bodyEdit.passwordChangeDate = moment().format();
        this.bodyEdit.roleId = this.form.roleId;
        this.bodyEdit.userCode = this.form.userId;
        this.bodyEdit.username = this.form.username;
        this.service.sendPostRequest('/v1/user-management/edit-user', this.bodyEdit, this,
          (main, response) => {
            const { data, statusCode } = response
            if (statusCode === '200') {
              this.loading = false;
              this.dialog.open(DialogActionStatusComponent, {
                data: {
                  pass: true,
                  title: 'Save Success', description: ''
                }
              }).afterClosed().subscribe(result => {
                this.router.navigate(['admin', 'user', 'view', this.userId])
              })
            } else {
              this.loading = false;
              this.dialog.open(DialogActionStatusComponent, {
                data: {
                  pass: false,
                  title: 'Save Failed', description: data
                }
              }).afterClosed().subscribe(result => {

              })
            }
          })
      }
    }
  }

  onCancel(): void {
    this.dialog.open(DialogDynamicConfirmComponent, {
      data: { title: 'Cancel', description: 'Are you sure you want to cancel ?' }
    })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.router.navigate(['admin', 'user', 'view', this.userId])
        }
      })

  }
}
