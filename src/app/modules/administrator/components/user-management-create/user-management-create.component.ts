import { Component, OnInit, ViewChild } from '@angular/core';
import { Compiler_compileModuleSync__POST_R3__ } from '@angular/core/src/linker/compiler';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { DialogDynamicConfirmComponent } from 'src/app/shared/components/dialog-dynamic-confirm/dialog-dynamic-confirm.component';
import { Service } from 'src/app/shared/service';
import { UserManagementCreateFormModels } from '../../models/user-management.models';
import * as moment from 'moment';
import { InputFieldComponent } from 'src/app/shared/components/input-field/input-field.component';
import { DropdownComponent } from 'src/app/shared/components/dropdown/dropdown.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogActionStatusComponent } from 'src/app/shared/components/dialog-action-status/dialog-action-status.component';
@Component({
  selector: 'app-user-management-create',
  templateUrl: './user-management-create.component.html',
  styleUrls: ['./user-management-create.component.scss']
})
export class UserManagementCreateComponent implements OnInit {
  public lookUpStatus: any[] = [];
  public loading: boolean = false;
  public isValid: boolean = true;
  public regUser: any = /^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$/;
  public formBuilder: FormGroup;
  public form: UserManagementCreateFormModels = new UserManagementCreateFormModels();
  public isShowPwd: boolean;
  public inputHintMsg: string = '8 characters or longer, with at least one lowercase and one uppercase letter.';
  @ViewChild('username') public username: InputFieldComponent;
  @ViewChild('role') public role: DropdownComponent;
  constructor(
    public router: Router,
    public dialog: MatDialog,
    public service: Service,
    private builder: FormBuilder
  ) { }

  ngOnInit() {
    this.setLookUp();
    this.createFormData();
  }

  setLookUp(): void {
    this.lookUpStatus = [
      { label: 'Active', value: true },
      { label: 'Inactive', value: false }]
  }

  onGeneratePassword(): void {
    this.loading = true;
    this.service.sendGetRequest('/v1/user-management/generate-password', this,
      (main, response) => {
        const { data, statusCode } = response
        if (statusCode === '200') {
          this.isValid = true;
          this.form.password = data.password;
          this.form.passwordChangeDate = moment(data.lastChangeDate).format('YYYY-MM-DD');
          this.loading = false;
        }
      })
  }

  createFormData() {
    this.form.userId = '-'
    this.form.status = true;
    this.formBuilder = this.builder.group({
      password: [{ value: null, disabled: true }, [Validators.required]],
    })
  }

  validatePassword(): void {
    if (this.form.password) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  onSubmit(e): void {
    e.preventDefault();
    this.validatePassword()
    if (this.formBuilder.invalid) {
      // check validate form builder here
      this.username.checkValidate();
      this.role.checkValidate();
      for (let i in this.formBuilder.controls) {
        this.formBuilder.controls[i].markAsTouched();
      }
    } else {
      this.username.checkValidate();
      this.role.checkValidate();
      // when passed by form builder will do this process 
      // because shared component cannot use with form builder
      if (
        this.username.checkValidate() &&
        this.role.checkValidate() &&
        this.isValid
      ) {
        this.loading = true;
        this.service.sendPostRequest('/v1/user-management/create-user', this.form, this,
          (main, response) => {
            const { data, statusCode } = response
            if (statusCode === '200') {
              this.loading = false;
              this.dialog.open(DialogActionStatusComponent, {
                data: {
                  pass: true,
                  title: 'Create Success', description: ''
                }
              }).afterClosed().subscribe(result => {
                this.router.navigate(['admin', 'user', 'view', response.data.userId])
              })
            } else {
              this.loading = false;
              this.dialog.open(DialogActionStatusComponent, {
                data: {
                  pass: false,
                  title: 'Create Failed', description: data
                }
              }).afterClosed().subscribe(result => {

              })
            }
          })
      }
    }
  }

  onCancel(): void {
    this.dialog.open(DialogDynamicConfirmComponent, {
      data: { title: 'Cancel', description: 'Are you sure you want to cancel ?' }
    })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.router.navigate(['admin', 'user'])
        }
      })

  }

}
