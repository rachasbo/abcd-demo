import { CoreModel } from "src/app/shared/models/core.model";



export class UserManagementCreateFormModels extends CoreModel {
    password: string = null;
    username: string = null;
    roleId: number = null;
    passwordChangeDate: string;
    userId: number | string = null;
    status: boolean = null;
}

export class UserManagementSearchFormModel extends CoreModel {
    userId: number = null;
    usernameId: number = null;
    usernameLabel: string = null;
    administratorId: number = null;
    status: string;
    roleId: number;
}

export class UserManagementViewModels extends CoreModel {
    isActive: boolean;
    lastLoginDate: string;
    numberRetry: number;
    passwordChangeDate: string;
    passwordExpireDate: string;
    roleId: number;
    roleName: string;
    userCode: string;
    username: string;
}

export class UserManagementEditFormModels extends CoreModel {
    userCode: string | number;
    username: string;
    roleId: number;
    isActive: boolean;
    password: string;
    passwordChangeDate: string;
}