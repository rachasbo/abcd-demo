import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagementCreateComponent } from './components/user-management-create/user-management-create.component';
import { UserManagementEditComponent } from './components/user-management-edit/user-management-edit.component';
import { UserManagementListComponent } from './components/user-management-list/user-management-list.component';
import { UserManagementViewComponent } from './components/user-management-view/user-management-view.component';

const routes: Routes = [
    {
        path: 'user',
        component: UserManagementListComponent
    },
    {
        path: 'user/view/:id',
        component: UserManagementViewComponent
    },
    {
        path: 'user/create',
        component: UserManagementCreateComponent
    },
    {
        path: 'user/edit/:id',
        component: UserManagementEditComponent
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdministratorRoutingModule { }
