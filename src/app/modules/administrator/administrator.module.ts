import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule, MatFormFieldModule, MatSelectModule, MatOptionModule, MatCardModule, MatInputModule, MatIconModule, MatButtonModule, MatCheckboxModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatBadgeModule, MatProgressSpinnerModule, MatRadioModule, MatChipsModule, MatDividerModule, MatPaginatorModule } from '@angular/material';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfigRoutingModule } from '../config/config-routing.module';
import { AdministratorRoutingModule } from './administrator-routing.model';
import { UserManagementListComponent } from './components/user-management-list/user-management-list.component';
import { UserManagementViewComponent } from './components/user-management-view/user-management-view.component';
import { UserManagementCreateComponent } from './components/user-management-create/user-management-create.component';
import { UserManagementEditComponent } from './components/user-management-edit/user-management-edit.component';

@NgModule({
  declarations: [UserManagementListComponent, UserManagementViewComponent, UserManagementCreateComponent, UserManagementEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatChipsModule,
    AdministratorRoutingModule,
    MatDividerModule,
    MatPaginatorModule
  ],

})
export class AdministratorModule { }
