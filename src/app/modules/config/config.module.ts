import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { ConfigRoutingModule } from './config-routing.module';
import { DataTypeFormComponent } from './components/datatype-form/datatype-form.component';
import { DataTypeListComponent } from './components/datatype-list/datatype-list.component';
import { DataTypeViewComponent } from './components/datatype-view/datatype-view.component';

// import { MatIconModule } from '@angular/material';
import { MatFormFieldModule, MatBadgeModule, MatAutocompleteModule, MatProgressSpinnerModule, MatSelectModule, MatOptionModule, MatCardModule, MatInputModule, MatIconModule, MatButtonModule, MatCheckboxModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatRadioModule, MatChipsModule } from '@angular/material';
import { CertItmxComponent } from './components/cert-itmx/cert-itmx.component';
import { EditItmxDetailDialogComponent } from './components/cert-dialog/edit-itmx-detail-dialog/edit-itmx-detail-dialog.component';
import { EditHostDialogComponent } from './components/cert-dialog/edit-host-dialog/edit-host-dialog.component';
import { EditPortDialogComponent } from './components/cert-dialog/edit-port-dialog/edit-port-dialog.component';
import { CertIcmgComponent } from './components/cert-icmg/cert-icmg.component';
import { EditIcmgDetailDialogComponent } from './components/cert-dialog/edit-icmg-detail-dialog/edit-icmg-detail-dialog.component';

@NgModule({
  declarations: [DataTypeFormComponent, DataTypeListComponent, DataTypeViewComponent, CertItmxComponent, EditItmxDetailDialogComponent, EditHostDialogComponent, EditPortDialogComponent, CertIcmgComponent, EditIcmgDetailDialogComponent],
  imports: [
    CommonModule,
    ConfigRoutingModule,
    SharedModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatChipsModule
  ],
  entryComponents: [
    EditIcmgDetailDialogComponent,
    EditHostDialogComponent,
    EditPortDialogComponent,
    EditItmxDetailDialogComponent
  ]

})
export class ConfigModule { }
