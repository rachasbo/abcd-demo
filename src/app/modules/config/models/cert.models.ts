import { CoreModel } from "src/app/shared/models/core.model";

export class CertItmxTableModels extends CoreModel {
    id: number = null;
    name: string = '';
    alias: string = '';
    date: string = '';
}

export class CertIcmgTableModels extends CoreModel {
    id: number = null;
    serverId: string = '';
    certificate: string = '';
    keyStore: string = '';
    alias: string = '';
    passphase: string = '';
}