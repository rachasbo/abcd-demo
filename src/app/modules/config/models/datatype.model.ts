import { CoreModel } from './../../../shared/models/core.model';

export class DataTypeModel extends CoreModel {
    public id: number = 0;
    public name: string = "";
    public desc: string = "";
    public appFlowName: string = "";
}
