import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';

import { DataTypeModel } from '../../models/datatype.model';

@Component({
  selector: 'app-datatype-form',
  templateUrl: './datatype-form.component.html',
  styleUrls: ['./datatype-form.component.scss']
})
export class DataTypeFormComponent implements OnInit {

  data: DataTypeModel = new DataTypeModel();
  id: number = 0;

  form: FormGroup;

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private builder:FormBuilder
  ) {
    const params = this.activateRoute.snapshot.params;
    this.id = params.id;

    if (this.router.url != '/config/datatype/create') {
      this.genSampleData();
    }

    this.createFormData();
  }

  ngOnInit() {

  }

  genSampleData() {
    this.data = new DataTypeModel();
    this.data.deserialize({
      id: this.id,
      name: "Datatype " + this.id,
      desc: "desc desc desc",
      appFlowName: "app flow name"
    })
  }

  private createFormData() {
    this.form = this.builder.group({
      //id: [this.data.id, Validators.required],
      name: [this.data.name, Validators.required],
      desc: [this.data.desc, Validators.required],
    })
  }

  onSubmit() {
    this.form.get('leadCode').markAsTouched();
    this.form.get('contactCode').markAsTouched();
    this.form.get('contactName').markAsTouched();
    this.form.get('leadStatusCode').markAsTouched();
    //this.form.get('leadStatusName').markAsTouched();
    this.form.get('leadCompany').markAsTouched();
    this.form.get('data1').markAsTouched();

    //if (this.form.invalid) return;

    console.log(this.form.value);

    //this.router.navigate(['lead', this.id]);
  }

  customValidator(control: AbstractControl) {
    if (control.value == '') {
      return { required: true }
    }
  }

}
