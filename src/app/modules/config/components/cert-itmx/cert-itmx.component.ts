import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CertItmxTableModels } from '../../models/cert.models';
import { EditHostDialogComponent } from '../cert-dialog/edit-host-dialog/edit-host-dialog.component';
import { EditItmxDetailDialogComponent } from '../cert-dialog/edit-itmx-detail-dialog/edit-itmx-detail-dialog.component';
import { EditPortDialogComponent } from '../cert-dialog/edit-port-dialog/edit-port-dialog.component';


@Component({
  selector: 'app-cert-itmx',
  templateUrl: './cert-itmx.component.html',
  styleUrls: ['./cert-itmx.component.scss']
})
export class CertItmxComponent implements OnInit {
  public data: CertItmxTableModels[] = [];
  public host: string = 'mockup.host-ip.com';
  public port: string = '4200';;
  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.setTable();
  }


  setTable(): void {
    for (var i = 1; i <= 15; i++) {
      var data = new CertItmxTableModels();
      data.deserialize({
        id: i,
        name: "Certificate " + i,
        alias: "12345",
        date: "2021-10-01"
      });
      this.data.push(data);
    }
  }

  onEdit(row): void {
    this.dialog.open(EditItmxDetailDialogComponent, { data: { form: row } })
      .afterClosed()
      .subscribe(result => {
      })
  }

  onEditPort(): void {
    this.dialog.open(EditPortDialogComponent, { data: { value: this.port } })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.port = result.value;
        }
      })
  }

  onEditHost(): void {
    this.dialog.open(EditHostDialogComponent, { data: { value: this.host } })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.host = result.value;
        }
      })
  }

  onView(): void {

  }

}
