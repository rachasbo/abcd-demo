import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertItmxComponent } from './cert-itmx.component';

describe('CertItmxComponent', () => {
  let component: CertItmxComponent;
  let fixture: ComponentFixture<CertItmxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertItmxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertItmxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
