import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { CoreComponent } from 'src/app/shared/components/core_component';

import { DataTypeModel } from './../../models/datatype.model';

@Component({
  selector: 'app-datatype-list',
  templateUrl: './datatype-list.component.html',
  styleUrls: ['./datatype-list.component.scss']
})
// export class DataTypeListComponent extends CoreComponent implements OnInit {
export class DataTypeListComponent implements OnInit {

  datas: DataTypeModel[] = []

  constructor(
    private router: Router
  ) {
    // super();
  }

  ngOnInit() {
    this.genSampleDatas();
  }

  private genSampleDatas() {
    for (var i=1; i<=15; i++) {
      var data = new DataTypeModel();
      data.deserialize({
        id: i,
        name: "Datatype " + i,
        desc: "desc desc desc",
        appFlowName: "app flow name"
      });
      this.datas.push(data);
    }
  }

  onCreate() {
    this.router.navigate(['config', 'datatype', 'create']);
  }

  onView(id: number) {
    this.router.navigate(['config', 'datatype', id]);
  }

  onEdit(id: number) {
    this.router.navigate(['config', 'datatype', id, 'edit']);
  }

}
