import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DatepickerComponent } from 'src/app/shared/components/datepicker/datepicker.component';
import { DialogDynamicConfirmComponent } from 'src/app/shared/components/dialog-dynamic-confirm/dialog-dynamic-confirm.component';
import { InputFieldComponent } from 'src/app/shared/components/input-field/input-field.component';
import { TextAreaComponent } from 'src/app/shared/components/text-area/text-area.component';
import { CertIcmgTableModels } from '../../../models/cert.models';

@Component({
  selector: 'app-edit-icmg-detail-dialog',
  templateUrl: './edit-icmg-detail-dialog.component.html',
  styleUrls: ['./edit-icmg-detail-dialog.component.scss']
})
export class EditIcmgDetailDialogComponent implements OnInit {
  public form: CertIcmgTableModels = new CertIcmgTableModels();
  @ViewChild('alias') public alias: InputFieldComponent;
  @ViewChild('passphase') public passphase: InputFieldComponent;
  @ViewChild('keyStore') public keyStore: InputFieldComponent;
  @ViewChild('serverId') public serverId: InputFieldComponent;
  @ViewChild('certificate') public certificate: InputFieldComponent;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditIcmgDetailDialogComponent>,
    public dialog: MatDialog
  ) {
    this.form = this.data.form;
  }

  ngOnInit() { }

  onClose(): void {
    this.dialog.open(DialogDynamicConfirmComponent, {
      data: { title: 'Cancel', description: 'Are you sure you want to cancel ?' }
    })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.dialogRef.close({ status: false, value: this.form })
        }
      })
  }

  onSubmit(): void {
    this.serverId.checkValidate();
    this.certificate.checkValidate();
    this.alias.checkValidate();
    this.passphase.checkValidate();
    this.keyStore.checkValidate();
    if (
      this.serverId.checkValidate() &&
      this.certificate.checkValidate() &&
      this.alias.checkValidate() &&
      this.passphase.checkValidate() &&
      this.keyStore.checkValidate() 
    ) {
      this.dialogRef.close({ status: true, value: this.form })
    }
  }

}
