import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIcmgDetailDialogComponent } from './edit-icmg-detail-dialog.component';

describe('EditIcmgDetailDialogComponent', () => {
  let component: EditIcmgDetailDialogComponent;
  let fixture: ComponentFixture<EditIcmgDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditIcmgDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIcmgDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
