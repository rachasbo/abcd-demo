import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogDynamicConfirmComponent } from 'src/app/shared/components/dialog-dynamic-confirm/dialog-dynamic-confirm.component';
import { InputFieldComponent } from 'src/app/shared/components/input-field/input-field.component';

@Component({
  selector: 'app-edit-port-dialog',
  templateUrl: './edit-port-dialog.component.html',
  styleUrls: ['./edit-port-dialog.component.scss']
})
export class EditPortDialogComponent implements OnInit {
  public value: string;
  @ViewChild('field') public field: InputFieldComponent;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditPortDialogComponent>,
    public dialog: MatDialog
  ) {
    this.value = this.data.value;
  }

  ngOnInit() { }

  onClose(): void {
    this.dialog.open(DialogDynamicConfirmComponent, {
      data: {
        title: 'Cancel',
        description: 'Are you sure you want to cancel ?'
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.dialogRef.close({ status: false, value: this.value })
        }
      })
  }

  onSubmit(): void {
    this.field.checkValidate();
    if (this.field.checkValidate()) {
      this.dialogRef.close({ status: true, value: this.value })
    }
  }

}
