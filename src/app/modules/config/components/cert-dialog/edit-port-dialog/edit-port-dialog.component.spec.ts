import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPortDialogComponent } from './edit-port-dialog.component';

describe('EditPortDialogComponent', () => {
  let component: EditPortDialogComponent;
  let fixture: ComponentFixture<EditPortDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPortDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPortDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
