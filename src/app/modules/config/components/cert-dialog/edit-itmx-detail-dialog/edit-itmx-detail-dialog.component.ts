import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DatepickerComponent } from 'src/app/shared/components/datepicker/datepicker.component';
import { DialogDynamicConfirmComponent } from 'src/app/shared/components/dialog-dynamic-confirm/dialog-dynamic-confirm.component';
import { InputFieldComponent } from 'src/app/shared/components/input-field/input-field.component';
import { TextAreaComponent } from 'src/app/shared/components/text-area/text-area.component';
import { CertItmxTableModels } from '../../../models/cert.models';

@Component({
  selector: 'app-edit-itmx-detail-dialog',
  templateUrl: './edit-itmx-detail-dialog.component.html',
  styleUrls: ['./edit-itmx-detail-dialog.component.scss']
})
export class EditItmxDetailDialogComponent implements OnInit {
  public form: CertItmxTableModels = new CertItmxTableModels();
  @ViewChild('alias') public alias: InputFieldComponent;
  @ViewChild('date') public date: DatepickerComponent;
  @ViewChild('certi') public certi: TextAreaComponent;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditItmxDetailDialogComponent>,
    public dialog: MatDialog
  ) {
    this.form = this.data.form;
  }

  ngOnInit() { }

  onClose(): void {
    this.dialog.open(DialogDynamicConfirmComponent, {
      data: { title: 'Cancel', description: 'Are you sure you want to cancel ?' }
    })
      .afterClosed()
      .subscribe(result => {
        if (result.status) {
          this.dialogRef.close({ status: false, value: this.form })
        }
      })
  }

  onSubmit(): void {
    this.alias.checkValidate();
    this.certi.checkValidate();
    this.date.checkValidate();
    if (this.alias.checkValidate() &&
      this.certi.checkValidate() &&
      this.date.checkValidate()) {
      this.dialogRef.close({ status: true, value: this.form })
    }
  }

}
