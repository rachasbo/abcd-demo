import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditItmxDetailDialogComponent } from './edit-itmx-detail-dialog.component';

describe('EditItmxDetailDialogComponent', () => {
  let component: EditItmxDetailDialogComponent;
  let fixture: ComponentFixture<EditItmxDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditItmxDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditItmxDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
