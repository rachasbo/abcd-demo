import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CertIcmgTableModels } from '../../models/cert.models';
import { EditIcmgDetailDialogComponent } from '../cert-dialog/edit-icmg-detail-dialog/edit-icmg-detail-dialog.component';

@Component({
  selector: 'app-cert-icmg',
  templateUrl: './cert-icmg.component.html',
  styleUrls: ['./cert-icmg.component.scss']
})
export class CertIcmgComponent implements OnInit {
  public data: any[] = [];
  public host: string = 'mockup.host-ip.com';
  public port: string = '4200';;
  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.setTable();
  }
  setTable(): void {
    for (var i = 1; i <= 15; i++) {
      var data = new CertIcmgTableModels();
      data.deserialize({
        id: i,
        certificate: "Certificate " + i,
        alias: "12345",
        passphase: "2021-10-01",
        serverId: 'Server ' + i,
        keyStore: 'Key ' + i
      });
      this.data.push(data);
    }
  }

  onEdit(row): void {
    this.dialog.open(EditIcmgDetailDialogComponent, { data: { form: row } })
      .afterClosed()
      .subscribe(result => {
      })
  }


  onView(): void {

  }

}
