import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertIcmgComponent } from './cert-icmg.component';

describe('CertIcmgComponent', () => {
  let component: CertIcmgComponent;
  let fixture: ComponentFixture<CertIcmgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertIcmgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertIcmgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
