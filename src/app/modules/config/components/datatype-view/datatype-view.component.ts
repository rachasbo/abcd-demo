import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DataTypeModel } from '../../models/datatype.model';

@Component({
  selector: 'app-datatype-view',
  templateUrl: './datatype-view.component.html',
  styleUrls: ['./datatype-view.component.scss']
})
export class DataTypeViewComponent implements OnInit {

  data: DataTypeModel;
  id: number = 0;

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {
    const params = this.activateRoute.snapshot.params;
    this.id = params.id;
  }

  ngOnInit() {
    this.genSampleData();
  }

  genSampleData() {
    this.data = new DataTypeModel();
    this.data.deserialize({
      id: this.id,
      name: "Datatype " + this.id,
      desc: "desc desc desc",
      appFlowName: "app flow name"
    })
  }

  onEdit() {
    this.router.navigate(['config', 'datatype', this.id, 'edit']);
  }

}
