import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTypeViewComponent } from './datatype-view.component';

describe('DataTypeViewComponent', () => {
  let component: DataTypeViewComponent;
  let fixture: ComponentFixture<DataTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
