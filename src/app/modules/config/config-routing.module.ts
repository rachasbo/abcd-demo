import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataTypeListComponent } from './components/datatype-list/datatype-list.component';
import { DataTypeFormComponent } from './components/datatype-form/datatype-form.component';
import { DataTypeViewComponent } from './components/datatype-view/datatype-view.component';
import { CertItmxComponent } from './components/cert-itmx/cert-itmx.component';
import { CertIcmgComponent } from './components/cert-icmg/cert-icmg.component';
const routes: Routes = [
  {
    path: 'datatype',
    component: DataTypeListComponent
  },
  {
    path: 'datatype/create',
    component: DataTypeFormComponent
  },
  {
    path: 'datatype/:id',
    component: DataTypeViewComponent
  },
  {
    path: 'datatype/:id/edit',
    component: DataTypeFormComponent
  },
  {
    path: 'cert/itmx',
    component: CertItmxComponent
  },
  {
    path: 'cert/icmg',
    component: CertIcmgComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigRoutingModule { }
