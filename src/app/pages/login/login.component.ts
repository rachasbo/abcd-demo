import { Component, OnInit } from '@angular/core';
import { Service } from 'src/app/shared/service';
import { Router } from '@angular/router';
import { Auth } from 'src/app/shared/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  isLoading: Boolean = false;

  isLoginFailed: Boolean = false;

  isShowPwd = false;
  
  constructor(
    private service: Service,
    private router: Router,
    // public warningdialog: MatDialog,
    private auth: Auth,
    private builder: FormBuilder
  ) {

  }

  ngOnInit() {
    this.createFormData();
  }

  private createFormData() {
    this.form = this.builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  public onCancel() {
    this.form.reset();
  }

  public onLogin() {
    // console.log(this.form.value);

    // localStorage.setItem("auth_token", 'xxxx'); // force success logined for test
    // window.location.href = '/';
    // return;


    this.isLoginFailed = false;

    this.auth.clearForcedChangePwd();

    if (this.form.invalid)  {
      console.log('form => invalid');

      for(let i in this.form.controls) {
        this.form.controls[i].markAsTouched();
      }

      this.isLoginFailed = true;

    } else {
      console.log('form => valid');

      this.isLoading = true;

      // localStorage.removeItem("auth_token");
      this.auth.clearAllLoginSessionData();

      //var data = this.form.value;

      var data = {
        'username': this.form.get('username').value,
        'password': this.form.get('password').value
      }

      // this.isLoading = true;

      // console.log("/v1/auth/login");
      // console.log(data);
      this.service.sendPostRequest("/v1/auth/login", data, this, function(main, response) {
        console.log(response);

        // main.isLoading = false;

        if (response && response.data) {

          main.auth.saveUserLogined(response.data.token);

          // response.data.isForceChange = true;   // force for test

          setTimeout(function() {
            if (response.data.isForceChange) {
              main.auth.setForcedChangePwd();
  
              window.location.href = '/change_pwd';
            } else {
              // main.router.navigate(['/']);
              window.location.href = '/';
            }
          }, 1500);
          
        } else {
          main.isLoginFailed = true;

          main.isLoading = false;
        }
      }, true, true, true);

    }

  }

}
