import { Component, OnInit, Inject } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ChildSubMenu } from 'src/app/shared/models/navbar.model';
import { Service } from 'src/app/shared/service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Auth } from 'src/app/shared/auth';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  env = environment;
  tabList: ChildSubMenu;

  constructor(
    private dataService: Service,
    private router: Router,
    public warningdialog: MatDialog,
    private auth: Auth,
  ) {

  }

  ngOnInit() {
    // if (this.auth.isLogined()) {
    //   this.dataService.sendGetTabPermission('dashboard', null, this, function (main, response) {
    //     main.tabList = response.menu;
    //     main.setTabPermission();
    //     main.showLoading = false;
    //   });
    // }

    if (!this.auth.isLogined()) {
      this.router.navigate(['login']);
    } else {
      if (this.auth.isForcedChangePwd()) {
        // this.router.navigate(['change_pwd']);
        window.location.href = '/change_pwd';
      }
    }
  }

  setTabPermission() {
    if(this.tabList.permission.length == 0){
      const dialofRef = this.warningdialog.open(DialogWarning, {
        // data: dataObj
      }).afterClosed().subscribe(dialogResult => {
        this.router.navigate(['deal','deal-deal']);
      });;
    }
  }
}

@Component({
  selector: 'dialog-warning',
  templateUrl: './dialog-warning.html',
  styleUrls: ['./home.component.scss']
})
export class DialogWarning implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogWarning>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router
  ) {
  }
  ngOnInit() {
  }

  onOK() {
    this.dialogRef.close();
  }
}
