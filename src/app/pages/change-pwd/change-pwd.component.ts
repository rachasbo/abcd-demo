import { Component, OnInit } from '@angular/core';
import { Service } from 'src/app/shared/service';
import { Router } from '@angular/router';
import { Auth } from 'src/app/shared/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserModel } from 'src/app/modules/user-management/models/user-model.model';

@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.scss']
})
export class ChangePwdComponent implements OnInit {

  isLoading: Boolean = false;

  form: FormGroup;

  // isFormFailed: Boolean = false;

  isShowPwd = false;
  isShowNewPwd = false;
  isShowNewPwdComfirm = false;

  user: UserModel;

  inputHintMsg = '8 characters or longer,with at least one lowercase and one uppercase letter.';
  inputMatchErrMsg = 'The password and confirmation password do not match.';
  inputPwdCurrentMsg = this.inputHintMsg;
  
  constructor(
    private service: Service,
    private router: Router,
    private auth: Auth,
    private builder: FormBuilder
  ) {

  }

  ngOnInit() {
    if (this.auth.isLogined()) {
      this.user = this.auth.getUser();
      // console.log('user:', this.user);

      this.createFormData();
    } else {
      window.location.href = '/login';
    }
  }

  private createFormData() {
    let username = '';
    if (this.user && this.user.name) {
      username = this.user.name;
    }

    this.form = this.builder.group({
      username: [username],
      password: [null, Validators.required],
      newPassword: [null, Validators.required],
      newPasswordConfirm: [null, Validators.required],
    })
  }

  public onCancel() {
    this.auth.logout();
  }

  public onSubmit() {
    // console.log(this.form.value);

    // this.isFormFailed = false;
    this.inputPwdCurrentMsg = this.inputHintMsg;

    if (this.form.invalid)  {
      console.log('form => invalid');

      for(let i in this.form.controls) {
        this.form.controls[i].markAsTouched();
      }

      // this.isFormFailed = true;

    } else {
      console.log('form => valid');

      this.isLoading = true;

      // window.location.href = '/';

      let password = this.form.get('password').value.trim();
      let newPassword = this.form.get('newPassword').value.trim();
      let newPasswordConfirm = this.form.get('newPasswordConfirm').value.trim();

      if (newPassword != newPasswordConfirm) {
        // this.isFormFailed = true;
        this.inputPwdCurrentMsg = this.inputMatchErrMsg;
      } else {

        let data = {
          'password': password,
          'newPassword': newPassword
        }

        this.service.sendPostRequest("/v1/auth/change-password", data, this, function(main, response) {
          console.log(response);
  
          if (response && response.statusCode=='200') {
            main.auth.clearForcedChangePwd();

            window.location.href = '/';
          } else {
            // main.isFormFailed = true;
            main.inputPwdCurrentMsg = response.message;
          }

          main.isLoading = false;
        }, true, true, true);

      }

    }

  }

}
