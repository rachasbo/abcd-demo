import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {

  @Input('value') value:string;
  @Input('styleValue') styleValue:string;

  constructor() { }

  ngOnInit() {
    if(this.styleValue){
      this.styleValue = this.styleValue+" word-break";
    }else{
      this.styleValue = "word-break";
    }
  }
}
