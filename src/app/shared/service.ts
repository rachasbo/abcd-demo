import { Router } from '@angular/router';
import { NavbarModel, ChildSubMenu } from './models/navbar.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { DialogErrorComponent } from './components/dialog-error/dialog-error.component';
import { DialogSessionTimeoutComponent } from './components/dialog-session-timeout/dialog-session-timeout.component';
import { DialogSuccessComponent } from './components/dialog-success/dialog-success.component';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';

//import { SubscribeOnObservable, Subscriber } from 'rxjs/internal-compatibility';
import { Auth } from './auth';
import { DialogWarningComponent } from './components/dialog-warning/dialog-warning.component';

@Injectable({
    providedIn: 'root'
})

export class Service {

    //headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + localStorage.getItem("msal.idtoken") });

    domain = environment.api;

    main: any;

    isOpenDialogTimeout = false;

    constructor(
        private httpClient: HttpClient,
        public dialogError: MatDialog,
        private auth: Auth,
        private router: Router,
        private location: Location
    ) {

    }

    // public getHeader() {
    //     return new HttpHeaders({
    //         'Content-Type': 'application/json; charset=utf-8',
    //         // 'Authorization': 'Bearer ' + this.auth.getAccessToken()
    //         'Authorization': this.auth.getAccessToken()
    //     })
    // }
    public getHeader() {
        let header;

        let token = this.auth.getAccessToken();
        // let token = localStorage.getItem("msal.idtoken");

        if (token) {
            header = new HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8',
                // 'Authorization': 'Bearer ' + token
                'Authorization': token
            })
        } else {
            header = new HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8'
            })
        }

        return header;
    }
    // public getHeader(skipAuthHeader=false) {
    //     let headers;

    //     if (!skipAuthHeader) {
    //         headers = new HttpHeaders({
    //             'Content-Type': 'application/json; charset=utf-8',
    //             // 'Authorization': 'Bearer ' + this.auth.getAccessToken()
    //             'Authorization': this.auth.getAccessToken()
    //         })
    //     } else {
    //         headers = new HttpHeaders({
    //             'Content-Type': 'application/json; charset=utf-8'
    //         })
    //     }

    //     return headers;
    // }

    public sendGetRequest(url: string, main, callback, disabledError=false, forcedResponseError=false) {
        let service = this.httpClient.get(this.domain + url, { 'headers': this.getHeader() });

        this.handleResponse(service, main, callback, true, disabledError, forcedResponseError);
    }

    public sendPostRequest(url: string, body: any, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        let service = this.httpClient.post(this.domain + url, body, { 'headers': this.getHeader() });
        
        this.handleResponse(service, main, callback, disabledSuccess, disabledError, forcedResponseError);
    }

    public sendPostRequestMultipart(url: string, body: any, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        // let header = new HttpHeaders({ 'Authorization': 'Bearer ' + this.auth.getAccessToken() });
        let header = new HttpHeaders({ 'Authorization': this.auth.getAccessToken() });

        let service = this.httpClient.post(this.domain + url, body, { 'headers': header });

        this.handleResponse(service, main, callback, disabledSuccess, disabledError, forcedResponseError);
    }

    public sendPutRequest(url: string, body: any, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        let service = this.httpClient.put(this.domain + url, body, { 'headers': this.getHeader() });

        this.handleResponse(service, main, callback, disabledSuccess, disabledError, forcedResponseError);
    }

    public sendPutRequestMultipart(url: string, body: any, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        let header = new HttpHeaders({ 'Authorization': 'Bearer ' + this.auth.getAccessToken() });

        let service = this.httpClient.put(this.domain + url, body, { 'headers': header });

        this.handleResponse(service, main, callback, disabledSuccess, disabledError, forcedResponseError);
    }

    public sendDeleteRequest(url: string, body: any, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        let service = this.httpClient.delete(this.domain + url, { 'headers': this.getHeader() });

        this.handleResponse(service, main, callback, disabledSuccess, disabledError, forcedResponseError);
    }

    public sendPatchRequest(url: string, body: any, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        let service = this.httpClient.patch(this.domain + url, body, { 'headers': this.getHeader() });

        this.handleResponse(service, main, callback, disabledSuccess, disabledError, forcedResponseError);
    }

    private handleResponse(service, main, callback, disabledSuccess=true, disabledError=false, forcedResponseError=false) {
        let data_response = null;
        
        let Main = main;

        service.subscribe(
            values => {
                data_response = values;
                if (data_response.statusCode == 200) {
                    if (!disabledSuccess) {
                        const dialogRef = this.dialogError.open(DialogSuccessComponent, {
                            data: data_response.message
                        }).afterClosed().subscribe(result => {
                            callback(Main, data_response);
                        });
                    } else {
                        callback(Main, data_response);
                    }
                } else {
                    if (!disabledError) {
                        const dialogRef = this.dialogError.open(DialogErrorComponent, {
                            data: data_response.message
                        });
                    }

                    // callback(Main, null);

                    delete data_response.data;

                    callback(Main, data_response);
                }

                this.auth.checkUpdateToken();
            }, error => {
                if (error.status == 401) {
                    //console.log("session time out : " + localStorage.getItem('msal.idtoken'));
                    console.log("session time out :\n" + this.auth.getAccessToken());

                    if (!disabledError) {
                        if (!this.isOpenDialogTimeout) {
                            this.isOpenDialogTimeout = true;
                            const dialogRef = this.dialogError.open(DialogSessionTimeoutComponent, {
                            }).afterClosed().subscribe(result => {
                                this.isOpenDialogTimeout = false;
                                this.auth.reLogin();
                            });
                        }
                    }
                } else if (error.status == 403) {
                    if (!disabledError) {
                        const dialogRef = this.dialogError.open(DialogErrorComponent, {
                            data: error.error.message
                        });
                    }

                    setTimeout(function() {
                        window.location.href = '/';
                    }, 2000);
                }

                if (forcedResponseError) {
                    // callback(Main, null);

                    let data_response = error.error;
                    delete data_response.data;

                    callback(Main, data_response);
                }
            }
        );
    }

    public sendDownloadRequest(url: string, main, callback, disabledError=false, forcedResponseError=false) {
        let data_response = null;

        let Main = main

        // var responseData = this.httpClient.get(this.domain).toPromise();
        // console.log("responseData", responseData);

        // let header = new HttpHeaders({
        //     'Content-Type': 'application/json; charset=utf-8',
        //     'Content-Disposition': 'attachment; filename="test.txt"',
        //     // 'Authorization': 'Bearer ' + token
        //     'Authorization': this.auth.getAccessToken()
        // })

        //let service = this.httpClient.get(this.domain + url, { 'headers': new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + localStorage.getItem("msal.idtoken") }) });
        // let service = this.httpClient.get(this.domain + url, { 'headers': this.getHeader() });
        // let service = this.httpClient.get(this.domain + url, { 'headers': header });
        let service = this.httpClient.get(this.domain + url, { 'headers': this.getHeader(), 'responseType': 'blob', 'observe': 'response' });
        // let service = this.httpClient.get(this.domain + url, { 'headers': this.getHeader(), 'observe': 'response' });
        service.subscribe(
            values => {
                data_response = values;
                // data_response:Blob = values;

                console.log(data_response);

                if (data_response.status == 200) {
                    // console.log(data_response.body);
                    // console.log(data_response.headers);
                    console.log(data_response.headers.get('Content-Disposition'));

                    let fileName: string;
                    try {
                        let contentDisposition: string = data_response.headers.get('content-disposition');
                        let r = /(?:filename=")(.+)(?:")/
                        fileName = r.exec(contentDisposition)[1];
                    } catch (e) {
                        fileName = 'download'
                    }

                    // var binaryData = [];
                    // // binaryData.push(data_response);
                    // binaryData.push(data_response.body);
                    // // var url = window.URL.createObjectURL(new Blob(binaryData, { type: data_response.type }));
                    // var url = window.URL.createObjectURL(new Blob(binaryData, { type: data_response.body.type }));

                    var url = window.URL.createObjectURL(data_response.body);
                    var a = document.createElement('a');
                    document.body.appendChild(a);
                    a.setAttribute('style', 'display: none');
                    a.setAttribute('target', 'blank');
                    a.href = url;
                    a.download = fileName;
                    a.click();

                    window.URL.revokeObjectURL(url);
                    a.remove();

                } else {
                    // if (!disabledError) {
                    //     const dialogRef = this.dialogError.open(DialogErrorComponent, {
                    //         data: data_response.message
                    //     });
                    // }

                    // // callback(Main, null);

                    // delete data_response.data;

                    // callback(Main, data_response);
                }

                this.auth.checkUpdateToken();
            }, async error => {
                console.log(error);
                console.log(error.error.text());

                let response = await error.error.text();
                let responseObj = JSON.parse(response);
                
                if (error.status == 401) {
                    //console.log("session time out : " + localStorage.getItem('msal.idtoken'));
                    console.log("session time out :\n" + this.auth.getAccessToken());

                    if (!disabledError) {
                        if (!this.isOpenDialogTimeout) {
                            this.isOpenDialogTimeout = true;
                            const dialogRef = this.dialogError.open(DialogSessionTimeoutComponent, {
                            }).afterClosed().subscribe(result => {
                                this.isOpenDialogTimeout = false;
                                this.auth.reLogin();
                            });
                        }
                    }
                } else {
                    if (!disabledError) {
                        // error.error.text().then(function(result) {
                        //     let response = JSON.parse(result);
                            
                        //     const dialogRef = Main.dialogError.open(DialogErrorComponent, {
                        //         data: response.message
                        //     });
                        // });

                        const dialogRef = this.dialogError.open(DialogErrorComponent, {
                            data: responseObj.message
                        });
                    }
                }

                if (forcedResponseError) {
                    // callback(Main, null);

                    let data_response = responseObj;
                    delete data_response.data;

                    callback(Main, data_response);
                }
            }
        );
    }

    public sendGetTabPermission(menuBar, subMenuName, main, callback) {
        let data_response = null;

        let Main = main

        let service = this.httpClient.get(this.domain + '/authen/v1/Users/permission', { 'headers': this.getHeader() });
        service.subscribe(
            values => {
                data_response = values;
                if (data_response.statusCode != 200) {
                    const dialogRef = this.dialogError.open(DialogErrorComponent, {
                        data: data_response.message
                    });
                    callback(Main, null);
                } else {
                    let response = data_response;
                    if (response != null) {
                        if (response.data != null) {
                            let menuList: NavbarModel[] = JSON.parse(JSON.stringify(response.data));
                            let menu: NavbarModel = null;
                            let subMenu: ChildSubMenu = null;
                            menuList.forEach(item => {
                                if (item.menuCode == menuBar) {
                                    menu = item;
                                }
                            });
                            let canView: boolean = false;

                            menu.permission.forEach(item => {
                                if (item == 'view') {
                                    canView = true;
                                }
                            });
                            // console.log("test View",canView);
                            if (canView) {
                                menu.child.forEach(item => {
                                    if (item.menuCode == subMenuName) {
                                        subMenu = item;
                                    }
                                });
                            } else {
                                // this.router.navigate(['/']);

                                document.getElementById('app-container-main').style.display = 'none';

                                this.dialogError.open(DialogWarningComponent, {
                                    data: {
                                        isOneButton: true,
                                        title: "Warning",
                                        content: "You don't have permission to view this page.",
                                        okBtnText: "OK",
                                    }
                                }).afterClosed().subscribe(result => {
                                    // console.log('CurrentURL = ', this.location.path());

                                    let nonAccessURL = this.location.path();

                                    // window.history.back();
                                    this.location.back();

                                    setTimeout(() => {
                                        // console.log('CurrentURL = ', this.location.path());

                                        if (this.location.path() == nonAccessURL) {
                                            this.router.navigate(['/']);
                                        }

                                        document.getElementById('app-container-main').style.display = '';
                                    }, 2000);

                                });
                            }

                            let data = {
                                menu: menu,
                                subMenu: subMenu
                            }

                            //   console.log(data);
                            data_response = data;
                            callback(Main, data_response);
                        }
                    }
                }
                this.auth.checkUpdateToken();
            }, error => {
                if (error.status == 401) {
                    //console.log("session time out : " + localStorage.getItem('msal.idtoken'));
                    console.log("session time out :\n" + this.auth.getAccessToken());

                    if (!this.isOpenDialogTimeout) {
                        this.isOpenDialogTimeout = true;
                        const dialogRef = this.dialogError.open(DialogSessionTimeoutComponent, {
                        }).afterClosed().subscribe(result => {
                            this.isOpenDialogTimeout = false;
                            this.auth.reLogin();
                        });
                    }
                }
            }
        );
    }

}
