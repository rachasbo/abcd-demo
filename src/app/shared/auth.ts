import { Injectable } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
//import { MsalService, BroadcastService } from '@azure/msal-angular';

import { environment } from '../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from '../modules/user-management/models/user-model.model';
// import { UserModel } from '../modules/master/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class Auth {

    // private service_url = "https://apiseaccrm-dev.apthai.com";
    private service_url = environment.api;

    private subscription: Subscription;

    private refreshTokenPeriod: number = 1000 * 60 * 10; // miliseconds

    //public updateLogin: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public updateLogin: BehaviorSubject<number> = new BehaviorSubject(-1);

    private user: UserModel;

    constructor(
        //private broadcastService: BroadcastService,
        private activatedRoute: ActivatedRoute,
        private httpClient: HttpClient,
        private router: Router
    ) {
        this.init();
    }

    init() {
        /*
        this.activatedRoute.queryParams.subscribe(params => {
            let auth_code = params['code'];
            if (auth_code) {
                console.log('auth_code:\n' + auth_code);

                if (this.getAuthCode() != auth_code) {
                    this.saveAuthCode(auth_code);

                    //let authen_url = 'http://localhost/seac_ad_api/auth.php?code=' + auth_code;
                    let authen_url = this.service_url + '/authen/v1/Auth/token/' + encodeURIComponent(auth_code) + '/uri/' + encodeURIComponent(environment.uri);

                    this.httpClient.get(authen_url, { 'headers': new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' }) }).subscribe(
                        response => {
                            let responseObj = JSON.parse(JSON.stringify(response));
                            console.log(responseObj);

                            if (responseObj.statusCode == '200') {
                                //console.log(responseObj.data);

                                this.saveAccessToken(responseObj.data.id_token);
                                this.saveRefreshToken(responseObj.data.refresh_token);

                                //window.location.href = environment.uri;

                                this.router.navigate(['/']);
                                this.loadUserData();
                            } else {
                                this.login();
                            }
                        }, error => {
                            console.log(error);
                        }
                    );
                }
            }
        });
        */

        /*
        if (this.isLogined() && !this.getUser()) {
            this.loadUserData();
        }
        */

        /*
        let main = this;

        setTimeout(function() {
            main.loadUserData();
        }, 2000);
        */

        if (this.isLogined()) {
            /*
            if (!this.getUser()) {
                this.loadUserData();
            }
            */
            this.updateLogin.next(1);
        } else {
            this.updateLogin.next(0);
        }
    }

    // public destroy() {
    //     if (this.subscription) {
    //         this.subscription.unsubscribe();
    //     }
    // }

    public isLogined() {
        let logined = false;

        if (this.getAccessToken() != null && this.getAccessToken() != '') {
            /*
            let jwtData = JSON.parse(atob(this.getAccessToken().split('.')[1]));
            //console.log('jwtData:\n', jwtData);

            let exp_date = jwtData.exp;
            //console.log('exp = ' + exp_date);

            let current_date = Math.floor(new Date().getTime() / 1000);
            //console.log('current = ' + current_date);

            if (exp_date > current_date) {  // token is not expired
                logined = true;
            }
            */

            logined = true;
        }

        return logined;
    }

    public login() {
        // window.location.href = environment.adAuthority + 'oauth2/v2.0/authorize?client_id=' + environment.adClientID + '&response_type=code&redirect_uri=' + encodeURIComponent(environment.uri) + '&response_mode=query&scope=' + encodeURIComponent(environment.adScope.join(' ')) + '&state=' + environment.adStateId;

        this.router.navigate(['/login']);
    }

    public reLogin() {
        this.login();
    }

    public logout() {
        // this.saveAccessToken('');
        this.clearAllLoginSessionData();

        window.location.href = '/';
    }

    public saveUserLogined(token: string) {
        this.updateLastLogin();
        this.saveAccessToken(token);
        this.loadUserData();
    }

    public loadUserData() {
        // let authen_url = this.service_url + '/authen/v1/Users/profile';
        let authen_url = this.service_url + '/v1/auth/user-profile';

        let headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            // 'Authorization': 'Bearer ' + this.getAccessToken()
            'Authorization': this.getAccessToken()
        });

        this.httpClient.get(authen_url, { 'headers': headers }).subscribe(
            response => {
                let responseObj = JSON.parse(JSON.stringify(response));
                console.log(responseObj);
                //console.log(responseObj.data);

                if (responseObj.statusCode == '200') {
                    //this.saveUserInfo(responseObj.data);

                    // this.user = new UserModel().deserialize(responseObj.data);

                    // this.user = new UserModel();
                    // this.user.id = responseObj.data.userCode;
                    // this.user.userName = responseObj.data.username;
                    // this.user.name = responseObj.data.username;
                    // this.user.roleId = responseObj.data.roleId;
                    // this.user.roleName = responseObj.data.roleName;

                    this.saveUser(responseObj.data);

                    this.updateLogin.next(1);
                } else {
                    this.updateLogin.next(0);
                }
            }, error => {
                console.log(error);

                this.updateLogin.next(0);

                if (error.status == 401) {

                }
            }
        );
    }

    private saveUser(response) {
        let userData = new UserModel();
        userData.id = response.userCode;
        userData.userName = response.username;
        userData.name = response.username;
        userData.roleId = response.roleId;
        userData.roleName = response.roleName;

        localStorage.setItem("user", btoa(JSON.stringify(userData)));
    }
    private loadUser(): UserModel {
        let userData = JSON.parse(atob(localStorage.getItem("user"))) as UserModel;

        return userData;
    }
    public getUser(): UserModel {
        if (!this.user) {
            this.user = this.loadUser();
        }

        return this.user;
    }

    private saveAuthCode(code: string) {
        localStorage.setItem("auth_code", code);
    }
    public getAuthCode() {
        return localStorage.getItem("auth_code");
    }

    private saveAccessToken(token: string) {
        localStorage.setItem("auth_token", token);

        console.log('### AccessToken:\n' + this.getAccessToken());

        this.updateLastLogin();
    }
    public getAccessToken() {
        let token = localStorage.getItem("auth_token");

        //console.log('getAccessToken:\n' + token);

        return token;
    }

    public setForcedChangePwd() {
        localStorage.setItem("forced_change_pwd", "yes");
    }
    public clearForcedChangePwd() {
        localStorage.removeItem("forced_change_pwd");
    }
    public isForcedChangePwd() {
        let isForced = false;

        let forced_change_pwd = localStorage.getItem("forced_change_pwd");

        if (forced_change_pwd=="yes") {
            isForced = true;
        }

        return isForced;
    }

    private saveRefreshToken(token: string) {
        localStorage.setItem("refresh_token", token);

        console.log('### RefreshToken:\n' + this.getRefreshToken());
    }
    public getRefreshToken() {
        return localStorage.getItem("refresh_token");
    }

    public clearAllLoginSessionData() {
        localStorage.removeItem("auth_code");
        localStorage.removeItem("auth_token");
        localStorage.removeItem("refresh_token");
        localStorage.removeItem("auth_lastLogin");
        localStorage.removeItem("user");
    }

    public refreshToken() {
        // let authen_url = this.service_url + '/authen/v1/Auth/refresh-token/' + encodeURIComponent(this.getRefreshToken()) + '/uri/' + encodeURIComponent(environment.uri);

        // this.httpClient.get(authen_url, { 'headers': new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' }) }).subscribe(
        //     response => {
        //         let responseObj = JSON.parse(JSON.stringify(response));
        //         console.log(responseObj);

        //         if (responseObj.statusCode == '200') {
        //             //console.log(responseObj.data);

        //             this.saveAccessToken(responseObj.data.id_token);
        //             this.saveRefreshToken(responseObj.data.refresh_token);

        //             this.loadUserData();
        //         }
        //     }, error => {
        //         console.log(error);
        //     }
        // );


        let url = this.service_url + '/v1/auth/refresh-token';

        let headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            // 'Authorization': 'Bearer ' + this.getAccessToken()
            'Authorization': this.getAccessToken()
        });

        this.httpClient.get(url, { 'headers': headers }).subscribe(
            response => {
                let responseObj = JSON.parse(JSON.stringify(response));
                console.log(responseObj);

                if (responseObj.statusCode == '200') {
                    //console.log(responseObj.data);
                    
                    // this.saveAccessToken(responseObj.data.id_token);
                    // this.saveRefreshToken(responseObj.data.refresh_token);

                    this.saveUserLogined(responseObj.data.token);
                }
            }, error => {
                console.log(error);
            }
        );
    }

    private updateLastLogin() {
        let auth_lastLogin = String(Date.now());

        localStorage.setItem("auth_lastLogin", auth_lastLogin);
    }

    public checkUpdateToken() {
        let main = this;

        setTimeout(function () {
            let auth_lastLogin = Number(localStorage.getItem("auth_lastLogin"));
            //console.log("auth_lastLogin = " + auth_lastLogin);
            //console.log("current_date = " + Date.now());
            //console.log("diff = " + (Date.now() - auth_lastLogin));
            //console.log("expire_time = " + main.refreshTokenPeriod);

            if (Date.now() - auth_lastLogin > main.refreshTokenPeriod) {
                console.log("checkUpdateToken : expired");

                main.updateLastLogin();

                if (main.isLogined()) {
                    main.refreshToken();
                }
            } else {
                //console.log("checkUpdateToken : not expire");
            }
            //}, Math.floor(Math.random() * 3000) + 1000);
        }, Math.floor(Math.random() * 5000) + 3000);
    }

}