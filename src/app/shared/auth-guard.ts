import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Auth } from 'src/app/shared/auth';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private auth: Auth,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {

    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let actived = false;
        
        if (this.auth.isLogined()) {
            if (this.auth.isForcedChangePwd()) {
                // this.activatedRoute.url.subscribe(url => console.log(url));
                // console.log(this.activatedRoute);

                actived = true;

                this.router.navigate(['/change_pwd']);
                // window.location.href = '/change_pwd';
            } else {
                actived = true;
            }
        } else {
            //this.auth.login();
            this.router.navigate(['/']);
            // window.location.href = '/';
        }

        return actived;
    }
}