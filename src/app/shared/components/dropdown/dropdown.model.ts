import { CoreModel } from '../../models/core.model';

export class DropdownModel extends CoreModel {
    public value : number;
    public label : string;
}

export class DropdownsModel extends CoreModel {
    data : Array<DropdownModel>;
}