import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { of, Subject } from 'rxjs';
import { Service } from 'src/app/shared/service';
import { DropdownModel, DropdownsModel } from './dropdown.model';


@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})

export class DropdownComponent implements OnInit, OnChanges {
  form: FormGroup;
  orders;
  orderControl = new FormControl('', [Validators.required]);
  selectedValue: string = "";
  id: number = 0;
  dropdownInfo: Array<DropdownModel>;

  @Input('value') valueOrder: number;
  @Input('form') formOrder: Array<Object>;
  @Input('disabled') disable: boolean;
  @Input('url') urlForm: string;
  @Input('default') defaultValue: string;
  @Input('required') orderRequire: string;
  @Input('errorMessage') errorMessage: String;

  @Input() inputModel: any;
  @Output() inputModelChange = new EventEmitter<string>();

  @Output('onValue') onValue = new EventEmitter();

  @Input('childsMarkAsTouchedTrigger') childsMarkAsTouchedTrigger: Subject<boolean>;

  constructor(private formBuilder: FormBuilder,
    private dataService: Service) {
    this.form = this.formBuilder.group({
      order: ['']
    });
  }

  submitValue(event) {
    let dropdownValue = this.dropdownInfo.filter(item => {
      return item.value == event.value
    })
    event.dropdownValue = dropdownValue;
    this.onValue.emit(event);


    this.form.controls.order.patchValue(event.value);
    // console.log("select",this.selectedValue);
  }

  ngOnChanges(value: any) {
    //console.log("value", value);
    if (value.urlForm) {

      this.setDefault();
    }
  }

  setDefault() {
    if (this.urlForm != null && this.urlForm != "") {

      this.dataService.sendGetRequest(this.urlForm, this, function (main, data) {
        if (data != null) {
          main.getDataByService(data);
        }
      }, true);

    } else {
      this.orders = this.formOrder;
    }


    // async orders
    of(this.orders).subscribe(order => {
      this.orders = order;
      this.form.controls.order.patchValue(this.valueOrder);
      var event: any = {};
      event.label = this.valueOrder;
      // this.onValue.emit(event);
    });
  }

  ngOnInit() {
    // this.dataService.sendGetRequest(this.urlForm).subscribe((data: any[])=>{
    //   console.log("dataService",data);
    // })

    if (this.urlForm != null && this.urlForm != "") {
      this.dataService.sendGetRequest(this.urlForm, this, function (main, data) {
        if (data != null) {
          main.getDataByService(data);
        }
      }, true);
    } else {
      this.orders = this.formOrder;
    }


    // async orders
    of(this.orders).subscribe(order => {
      console.log('in sub', order)
      this.orders = order;
      this.form.controls.order.patchValue(this.valueOrder);
      var event: any = {};
      event.label = this.valueOrder;
      event.dropdownValue = 'order';
      console.log('in sub', order, event)
      // this.onValue.emit(event);
    });

    if (this.childsMarkAsTouchedTrigger) {
      this.childsMarkAsTouchedTrigger.subscribe(() => {
        this.form.get('order').markAsTouched();
        // this.form.controls.order.markAsTouched();

        // console.log('DropDown: markAsTouched');
        // console.log('value =', this.form.get('order').value);
      })
    }
  }

  getDataByService(response) {
    this.dropdownInfo = [];
    this.dropdownInfo = JSON.parse(JSON.stringify(response.data));
    this.orders = this.dropdownInfo;
  }

  checkValidate() {
    console.log(this.valueOrder);
    this.form.get('order').markAsTouched();

    return !this.form.get('order').hasError('required');
  }
}
