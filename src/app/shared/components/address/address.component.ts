import { DropdownComponent } from 'src/app/shared/components/dropdown/dropdown.component';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Service } from '../../service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { InputFieldComponent } from '../input-field/input-field.component';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

  urlProvince = "";
  urlDistrict = "";
  urlSubDistrict = "";

  defualtZipCode = "";
  defualtCountry;
  defualtProvince;
  defualtDistrict;
  defualtSubDistrict;

  form: FormGroup;

  @ViewChild('Country') countryDrop: DropdownComponent;

  @Output('onValue') onValue: EventEmitter<string> = new EventEmitter<string>();

  @Input('inputCountry') inputCountry: number;
  @Input('inputProvince') inputProvince: number;
  @Input('inputDistrict') inputDistrict: number;
  @Input('inputSubDistrict') inputSubDistrict: number;
  @Input('inputZipCode') inputZipCode: string;

  @Input('hideHeader') hideHeader: boolean;

  @Input() zipCode: any;
  @Output() zipCodeChange = new EventEmitter<string>();
  public regNumber = /^[0-9]+$/;

  constructor(private dataService: Service, private formBuilder: FormBuilder) { }
  

  checkValidate() {
    this.form.get('zipCode').markAsTouched();
    return !this.form.get('zipCode').hasError('pattern');
  }

  onValueDropCountry(event) {
    // console.log("onValueDropCountry",event);
    let country = event.value;

    this.urlProvince = "/master/v1/Master/province?Limit=0&CountryId=" + country;

    this.inputProvince = 0;
    this.inputDistrict = 0;
    this.inputSubDistrict = 0;
    this.inputZipCode = "";
    if (event.value == 0 || event.value == undefined) {
      this.urlSubDistrict = "/master/v1/Master/sub-district?Limit=0&DistrictId=0";
      this.urlDistrict = "/master/v1/Master/district?Limit=0&ProvinceId=0";
    }
    this.countryDrop.checkValidate();
    // console.log("countryDrop",this.countryDrop.checkValidate());

    let obj: any = {};
    obj.id = "country";
    obj.value = country;
    this.onValue.emit(obj);
  }

  onValueDropProvince(event) {
    // console.log("onValueDropProvince",event);
    let province = event.value;

    this.inputDistrict = 0;
    this.inputSubDistrict = 0;
    this.inputZipCode = "";
    this.urlDistrict = "/master/v1/Master/district?Limit=0&ProvinceId=" + province;
    if (event.value == 0 || event.value == undefined) {
      this.urlSubDistrict = "/master/v1/Master/sub-district?Limit=0&DistrictId=0";

    }

    let obj: any = {};
    obj.id = "province";
    obj.value = province;
    this.onValue.emit(obj);
  }

  onValueDropDistrict(event) {
    let district = event.value;
    this.urlSubDistrict = "/master/v1/Master/sub-district?Limit=0&DistrictId=" + district;
    // if (event.value == 0 || event.value == undefined) {
      this.inputSubDistrict = 0;
      this.inputZipCode = "";
    // }

    let obj: any = {};
    obj.id = "district";
    obj.value = district;
    this.onValue.emit(obj);
  }

  onValueDropSubDistrict(event) {

    let subDistrict = event.value;

    // if (event.value == 0 || event.value == undefined) {
      this.inputZipCode = "";
    // }

    let obj: any = {};
    obj.id = "subDistrict";
    obj.value = subDistrict;
    this.onValue.emit(obj);

    this.dataService.sendGetRequest("/master/v1/Master/zipcode?SubDistrictId=" + subDistrict, this, function (main, data) {
      if (data != null) {
        main.inputZipCode = data.data.zipcode;
        main.zipCode = data.data.zipcode;
      }
    });
  }

  ngOnInit() {
    this.form = new FormGroup({
      zipCode: new FormControl()
    });
    // this.defualtZipCode = this.inputZipCode;
    this.zipCode = this.inputZipCode;
    // this.urlProvince = "/master/v1/Master/province?Limit=0&CountryId="+this.inputCountry;
    // this.urlDistrict = "/master/v1/Master/district?Limit=0&ProvinceId="+this.inputProvince;
    // this.urlSubDistrict = "/master/v1/Master/sub-district?Limit=0&DistrictId="+this.inputDistrict;
  }

  ngOnChanges(value: any) {
    // console.log("value",value);
    if (value.inputCountry) {
      if (value.inputCountry.currentValue) {
        if (value.inputCountry.currentValue != 0) {
          this.urlProvince = "/master/v1/Master/province?Limit=0&CountryId=" + value.inputCountry.currentValue;
        }
      }
    }

    if (value.inputProvince) {
      if (value.inputProvince.currentValue) {
        if (value.inputProvince.currentValue != 0) {
          this.urlDistrict = "/master/v1/Master/district?Limit=0&ProvinceId=" + value.inputProvince.currentValue;
        }
      }
    }
    if (value.inputDistrict) {
      if (value.inputDistrict.currentValue) {
        if (value.inputDistrict.currentValue != 0) {
          this.urlSubDistrict = "/master/v1/Master/sub-district?Limit=0&DistrictId=" + value.inputDistrict.currentValue;
        }
      }
    }

  }


}
