import { DialogErrorComponent } from './../dialog-error/dialog-error.component';
import { Files } from './../../models/upload-file.model';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter, OnChanges, Injectable } from '@angular/core';
import { Service } from 'src/app/shared/service';
import { MatDialog } from '@angular/material';

export interface FileData {
  fileName: String;
  link: String;
}

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})

export class UploadFileComponent implements OnInit, OnChanges {

  @Input("isMulti") isMulti: boolean;
  @Input("filesList") filesList: Array<Files>;
  @Input("canDeleteOld") canDeleteOld: boolean = true;
  @Input("uploadUrl") uploadUrl: string;
  @Input("deleteUrl") deleteUrl: string;
  @Input('canUpload') canUpload: boolean;
  @Input('canDownLoad') canDownload: boolean;
  @Input('appenedName') appenedName: string = '';
  @Input('isOneFileAttach') isOneFileAttach: boolean;
  @Input('maxSize') maxSize: number;
  @Input('disabled') disabled: boolean = false;
  @Output('showLoading') showLoading = new EventEmitter();
  @ViewChild('inputFile') inputFile;

  cantDelListSize: number;
  constructor(private dataService: Service, private dialog: MatDialog) { }

  ngOnInit() {
    if (!this.canDeleteOld) {
      this.cantDelListSize = this.filesList.length;
    }
  }

  ngOnChanges(value) {
    // console.log(value);
    // if(value.uploadUrl){
    //   this.uploadUrl = value.uploadUrl.currentValue;
    // }
  }

  onDownLoadFile(fileId: number) {
    this.showLoading.emit(true);

    this.dataService.sendGetRequest('/master/v1/File/' + fileId, this, function (main, response) {
      // console.log(response);
      if (response != null) {
        let link = document.createElement('a');
        link.setAttribute('type', 'hidden');
        link.href = response.data;
        link.download = "file";
        document.body.appendChild(link);
        link.click();
        link.remove();
      }
      main.showLoading.emit(false);

    });
  }

  onFileSelected(event) {
    console.log(event);
    if (event.target.files.length > 0) {
      this.showLoading.emit(true);
      let fileListSize = event.target.files.length;
      for (let i = 0; i < event.target.files.length; i++) {
        console.log(event.target.files[i]);
        if (event.target.files.item(i).size > this.maxSize) {
          this.dialog.open(DialogErrorComponent, {
            data: "Maximum upload file size : " + this.maxSize / (1024 * 1024) + " MB."
          });
          this.showLoading.emit(false);
          return false;
        } else {
          const formData: FormData = new FormData();
          formData.append(this.appenedName, event.target.files[i]);

          this.dataService.sendPostRequestMultipart(this.uploadUrl, formData, this, function (main, response) {
            console.log(response);

            main.inputFile.nativeElement.value = '';

            if (response != null) {
              if (response.statusCode == 200 && response.data != null) {
                if (main.isOneFileAttach) {
                  main.fileList = [];
                }
                let data: Files = JSON.parse(JSON.stringify(response.data));
                let file = new Files();
                file.fileName = data.fileName;
                file.fileId = data.fileId;
                main.filesList.push(file);
                console.log(main.filesList);
              }
            } else {
              main.showLoading.emit(false);
            }

            if (i === fileListSize - 1) {
              main.showLoading.emit(false);
            }

          });
        }
      }
    }
  }

  onDelete(fileId: number) {
    console.log('fileId', fileId);
    this.showLoading.emit(true);
    this.dataService.sendDeleteRequest('/master/v1/File/' + fileId, null, this, function (main, response) {
      console.log(response);
      if (response != null) {
        if (response.statusCode == 200) {
          const index = main.filesList.findIndex(item => item.fileId === fileId);
          main.filesList.splice(index, 1);
        }
      }
      main.showLoading.emit(false);
    }, false);
  }
}
