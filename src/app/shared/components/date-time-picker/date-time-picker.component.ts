import { Component, OnInit, Output, EventEmitter, Input, OnChanges, ViewChildren } from '@angular/core';
import { MatDatepicker, ThemePalette } from '@angular/material';
import { DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter, MatDatepickerInputEvent, MAT_DATE_LOCALE } from '@angular/material';
import { FormControl, Validators, FormGroup, FormBuilder, ValidationErrors } from '@angular/forms';
import * as moment from 'moment'; // add this 1 of 4
import { AppDateAdapter, APP_DATE_FORMATS } from './date.adapter';
import { of, Subject } from 'rxjs';
import { containsElement } from '@angular/animations/browser/src/render/shared';

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class DateTimePickerComponent implements OnInit, OnChanges {

  @Input('placeHolder') placeHolder: string;
  // @Input('defaultValue') defaultValue:Date;
  @Input('id') id: string;
  @Input('value') value: Date;
  @Input('required') required: boolean = false;
  @Input('errorMessage') errorMessage: String;
  @Input() dateModel: any;
  @Input('minDate') minDate: Date;
  @Input('maxDate') maxDate: Date;
  @Input('disabled') disabled: boolean;
  @Input('childsMarkAsTouchedTrigger') childsMarkAsTouchedTrigger: Subject<boolean>;
  @Output('onValue') onValue = new EventEmitter();
  @Output() dateModelChange = new EventEmitter<string>();
  
  @Input('hideTime') hideTime: boolean = false;
  disableMinute: boolean = false;
  @Input('showSpinners') showSpinners: boolean = true;
  @Input('showSeconds') showSeconds: boolean = true;
  @Input('stepHour') stepHour: number = 1;
  @Input('stepMinute') stepMinute: number = 1;
  @Input('stepSecond') stepSecond: number = 1;
  @Input('enableMeridian') enableMeridian: boolean = false;
  color: ThemePalette;
  touchUi: boolean = false;
  

  date;

  custom_touched: Boolean = false;

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {

  }

  ngOnInit() {
    // if (this.required) {
    //   this.form = this.formBuilder.group({
    //     dateFieldValue: [this.value, Validators.required],
    //     dateField: [this.value]
    //     //dateField: [this.value, this.myDateValidate]
    //   })
    // } else {
    //   this.form = this.formBuilder.group({
    //     dateFieldValue: [this.value],
    //     dateField: [this.value]
    //   })
    // }

    this.form = this.formBuilder.group({
      dateFieldValue: [this.value],
      dateField: [this.value]
    })

    if (this.required) {
      this.form.get('dateFieldValue').setValidators(Validators.required);
      this.form.get('dateFieldValue').updateValueAndValidity();

      // if (this.placeHolder.indexOf('*') == -1) {
      //   if (this.placeHolder && this.placeHolder != '') {
      //     this.placeHolder = this.placeHolder + '*';
      //   }
      // }
    }

    if (this.disabled) {
      this.form.disable();
    }

    // this.form = this.formBuilder.group({
    //   dateField: [this.value]
    // });

    // async date
    of(this.date).subscribe(value => {
      this.date = value;
      this.form.controls.dateFieldValue.patchValue(this.value);
      this.form.controls.dateField.patchValue(this.value);
      var event: any = {};
      event.value = this.value;
    });

    if (this.childsMarkAsTouchedTrigger) {
      this.childsMarkAsTouchedTrigger.subscribe(() => {
        // console.log(this.form.value);

        this.form.get('dateFieldValue').markAsTouched();

        this.custom_touched = true;

        /*
        console.log(this.form.get('dateField'));

        console.log("value = ", this.form.get('dateField').value);

        if (this.form.invalid)  {
          console.log('datepicker => invalid');

          //console.log(this.form.get('dateField').errors);

          if (this.form.get('dateField').errors.required) {
            console.log('found error required');
          } else {
            console.log('not found error required');
          }

          const controlErrors: ValidationErrors = this.form.get('dateField').errors;
          if (controlErrors !== null) {
            Object.keys(controlErrors).forEach(keyError => {
              console.log(keyError, controlErrors[keyError]);
            });
          }
        } else {
          console.log('datepicker => valid');
        }
        */
      })
    }
  }

  /*
  myDateValidate(control: FormControl) {
    if (!(control.value instanceof Date && !isNaN(control.value.getTime()))) {
      return {
        'required': true
      }
    }

    return null;
  }
  */

  ngOnChanges(value: any) {
    //console.log("value",value);

    // if (this.required) {
    //   this.form = this.formBuilder.group({
    //     dateFieldValue: [this.value, Validators.required],
    //     dateField: [this.value]
    //   })
    // } else {
    //   this.form = this.formBuilder.group({
    //     dateFieldValue: [this.value],
    //     dateField: [this.value]
    //   })
    // }

    // if(this.disabled){
    //   this.form.disable();
    // }
    if (!this.form) {
      this.form = this.formBuilder.group({
        dateFieldValue: [this.value],
        dateField: [this.value]
      })
    }

    if (value.required) {
      if (!this.form) {
        this.form = this.formBuilder.group({
          dateFieldValue: [this.value],
          dateField: [this.value]
        })
      }

      if (this.required) {
        this.form.get('dateFieldValue').setValidators(Validators.required);

        // if (this.placeHolder.indexOf('*') == -1) {
        //   if (this.placeHolder && this.placeHolder != '') {
        //     this.placeHolder = this.placeHolder + ' *';
        //   }
        // }
      } else {
        this.form.get('dateFieldValue').setValidators(null);
      }

      this.form.get('dateFieldValue').updateValueAndValidity();
    } else if (value.disabled) {
      if (this.disabled) {
        this.form.disable();
      } else {
        this.form.enable();
      }
    }


    // this.form = this.formBuilder.group({
    //   dateField: [this.value]
    // })

    // async date
    of(this.date).subscribe(value => {
      this.date = value;
      this.form.controls.dateFieldValue.patchValue(this.value);
      this.form.controls.dateField.patchValue(this.value);
      var event: any = {};
      event.value = this.value;
    });
  }

  onDateChange(date: any) {
    console.log("Moment:", moment(date.value).format('YYYY-MM-DD\THH:mm:ss'));
    // console.log(date.value);
    let day = date.value.getDate();
    let month = date.value.getMonth() + 1;
    let year = date.value.getFullYear();
    // console.log(`${year}-${month}-${day}`);

    this.form.get('dateFieldValue').setValue(moment(date.value).format('YYYY-MM-DD\THH:mm:ss'));

    this.dateModelChange.emit(moment(date.value).format('YYYY-MM-DD\THH:mm:ss'));

    this.onValue.emit({
      value: moment(date.value).format('YYYY-MM-DD\THH:mm:ss'),
      label: '',
      day: day,
      month: month,
      year: year
    });
  }

  checkValidate() {
    // console.log('checkValidate', this.form.get('dateFieldValue'));
    // if (this.form.get('dateFieldValue').hasError('required')) {
    //   console.log('required = true');
    // } else {
    //   console.log('required = false');
    // }

    // this.form.get('dateField').markAsTouched();
    // return !this.form.get('dateField').hasError('required');

    this.form.get('dateFieldValue').markAsTouched();
    return !this.form.get('dateFieldValue').hasError('required');
  }

}
