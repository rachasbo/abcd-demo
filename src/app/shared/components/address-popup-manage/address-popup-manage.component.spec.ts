import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressPopupManageComponent } from './address-popup-manage.component';

describe('AddressPopupManageComponent', () => {
  let component: AddressPopupManageComponent;
  let fixture: ComponentFixture<AddressPopupManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressPopupManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressPopupManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
