import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Service } from '../../service';

@Component({
  selector: 'app-address-popup-manage',
  templateUrl: './address-popup-manage.component.html',
  styleUrls: ['./address-popup-manage.component.scss']
})
export class AddressPopupManageComponent implements OnInit {

  public onClose: BehaviorSubject<string> = new BehaviorSubject('');

  title: string;
  enableUploadFile: boolean;

  form: FormGroup;
  
  constructor(
    private service: Service,
    private builder: FormBuilder
  ) {
    
  }

  ngOnInit() {

  }

  public setData(title: string, enableUploadFile: boolean) {
    this.title = title;
    this.enableUploadFile = enableUploadFile;

    this.createFormData();
  } 

  public createFormData() {
    this.form = this.builder.group({
      //requestApproveNote: [this.dealPriceInfo.requestApproveNote]
      address1: [],
      address2: []
    });
  }

  public onAddressChange(value) {

  }

  public onCancel() {
    this.onClose.next('close');
  }

  public onSave() {
    console.log(this.form.value);

    let body = this.form.value;
    console.log(body);

    this.onClose.next('save');
  }

}
