import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef , MatDialog, MAT_DIALOG_DATA} from '@angular/material';
// import { Auth } from '../../auth';

@Component({
  selector: 'app-dialog-session-timeout',
  templateUrl: './dialog-session-timeout.component.html',
  styleUrls: ['./dialog-session-timeout.component.scss']
})
export class DialogSessionTimeoutComponent implements OnInit{
  constructor(
    public dialogRef: MatDialogRef<DialogSessionTimeoutComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) { }

  onOKDialog(): void{
    this.dialogRef.close();
    // this.auth.login();
  }

  ngOnInit() {
    // console.log("this.data", this.data);
  }
}