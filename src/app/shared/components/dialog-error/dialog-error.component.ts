import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef , MatDialog, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-dialog-error',
  templateUrl: './dialog-error.component.html',
  styleUrls: ['./dialog-error.component.scss']
})
export class DialogErrorComponent implements OnInit{
  constructor(
    public dialogRef: MatDialogRef<DialogErrorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) { }

  onOKDialog(): void{
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log("this.data", this.data);
  }
}
