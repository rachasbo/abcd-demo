import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-dynamic-confirm',
  templateUrl: './dialog-dynamic-confirm.component.html',
  styleUrls: ['./dialog-dynamic-confirm.component.scss']
})
export class DialogDynamicConfirmComponent implements OnInit {
  public txtSubmit: string = 'OK';
  public txtCancel: string = 'Cancel';
  public description: string;
  public title: string;
  constructor(
    public dialogRef: MatDialogRef<DialogDynamicConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (this.data.txtSubmit) {
      this.txtSubmit = this.data.txtSubmit;
    }
    if (this.data.txtCancel) {
      this.txtCancel = this.data.txtCancel;
    }
    this.description = this.data.description;
    this.title = this.data.title;
  }

  ngOnInit() {
  }


  onClose(): void {
    this.dialogRef.close({ status: false })
  }

  onSubmit(): void {
    this.dialogRef.close({ status: true })
  }

}
