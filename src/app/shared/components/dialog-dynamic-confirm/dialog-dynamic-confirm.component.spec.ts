import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDynamicConfirmComponent } from './dialog-dynamic-confirm.component';

describe('DialogDynamicConfirmComponent', () => {
  let component: DialogDynamicConfirmComponent;
  let fixture: ComponentFixture<DialogDynamicConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogDynamicConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDynamicConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
