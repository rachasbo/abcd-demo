import { Component, ElementRef, ViewChild, OnInit, EventEmitter, Output, Input, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { MatOption } from '@angular/material';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER, TAB } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Service } from 'src/app/shared/service';

interface IOption {
  value: number;
  label: string;
}
interface IOptionFormated {
  value: number;
  label: string;
  label_short: string;
}

@Component({
  selector: 'app-multi-select-autocomplete',
  templateUrl: './multi-select-autocomplete.component.html',
  styleUrls: ['./multi-select-autocomplete.component.scss']
})
export class MultiSelectAutocompleteComponent implements OnInit, OnChanges {

  @Input('fieldName') fieldName: string;
  @Input('optionAllTitle') optionAllTitle: string;
  @Input('defaultValue') defaultValue: number[] = [];
  @Input('errorMessage') errorMessage: string = 'error message';
  @Input('required') required: string;
  @Input('url') url: string;
  @Input('id') id: string;
  @Input('options') defaultOptions: any;
  @Input('limit') limit: number = 50;
  @Input('filterMode') filterMode: string = 'left'; // left, any
  @Input('maxSelected') maxSelected: number;
  @Input('appearance') appearance: string;
  @Output('onValueUpdate') onValueUpdate = new EventEmitter();
  @Input('disabled') disabled: boolean = false;

  // @ViewChild('optionAll') private optionAll: MatOption;
  // private optionOne: MatOption;

  // form: FormGroup;

  // ###########################################################################

  // visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA, TAB];
  InputForm = new FormControl();
  isDataLoaded = false;

  optionsFiltered: Observable<IOption[]>;
  optionsSelected: number[] = [];
  options: IOption[] = [];

  @ViewChild('InputElement') InputElement: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    // private builder: FormBuilder,
    private dataService: Service
  ) {
    // this.filteredFruits = this.InputForm.valueChanges.pipe(
    //     startWith(null),
    //     map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
  }

  ngOnInit() {
    this.loadOptions();

    // console.log('defaultValue = ', this.defaultValue);

    this.optionsSelected = this.defaultValue;
  }

  ngOnChanges(value: any) {
    if (value.defaultValue && value.defaultValue.previousValue != value.defaultValue.currentValue) {
      this.optionsSelected = this.defaultValue;
    }

    //เพิ่มใหม่เรื่อง Disabled
    if (value.disabled) {
      if (value.disabled.previousValue != value.disabled.currentValue) {
        // console.log("test",this.disabled);
        this.disabled = value.disabled.currentValue;

      console.log('dis', this.disabled)
        // this.createFormGroup();
      }
    }
  }

  private loadOptions() {
    if (this.url !== undefined && this.url != '') {

      // this.url = this.url.replace('Limit=0', 'Limit=10');

      // console.log(this.url);
      this.dataService.sendGetRequest(this.url, this, function (main, data) {
        // console.log("response :", data);

        if (data != null) {
          main.options = data.data;

          // let foundAllSelected = true;
          // for (var i = 0; i < main.options.length; i++) {
          //   if (main.defaultValue.indexOf(main.options[i].value) == -1) {
          //     foundAllSelected = false;
          //   }
          // }

          // if (foundAllSelected) {
          //   main.defaultValue.push(0);
          // }

          main.renderOptions();
        }
      });
    } else if (this.defaultOptions !== undefined) {
      this.options = this.defaultOptions;
    }
  }

  private renderOptions() {
    setTimeout(() => {
      this.optionsFiltered = this.InputForm.valueChanges.pipe(
        // startWith(this.defaultValue),
        // startWith(this.getOptionByValue(this.defaultValue)),
        startWith(null),
        map(value => this.filtered(value))
      )

      this.isDataLoaded = true;
    }, 100);
  }

  public addOptionBySelect(event: MatAutocompleteSelectedEvent): void {
    let option: IOption = event.option.value;
    // console.log('selected_option =', option);

    if (this.maxSelected) {
      if ((this.optionsSelected.length < this.maxSelected) && (option.value > 0)) {
        this.optionsSelected.push(option.value);
      }
    } else {
      if (option.value > 0) {
        this.optionsSelected.push(option.value);
      }
    }


    this.InputElement.nativeElement.value = '';
    this.InputForm.setValue(null);

    this.updateValue();
  }

  public addOptionByInput(event: MatChipInputEvent): void {
    // console.log('add_option =', event);

    const input = event.input;
    let value = event.value.trim();
    value = (value || '');

    // Search & Add option
    // if ((value || '').trim()) {
    if (value != '') {
      // this.fruits.push(value.trim());
      let filtered_options: IOption[] = this.filtered(value);

      if (this.maxSelected) {
        if ((this.optionsSelected.length < this.maxSelected) && (filtered_options && filtered_options.length > 0)) {
          this.optionsSelected.push(filtered_options[0].value);
        }
      } else {
        if (filtered_options && filtered_options.length > 0) {
          this.optionsSelected.push(filtered_options[0].value);
        }
      }

    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.InputForm.setValue(null);

    this.updateValue();
  }

  public removeOption(value: number): void {
    const index = this.optionsSelected.indexOf(value);

    if (index >= 0) {
      this.optionsSelected.splice(index, 1);
    }

    this.updateValue();
  }

  // private _filter(value: string): string[] {
  //   const filterValue = value.toLowerCase();

  //   return this.allFruits.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  // }

  private filtered(value: any): IOption[] {
    // console.log('filtered_value = ', value, typeof value);

    var filteredOptions: IOption[] = [];

    // console.log('value == undefined', value == undefined);
    // console.log('value == null', value == null);
    // console.log('value == ""', (typeof value === 'string' && value == ''));
    // console.log('value == 0', (typeof value === 'number' && value == 0));
    // console.log('value.value == 0', (typeof value === 'object' && value.value == 0));

    if (value == undefined || value == null || (typeof value === 'string' && value == '') || (typeof value === 'number' && value == 0) || (typeof value === 'object' && value.value == 0)) {
      // console.log('step1');
      filteredOptions = this.options;

      // if (this.defaultValue > 0 && ((typeof value === 'string' && value == '') || (typeof value === 'object' && value.value == 0))) {
      //   // this.form.get('inputField').setValue(this.getOptionByValue(0));
      //   // this.onValueChange(true);
      //   this.defaultValue = 0;
      // }
    } else {
      // console.log('step2');
      if (typeof value === 'string') {
        value = value.toLowerCase().trim();

        if (this.filterMode == 'any') {
          filteredOptions = this.options.filter(option => option.label.toLowerCase().includes(value));
        } else if (this.filterMode == 'left') {
          filteredOptions = this.options.filter(option => option.label.toLowerCase().indexOf(value) == 0);
        }
      } else if (typeof value === 'number') {
        filteredOptions = this.options.filter(option => option.value == value);
      } else if (typeof value === 'object') {
        filteredOptions = this.options.filter(option => option.value == value.value);
      }
    }

    if(!filteredOptions){
      filteredOptions = [];
    }

    if (filteredOptions.length == 0) {
      filteredOptions = [{
        label: "No Matches",
        // value: ""
        value: 0
      }];
    } else {
      filteredOptions = filteredOptions;
      // filteredOptions = filteredOptions.slice(0, 10);
    }

    if (this.limit > 0) {
      filteredOptions = filteredOptions.slice(0, this.limit);
    }
    // console.log('limit=', this.limit);
    // console.log('filteredOptions(length)=', filteredOptions.length);

    return filteredOptions;
  }

  public getOptionByValue(value: number): IOptionFormated {
    let option: IOptionFormated = {
      value: 0,
      label: '',
      label_short: ''
    };

    if (value > 0) {
      let filteredOptions = this.options.filter(option => option.value == value);
      if (filteredOptions.length > 0) {
        let this_option = filteredOptions[0];

        option.value = this_option.value;
        option.label = this_option.label;
        option.label_short = this.genNameShort(this_option.label);
      }
    }

    return option;
  }

  private genNameShort(name: string) {
    let name_short = name;

    if (name_short.indexOf('/') > -1) {
      let temp_arr = name_short.split('/');
      name_short = temp_arr[0].trim();
    }

    name_short = name_short.replace("บริษัท", "");
    // name_short = name_short.replace("จำกัด","");
    name_short = name_short.replace("(มหาชน)", "");
    name_short = name_short.replace("จำกัด มหาชน", "");
    name_short = name_short.replace("..", ".");
    name_short = name_short.replace("'s", "");

    if (name_short.toLowerCase().startsWith("บ.")) {
      name_short = name_short.replace("บ.", "");
    }

    while (name_short.indexOf('  ') > -1) name_short = name_short.replace("  ", " ");
    name_short = name_short.trim();

    name_short = name_short.replace(".,", "");
    name_short = this.ireplace("Ltd.", "Ltd", name_short);
    name_short = this.ireplace("Limited.", "Ltd", name_short);
    name_short = this.ireplace("Part.", "Part", name_short);
    name_short = this.ireplace("Corporation Ltd", "Co Ltd", name_short);
    name_short = this.ireplace("Corp Ltd", "Co Ltd", name_short);
    name_short = this.ireplace("Corporation Company Limited", "", name_short);

    name_short = this.ireplace("Public Co", "Co", name_short);
    name_short = this.ireplace("Pub Co", "Co", name_short);
    name_short = this.ireplace("Co Ltd", "CoLtd", name_short);
    name_short = this.ireplace("CoLtd", "", name_short);
    name_short = this.ireplace("Ltd Part", "LtdPart", name_short);
    name_short = this.ireplace("LtdPart", "", name_short);
    name_short = this.ireplace("Pcl.", "", name_short);
    name_short = this.ireplace("Inc.", "", name_short);

    name_short = this.ireplace("Group Company Limited", "", name_short);
    name_short = this.ireplace("Industry Company Limited", "", name_short);
    name_short = this.ireplace("Public Company Limited", "", name_short);
    name_short = this.ireplace("Company Limited", "", name_short);
    name_short = this.ireplace("CompanyLimited", "", name_short);

    name_short = this.ireplace("(Thailand)", "", name_short);
    name_short = this.ireplace("[Thailand]", "", name_short);
    name_short = this.ireplace("(Thai)", "", name_short);
    name_short = this.ireplace("[Thai]", "", name_short);
    name_short = name_short.replace("(ประเทศไทย)", "");
    name_short = name_short.replace("[ประเทศไทย]", "");
    name_short = name_short.replace("(ไทยแลนด์)", "");
    name_short = name_short.replace("[ไทยแลนด์]", "");
    name_short = name_short.replace("(ไทย)", "");
    name_short = name_short.replace("[ไทย]", "");
    name_short = name_short.replace("ห้างหุ้นส่วนจำกัด", "");

    if (name_short.toLowerCase().endsWith(" ltd")) {
      name_short = this.ireplace(" ltd", "", name_short);
    }
    if (name_short.toLowerCase().endsWith(" limited")) {
      name_short = this.ireplace(" limited", "", name_short);
    }
    if (name_short.toLowerCase().endsWith(" pcl")) {
      name_short = this.ireplace(" pcl", "", name_short);
    }
    if (name_short.toLowerCase().endsWith(" company")) {
      name_short = this.ireplace(" company", "", name_short);
    }
    if (name_short.toLowerCase().endsWith(" จำกัด")) {
      name_short = name_short.replace(" จำกัด", "");
    }

    name_short = name_short.replace("'", "");
    name_short = name_short.trim();

    return name_short;
  }

  private ireplace(rep: string, rby: string, value: string) {
    var pos = value.toLowerCase().indexOf(rep.toLowerCase());
    return pos == -1 ? value : value.substr(0, pos) + rby + value.substr(pos + rep.length);
  }

  public updateValue() {
    let selected_value = this.optionsSelected;

    selected_value = selected_value.filter(item => item !== 0);

    // console.log(selected_value);

    let result = {
      value: selected_value,
      id: this.id
    }
    this.onValueUpdate.emit(result);
  }

  public checkValidate() {
    // this.form.get('inputField').markAsTouched();

    // return !this.form.get('inputField').hasError('required');
    return false;
  }
}