import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Service } from '../../service';
import { MatDialog } from '@angular/material';
import { DialogWarningComponent } from '../dialog-warning/dialog-warning.component';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';

export interface IAddressFreeFormResult {
  action: string;
  type: string;
  data: IAddressFreeFormAddr;
  file: File
}
export interface IAddressFreeFormAddr {
  isHeadOffice: boolean;
  // branchNo: number;
  branchNo: string;
  addrEn: string;
  addrTh: string;
  subDistrictNameEn: string;
  subDistrictNameTh: string;
  districtNameEn: string;
  districtNameTh: string;
  provinceNameEn: string;
  provinceNameTh: string;
  countryNameEn: string;
  countryNameTh: string;
  zipcode: string;
  files: IAddressFreeFormFile[];
}
export interface IAddressFreeFormFile {
  fileId: number;
  fileName: string;
}

@Component({
  selector: 'app-address-freeform',
  templateUrl: './address-freeform.component.html',
  styleUrls: ['./address-freeform.component.scss']
})
export class AddressFreeFormComponent implements OnInit {

  // public onClose: BehaviorSubject<string> = new BehaviorSubject('');

  address: IAddressFreeFormAddr = {
    isHeadOffice: false,
    // branchNo: 0,
    branchNo: '',
    addrEn: '',
    addrTh: '',
    subDistrictNameEn: '',
    subDistrictNameTh: '',
    districtNameEn: '',
    districtNameTh: '',
    provinceNameEn: '',
    provinceNameTh: '',
    countryNameEn: '',
    countryNameTh: '',
    zipcode: '',
    files: []
  };

  public onClose: BehaviorSubject<IAddressFreeFormResult> = new BehaviorSubject<IAddressFreeFormResult>({
    action: '',
    type: '',
    data: this.address,
    file: null
  });

  type: string;
  title: string;
  enableBranch: boolean;
  enableUploadFile: boolean;

  // fileToUpload: File = null;

  // isRequiredFileUpload = false;

  form: FormGroup;

  canUpload = true;
  canDownload = true;
  canDelete = true;

  uploadUrl: string = '';

  isLoading = false;

  maxSize: number = 5 * 1024 * 1024// byte

  constructor(
    // private service: Service,
    private builder: FormBuilder,
    public dialogError: MatDialog
  ) {

  }

  ngOnInit() {

  }

  public setData(addr: IAddressFreeFormAddr, type: string, title: string, enableBranch: boolean, enableUploadFile: boolean, uploadUrl: string) {
    this.type = type;
    this.title = title;
    this.enableBranch = enableBranch;
    this.enableUploadFile = enableUploadFile;
    this.uploadUrl = uploadUrl;

    this.address = addr;

    this.createFormData();
  }

  public setLoading(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  public createFormData() {
    this.form = this.builder.group({
      isHeadOffice: [(this.address.isHeadOffice ? '1' : '0')],
      branchNo: [this.address.branchNo],
      addrTh: [this.address.addrTh],
      addrEn: [this.address.addrEn],
      countryNameTh: [this.address.countryNameTh],
      countryNameEn: [this.address.countryNameEn],
      provinceNameTh: [this.address.provinceNameTh],
      provinceNameEn: [this.address.provinceNameEn],
      districtNameTh: [this.address.districtNameTh],
      districtNameEn: [this.address.districtNameEn],
      subDistrictNameTh: [this.address.subDistrictNameTh],
      subDistrictNameEn: [this.address.subDistrictNameEn],
      zipcode: [this.address.zipcode]
    });

    this.branchNo_toggleValidator();
    this.form.get('isHeadOffice').valueChanges.subscribe(value => {
      this.branchNo_toggleValidator();
    });
  }

  private branchNo_toggleValidator() {
    if (this.enableBranch && this.form.get('isHeadOffice').value === '0') {
      // this.form.get('branchNo').setValidators(this.branchNoValidator);
      this.form.get('branchNo').setValidators([this.branchNoValidator, Validators.required]);
      // this.form.get('branchNo').setValidators(Validators.pattern('^[1-9]([0-9]{1,4})+$'));
    } else {
      this.form.get('branchNo').setValidators(null);
    }

    this.form.get('branchNo').updateValueAndValidity();
  }

  private branchNoValidator(control: FormControl) {
    // let pattern = new RegExp('^[1-9]([0-9]{1,4})?$');
    let pattern = new RegExp('^[0-9]{1,5}$');

    if (!(control.value > 0) || !pattern.test(control.value.toString())) {
      return {
        // 'required': true
        'invalided': true
      }
    }

    return null;
  }

  /*
  public handleFileUpload(files: FileList) {
    if (files.length > 0) {
      this.fileToUpload = files.item(0);
    }
  }
  */

  public onShowLoading(isShowLoading) {
    if (isShowLoading) {
      this.isLoading = true;
    } else {
      this.isLoading = false;
    }
  }

  public onCancel() {
    this.dialogError.open(DialogConfirmComponent, {
      data: ""
    }).afterClosed().subscribe(result => {
      if (result == 1) {
        this.onClose.next({
          action: 'close',
          type: this.type,
          data: this.address,
          file: null
        });
      }
    });
  }

  public onSave() {
    if (this.form.get('isHeadOffice').value == '1') {
      this.form.get('branchNo').setValue(0);
    }

    // console.log(this.form.value);

    if (this.form.invalid)  {
      console.log('form => invalid');

      for(let i in this.form.controls) {
        this.form.controls[i].markAsTouched();
      }
    // } else if (this.enableUploadFile && this.fileToUpload == null) {
    } else if (this.enableUploadFile && this.address.files.length == 0) {
      console.log('form => invalid (require upload file)');

      // this.isRequiredFileUpload = true;

      this.dialogError.open(DialogWarningComponent, {
        data: {
          title: 'Warning',
          content: 'Please upload file before save.',
          okBtnText: 'OK',
          isOneButton: true
        }
      }).afterClosed().subscribe(result => {

      });
    } else {
      console.log('form => valid');

      // this.onClose.next('save');
      this.onClose.next({
        action: 'save',
        type: this.type,
        data: this.form.value,
        // file: this.fileToUpload
        file: null
      });
    }
  }

}
