import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressFreeFormComponent } from './address-freeform.component';


describe('AddressFreeFormComponent', () => {
  let component: AddressFreeFormComponent;
  let fixture: ComponentFixture<AddressFreeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressFreeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressFreeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
