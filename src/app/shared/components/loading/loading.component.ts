import { Component, OnInit,Input,OnChanges } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit,OnChanges {

  @Input('showLoading') showLoading:boolean ;
  constructor() { }

  ngOnInit() {
    // this.showLoading = true;
  }
  ngOnChanges(value: any) {

  }
}
