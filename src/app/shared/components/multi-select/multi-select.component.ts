import { Component, OnInit, EventEmitter, Output, Input, ViewChild,OnChanges } from '@angular/core';

import { FormBuilder, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';
import { MatOption } from '@angular/material';
import { Service } from 'src/app/shared/service';
import { ifStmt } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss']
})
export class MultiSelectComponent implements OnInit,OnChanges {

  @Input('fieldName') fieldName: String;
  @Input('optionAllTitle') optionAllTitle: String;
  @Input('defaultValue') defaultValue: Number[];
  @Input('errorMessage') errorMessage: String;
  @Input('required') required:string;
  @Input('url') url:string;
  @Input('id') id:string;
  @Input('options') defaultOptions: any;
  @Input('disabled') disabled: boolean = false;

  @Output('onValueUpdate') onValueUpdate = new EventEmitter();

  @ViewChild('optionAll') private optionAll: MatOption;
  private optionOne: MatOption;

  form: FormGroup;

  options = [
    {
      value: 98080,
      label: 'Option 1',
      id:3
    },
    {
      value: 2,
      label: 'Option 2',
      id:4
    },
    {
      value: 3,
      label: 'Option 3',
      id:5
    },
    {
      value: 4,
      label: 'Option 4',
      id:5
    }
  ]

  constructor(
    private builder: FormBuilder,
    private dataService: Service

  ) {

  }

  ngOnChanges(value: any) {
    if(value.defaultValue){

      // console.log("value.defaultValue.currentValue",value.defaultValue);
      this.defaultValue = value.defaultValue.currentValue;

    let foundAllSelected = true;
    for (var i = 0; i < this.options.length; i++) {
      if (this.defaultValue.indexOf(this.options[i].value) == -1) {
        foundAllSelected = false;
      }
    }
    if (foundAllSelected) {
      this.defaultValue.push(0);
    }

    // console.log("foundAllSelected",foundAllSelected);
    this.createFormData();
  }
}
  ngOnInit() {
    // console.log(this.defaultValue);

    if (this.optionAllTitle == '') {
      this.optionAllTitle = 'Select All'
    }
    if (this.required) {
      this.fieldName = this.fieldName + "*"
    }
    // var foundAllSelected = true;
    // for (var i = 0; i < this.options.length; i++) {
    //   if (this.defaultValue.indexOf(this.options[i].value) == -1) {
    //     foundAllSelected = false;
    //   }
    // }
    // if (foundAllSelected) {
    //   this.defaultValue.push(0);
    // }

    // this.createFormData();

    if(this.url !== undefined && this.url != ''){
      // let response:any = this.dataService.sendGetRequest(this.url);

      this.dataService.sendGetRequest(this.url, this, function (main, data) {
        // console.log("data",data);
        if (data != null) {
          console.log('in data not null');
          main.options = data.data;
          let foundAllSelected = true;
          for (var i = 0; i < main.options.length; i++) {
            if (main.defaultValue.indexOf(main.options[i].value) == -1) {
              foundAllSelected = false;
            }
          }
          if (foundAllSelected) {
            main.defaultValue.push(0);
          }

        }
      });
  // console.log(result);

      // this.dataService.sendGetRequest(this.url).subscribe((result: any)=>{
      //   // console.log(result);
      //   this.options = result.data;
      //   let foundAllSelected = true;
      //   for (var i = 0; i < this.options.length; i++) {
      //     if (this.defaultValue.indexOf(this.options[i].value) == -1) {
      //       foundAllSelected = false;
      //     }
      //   }
      //   if (foundAllSelected) {
      //     this.defaultValue.push(0);
      //   }
      // })
    }else if(this.defaultOptions !== undefined){
      this.options = this.defaultOptions;
    }


  }

  checkValidate(){
    this.form.get('inputField').markAsTouched();

    return !this.form.get('inputField').hasError('required');
  }

  private createFormData() {
    if(this.required !== undefined){
      this.form = this.builder.group({
        inputField: [this.defaultValue, Validators.required]
      })
    }else{
      this.form = this.builder.group({
        inputField: [this.defaultValue]
      })
    }
  }

  toggleOption() {
    // console.log("id",id);
    if (this.optionAll.selected) {
      this.optionAll.deselect();
    }

    let selected_value = this.form.get('inputField').value;

    if (selected_value.length == this.options.length && !this.optionAll.selected) {
      this.optionAll.select();
    }

    this.updateValue();
  }

  toggleAllOptions() {
    if (this.optionAll.selected) {
      var keys = this.getOptionsKeys();
      keys.push(0);

      this.form.get('inputField').patchValue(keys);
    } else {
      this.form.get('inputField').patchValue([]);
    }

    //this.onValuesChange()

    this.updateValue();
  }

  updateValue() {
    let selected_value = this.form.get('inputField').value;

    selected_value = selected_value.filter(item => item !== 0);

    // console.log(selected_value);

    let result = {
      value : selected_value,
      id : this.id
    }
    this.onValueUpdate.emit(result);
  }

  getOptionsKeys() {
    var keys = [];

    for (var i = 0; i < this.options.length; i++) {
      keys.push(this.options[i].value);
    }

    return keys;
  }

  getOptionValue(key: Number) {
    var value = '';

    for (var i = 0; i < this.options.length; i++) {
      if (this.options[i].value == key) {
        value = this.options[i].label;
      }
    }

    return value;
  }

  // onValuesChange() {
  // //onValuesChange(allOptionClicked: Boolean) {
  //   //this.form.get('inputField').markAsTouched();

  //   //console.log(this.form.value);

  //   //if (this.form.invalid) return;

  //   //this.onValueUpdate.emit(this.form.get('inputField').value);


  //   /*
  //   let selected_value = this.form.get('inputField').value;

  //   if (selected_value.indexOf(0) > -1) {
  //     var temp_arr = [];
  //     for (var i=0; i < this.options.length; i++) {
  //       temp_arr.push(this.options[i].key);
  //     }
  //     selected_value = temp_arr;

  //     //this.form.get('inputField').patchValue([]);
  //   } else {

  //     //this.form.get('inputField').patchValue(selected_value);
  //   }

  //   console.log(selected_value);
  //   */


  //   let selected_value = this.form.get('inputField').value;


  //   /*
  //   if (selected_value.length < this.options.length && this.optionAll.selected) {
  //     var keys = selected_value;
  //     keys = keys.filter(item => item !== 0);

  //     this.form.get('inputField').patchValue(keys);
  //   } else if (selected_value.length == this.options.length && !this.optionAll.selected) {
  //     var keys = selected_value;
  //     keys.push(0);

  //     this.form.get('inputField').patchValue(keys);
  //   }
  //   */

  //   /*
  //   if (selected_value.length < (this.options.length + 1) && this.optionAll.selected) {
  //     selected_value = selected_value.filter(item => item !== 0);

  //     this.form.get('inputField').patchValue(selected_value);
  //   } else if (selected_value.length == (this.options.length + 1) && !this.optionAll.selected) {
  //     selected_value.push(0);

  //     this.form.get('inputField').patchValue(selected_value);
  //   }

  //   selected_value = selected_value.filter(item => item !== 0);
  //   */

  //   /*
  //   if (allOptionClicked) {
  //     console.log('allOptionClicked');
  //     if (this.optionAll.selected) {
  //       console.log('selected');
  //       selected_value = this.getOptionsKeys();
  //       selected_value.push(0);
  //     } else {
  //       console.log('not selected');
  //       selected_value = [];
  //     }
  //   } else {
  //     if (selected_value.length < (this.options.length + 1) && this.optionAll.selected) {
  //     } else if (selected_value.length == this.options.length && !this.optionAll.selected) {
  //       selected_value.push(0);
  //     }
  //   }

  //   this.form.get('inputField').patchValue(selected_value);

  //   selected_value = selected_value.filter(item => item !== 0);
  //   */

  //   console.log(selected_value);

  //   this.onValueUpdate.emit(selected_value);
  // }

}
