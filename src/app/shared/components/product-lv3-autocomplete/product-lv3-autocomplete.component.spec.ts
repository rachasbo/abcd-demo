import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLv3AutocompleteComponent } from './product-lv3-autocomplete.component';

describe('ProductLv3AutocompleteComponent', () => {
  let component: ProductLv3AutocompleteComponent;
  let fixture: ComponentFixture<ProductLv3AutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductLv3AutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductLv3AutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
