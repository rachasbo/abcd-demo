import { Observable, Subject } from 'rxjs';
import { Service } from 'src/app/shared/service';
import { startWith, map } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';


export interface IOption {
  label: string;
  value: ProductLv3Value;
}

export class ProductLv3Value{
  productLevel3Id: number;
  isCustomise: boolean;
}

@Component({
  selector: 'app-product-lv3-autocomplete',
  templateUrl: './product-lv3-autocomplete.component.html',
  styleUrls: ['./product-lv3-autocomplete.component.scss']
})
export class ProductLv3AutocompleteComponent implements OnInit, OnChanges {
  
  @Input("defaultValue") defaultValue: number;
  @Input('disabled') disabled: boolean;
  @Input('errorMessage') errorMessage: String;
  @Input('label') label: string;
  @Input('placeholder') placeholder: string;
  @Input('url') url: string;
  @Input('id') id: string;
  @Input('required') required: boolean;
  @Input('validator') validator: Boolean;
  @Input('additionalParams') additionalParams: string;
  @Input('childsMarkAsTouchedTrigger') childsMarkAsTouchedTrigger: Subject<boolean>;
  @Output('onValueUpdate') onValueUpdate = new EventEmitter();

  form: FormGroup;

  options: IOption[] = [

  ];
  temp_options: IOption[] = [

  ];
  filteredOptions: Observable<IOption[]>;

  skipBlur: boolean = false;
  disabled_option = false;


  constructor(
    private builder: FormBuilder,
    private dataService: Service
  ) { }

  ngOnInit() {

    if (this.required && this.placeholder && this.placeholder!='') {
      this.placeholder = this.placeholder + "*";
    }

    setTimeout(() => {
      if (this.disabled === true) {

        this.form.get('inputField').disable();
      } else {
        this.form.get('inputField').enable();
      }
    }, 500);
    
    if (this.childsMarkAsTouchedTrigger) {
      this.childsMarkAsTouchedTrigger.subscribe(() => { 
        this.form.get('inputField').markAsTouched();
      })
    }
    // this.getOption();

    this.getTempOption();
  }

  getTempOption() {
    // console.log("this.url",this.url);
    let url_path = this.url + "?Limit=0";

    this.dataService.sendGetRequest(url_path, this, function (main, data) {

      if (data != null) {
        main.temp_options = data.data;

        if (main.options.length === 0) {
          main.options = [{ label: "No Matches", value: "" }];
          main.disabled_option = true;
        } else {
          main.disabled_option = false;
        }

        if (main.required) {
          main.form = main.builder.group({
            inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
          })
        } else {

          main.form = main.builder.group({
            inputField: [main.getOptionByValue(main.defaultValue)]
          })
        }

        // console.log("Temp",main.temp_options );
      }
    });
  }

  getOption() {
    // console.log("this.url",this.url);
    let url_path = this.url + "?Limit=10";
    this.dataService.sendGetRequest(url_path, this, function (main, data) {

      if (data != null) {
        main.options = data.data;

        if (main.options.length === 0) {
          main.options = [{ label: "No Matches", value: "" }];
          main.disabled_option = true;
        } else {
          main.disabled_option = false;
        }

        // console.log("Temp",main.temp_options );
      }
    });
  }


  callLabel() {
    setTimeout(() => {
      this.filteredOptions = this.form.get('inputField').valueChanges.pipe(
        startWith(this.defaultValue),
        //map(value => typeof value === 'string' ? value : value.value),
        map(value => {
          if (typeof value === 'string') {
            return value;
          } else {
            if (value != null && value != undefined) {
              return value.value;
            }
          }
        }),
        map(label => label ? this.filterOptions(label) : this.options)
      )
    }, 100);
  }


  ngOnChanges(value: any) {
    // console.log("Test",this.temp_options);
    // console.log("onChange",this.id,value);
    if (value.url) {
      // console.log("in,url");
      // value.additionalParams

      // let url_path = this.url + "?TextSearch="+value+"&Limit=10";
      this.url = value.url.currentValue;
      // console.log("url",this.url)
      // this.getTempOption();
      let url_path = "";
      if (this.additionalParams) {
        url_path = this.url + "?Limit=10&" + this.additionalParams;
      } else {
        url_path = this.url + "?Limit=10";
      }

      // console.log("url path",url_path);
      let Main = this;
      this.dataService.sendGetRequest(url_path, this, function (main, data) {

        if (data != null) {
          main.options = data.data;

          if (main.options.length === 0) {
            main.options = [{ label: "No Matches", value: "" }];
            main.disabled_option = true;
          } else {
            main.disabled_option = false;
          }
          if (main.disabled === true) {
            main.form.get('inputField').disable();
          }


          main.callLabel();

          if (main.required) {
            main.form = main.builder.group({
              inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
            })
          } else {

            main.form = main.builder.group({
              inputField: [main.getOptionByValue(main.defaultValue)]
            })
          }


        }
      });
    }
    // console.log("value.defaultValue",value.defaultValue);
    if (value.defaultValue !== undefined) {
      // console.log("in,defaultValue",value.defaultValue.currentValue);
      // console.log("innn 2",this.id,value.defaultValue.currentValue);
      this.defaultValue = value.defaultValue.currentValue;

      // if(value.defaultValue.previousValue === undefined){

      //   this.getTempOption();

      // }

      if (this.required) {
        this.form = this.builder.group({
          inputField: [this.getOptionByValue(this.defaultValue), Validators.required]
        })
      } else {
        this.form = this.builder.group({
          inputField: [this.getOptionByValue(this.defaultValue)]
        })
      }


      let url_path = "";

      if (value.defaultValue.currentValue == "" || value.defaultValue.currentValue == 0) {
        // let url_path = this.url + "?Limit=10";
        // console.log("")

        if (this.additionalParams) {
          url_path = this.url + "?Limit=10&" + this.additionalParams;
        } else {
          url_path = this.url + "?Limit=10";
        }


        this.dataService.sendGetRequest(url_path, this, function (main, data) {
          // console.log("test1",value,main.url);
          if (data != null) {
            main.options = data.data;

            if (main.options.length === 0) {
              main.options = [{ label: "No Matches", value: "" }];
              main.disabled_option = true;
            } else {
              main.disabled_option = false;
            }
            // console.log("Data",data);
            // if (main.required) {
            //   main.form = main.builder.group({
            //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
            //   })
            // } else {

            //   main.form = main.builder.group({
            //     inputField: [main.getOptionByValue(main.defaultValue)]
            //   })
            // }

            main.callLabel();
            // return main.options;
          }

        });


      } else {
        // console.log("value.defaultValue.currentValue",value.defaultValue.currentValue);
        let label = this.getOptionLabelByValue(value.defaultValue.currentValue);
        // let label = "test";
        // console.log("label",label);
        if (this.additionalParams) {
          url_path = this.url + "?TextSearch=" + encodeURIComponent(label) + "&Limit=10&" + this.additionalParams;
        } else {
          url_path = this.url + "?TextSearch=" + encodeURIComponent(label) + "&Limit=10";
        }


        this.dataService.sendGetRequest(url_path, this, function (main, data) {
          // console.log("test1",value,main.url);
          if (data != null) {
            main.options = data.data;
            // console.log("Data",data);
            // if (main.required) {
            //   main.form = main.builder.group({
            //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
            //   })
            // } else {

            //   main.form = main.builder.group({
            //     inputField: [main.getOptionByValue(main.defaultValue)]
            //   })
            // }

            main.callLabel();
            // return main.options;
          }

        });

        // this.callLabel();
      }


    }
    // this.filteredOptions = this.form.get('inputField').valueChanges
    //   .pipe(
    //     startWith(this.defaultValue),
    //     map(value => typeof value === 'string' ? value : value.value),
    //     map(label => label ? this.filterOptions(label) : this.options)
    //   )




    if (value.additionalParams) {

      let url_path = "";
      this.additionalParams = value.additionalParams.currentValue;
      // console.log("additionalParams",this.additionalParams)
      // let label = "test";
      // console.log("label",label);
      // this.getTempOption();
      let input_value = this.form.get('inputField').value;
      // console.log("input_value",input_value);
      if (input_value === "" || input_value === undefined || input_value === null) {

        if (this.additionalParams) {
          url_path = this.url + "?Limit=10&" + this.additionalParams;
        } else {
          url_path = this.url + "?Limit=10";
        }

      } else {
        // console.log("input_value",input_value);

        if (this.additionalParams) {
          url_path = this.url + "?TextSearch=" + encodeURIComponent(input_value.label) + "&Limit=10&" + this.additionalParams;
        } else {
          url_path = this.url + "?TextSearch=" + encodeURIComponent(input_value.label) + "&Limit=10";
        }
      }

      this.dataService.sendGetRequest(url_path, this, function (main, data) {
        // console.log("test1",value,main.url);
        if (data != null) {
          main.options = data.data;
          if (main.options.length === 0) {
            main.options = [{ label: "No Matches", value: "" }];
            main.disabled_option = true;
          } else {
            main.disabled_option = false;
          }
          // console.log("Data",data);
          // if (main.required) {
          //   main.form = main.builder.group({
          //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
          //   })
          // } else {

          //   main.form = main.builder.group({
          //     inputField: [main.getOptionByValue(main.defaultValue)]
          //   })
          // }

          main.callLabel();
          // return main.options;
        }

      });

    }



    if (value.disabled) {

      this.disabled = value.disabled.currentValue;

      // setTimeout(() => {
        if (this.disabled === true) {

          this.form.get('inputField').disable();
        } else {
          this.form.get('inputField').enable();
        }
      // },500);

    }



  }
  private filterOptions(value: any): IOption[] {
    // console.log("in filter",);


    if (typeof value === 'string') {
      let url_path = "";
      if (this.additionalParams) {
        url_path = this.url + "?TextSearch=" + encodeURIComponent(value) + "&Limit=10&" + this.additionalParams;
      } else {
        url_path = this.url + "?TextSearch=" + encodeURIComponent(value) + "&Limit=10";
      }

      // let url_path = this.url + "?TextSearch="+value+"&Limit=10";
      this.dataService.sendGetRequest(url_path, this, function (main, data) {
        // console.log("test1",value,main.url);
        if (data != null) {
          main.options = data.data;
          if (main.options.length === 0) {
            main.options = [{ label: "No Matches", value: "" }];
            main.disabled_option = true;
          } else {
            main.disabled_option = false;
          }
          // console.log("Data",data);
          // if (main.required) {
          //   main.form = main.builder.group({
          //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
          //   })
          // } else {

          //   main.form = main.builder.group({
          //     inputField: [main.getOptionByValue(main.defaultValue)]
          //   })
          // }

          main.callLabel();
          // return main.options;
        }

      });

    }




    // let filterValue = '';

    // if (typeof value === 'string') {
    //   filterValue = value;
    // } else if (typeof value === 'number') {
    //   for (var i = 0; i < this.options.length; i++) {
    //     if (this.options[i].value == value) {
    //       filterValue = this.options[i].label;
    //     }
    //   }
    // } else {
    //   let option: IOption = value;

    //   filterValue = option.label;
    // }

    // filterValue = filterValue.trim().toLowerCase();
    // console.log("this.options",this.options.filter(option => option.label.toLowerCase().includes(filterValue)));
    // // console.log("this.option",this.options.filter(option => option.label.toLowerCase().includes(filterValue)));
    // return this.options.filter(option => option.label.toLowerCase().includes(filterValue));
    // // console.log("test2");


    // let optiontest:IOption[] = [{value: 1, label: "AP (Thailand) Public Company Limited/บริษัท เอพี (ไทยแลนด์) จำกัด"}];
    return this.options;
  }

  private onSelect() {
    this.skipBlur = true;
    this.onValueChange(true);
  }

  private onValueChange(save: boolean) {


    setTimeout(() => {
      let value = 0;
      let input_value = this.form.get('inputField').value;
      // console.log("Test",input_value);
      if (typeof input_value === 'string') {
        value = 0;
      } else {
        let option: IOption = input_value;
        value = option.value.productLevel3Id;
      }


      // console.log("value",value)
      if (save) {
        this.defaultValue = value;
      }

      this.skipBlur = false;

      let result = {
        label: input_value.label,
        value: value,
        id: this.id
      }
      this.onValueUpdate.emit(result);

    }, 100);
  }

  onBlur() {
    setTimeout(() => {
      if (!this.skipBlur) {

        if (this.defaultValue > 0) {
          this.form.get('inputField').patchValue(this.getOptionByValue(this.defaultValue));

        } else {
          this.form.get('inputField').patchValue('');

          let url_path = "";
          let input_value = this.form.get('inputField').value;
          if (input_value === "" || input_value === undefined || input_value === null) {

            if (this.additionalParams) {
              url_path = this.url + "?Limit=10&" + this.additionalParams;
            } else {
              url_path = this.url + "?Limit=10";
            }

          }

          this.dataService.sendGetRequest(url_path, this, function (main, data) {
            if (data != null) {
              main.options = data.data;
              if (main.options.length === 0) {
                main.options = [{ label: "No Matches", value: "" }];
                main.disabled_option = true;
              } else {
                main.disabled_option = false;
              }

              main.callLabel();
            }

          });

        }
      }
    }, 300);
  }

  public getOptionLabelByValue(value: number) {
    let label = '';
    // console.log('getOptionLabelByValue',this.temp_options,this.options);
    for (var i = 0; i < this.temp_options.length; i++) {
      if (this.temp_options[i].value.productLevel3Id == this.defaultValue) {
        label = this.temp_options[i].label;
      }
    }

    return label;
  }

  public getOptionByValue(value: number): IOption {

    let option: IOption;
    // console.log("getOptionByValue",this.options);
    for (var i = 0; i < this.temp_options.length; i++) {

      if (this.temp_options[i].value.productLevel3Id == this.defaultValue) {
        option = this.temp_options[i];
      }
    }

    return option;
  }

  private onClear() {
    this.form.get('inputField').patchValue('');

    let url_path = "";
    let input_value = this.form.get('inputField').value;
    if (input_value === "" || input_value === undefined || input_value === null) {

      if (this.additionalParams) {
        url_path = this.url + "?Limit=10&" + this.additionalParams;
      } else {
        url_path = this.url + "?Limit=10";
      }

    }

    this.dataService.sendGetRequest(url_path, this, function (main, data) {
      if (data != null) {
        main.options = data.data;
        if (main.options.length === 0) {
          main.options = [{ label: "No Matches", value: "" }];
          main.disabled_option = true;
        } else {
          main.disabled_option = false;
        }

        main.callLabel();
      }

    });


    this.onValueChange(true);
  }

  displayFn(value: any): string {
    // console.log("value",value);
    if (value) {
      return typeof value === 'string' ? value : value.label;
    } else {
      return "";
    }
  }

  checkValidate() {
    this.form.get('inputField').markAsTouched();
    return !this.form.get('inputField').hasError('required');
  }


}
