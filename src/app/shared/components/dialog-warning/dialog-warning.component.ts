import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-warning',
  templateUrl: './dialog-warning.component.html',
  styleUrls: ['./dialog-warning.component.scss']
})
export class DialogWarningComponent implements OnInit {
  isOneButton: boolean;
  title: string = "";
  content: string = "";
  cancelBtnText: string = "";
  okBtnText: string ="";

  constructor(
    public dialogRef: MatDialogRef<DialogWarningComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
   }
  ngOnInit() {
    this.isOneButton = this.data.isOneButton;
    this.title = this.data.title;
    this.content = this.data.content;
    this.cancelBtnText = this.data.cancelBtnText;
    this.okBtnText = this.data.okBtnText;
  }

  onClose(){
    this.dialogRef.close(0);
  }

  onConfirm(){
    this.dialogRef.close(1);
  }
}
