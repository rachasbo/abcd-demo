import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Service } from '../../service';
import { Subject } from 'rxjs';

export interface IData {
  value: number;
  label: string;
}

@Component({
  selector: 'app-dropdown-new',
  templateUrl: './dropdown-new.component.html',
  styleUrls: ['./dropdown-new.component.scss']
})
export class DropdownNewComponent implements OnInit, OnChanges {

  @Input('placeholder') placeholder: string;
  @Input('value') value: number;
  @Input('url') url: string;
  @Input('hideDefaultOption') hideDefaultOption: boolean;
  @Input('disabled') disabled: boolean;
  // @Input ('required') orderRequire: string;
  // @Input ('required') required: boolean = false;
  @Input ('required') required: any;
  @Input('errorMessage') errorMessage: String;
  @Output('onValueUpdate') onValueUpdate = new EventEmitter();

  @Input() inputModel: any;
  @Output() inputModelChange = new EventEmitter<string>();

  @Input('childsMarkAsTouchedTrigger') childsMarkAsTouchedTrigger: Subject<boolean>;

  public orderRequire: string;
  

  form: FormGroup;

  dataList: IData[] = [];
  //dataList: DropdownsModel[] = [];

  isSetDefault = false;

  constructor(
    public builder: FormBuilder,
    private service: Service
  ) {
    
  }

  ngOnInit() {
    this.createFormData();
    
    //this.loadContent();

    if (this.required == true || this.required == 'true') {
      this.orderRequire = 'true';
    } else {
      this.orderRequire = 'false';
    }

    // console.log(this.childsMarkAsTouchedTrigger);
    if (this.childsMarkAsTouchedTrigger) {
      this.childsMarkAsTouchedTrigger.subscribe(() => { 
        // console.log(this.form.get('inputField').value);
        this.form.get('inputField').markAsTouched();
      })
    }
  }

  ngOnChanges(value: any) {
    // console.log('ngOnChanges:\n', value);

    if (value.url) {
      //console.log(value.url.previousValue);
      //console.log(value.url.currentValue);

      this.loadContent();
    } else if (value.value && !value.value.firstChange && value.value.previousValue != value.value.currentValue) {
      this.form.get('inputField').setValue(value.value.currentValue);
    } else if (value.required && !value.required.firstChange && value.required.previousValue != value.required.currentValue) {
      if (value.required || value.required=='true') {
        this.form.get('inputField').setValidators(this.inputValidator);
        this.orderRequire = 'true';
      } else {
        this.form.get('inputField').setValidators(null);
        this.orderRequire = 'false';
      }
    }
  }

  private createFormData() {
    this.form = this.builder.group({
      inputField: ['']
      // inputField: (this.required) ? ['', Validators.required] : ['']
      // inputField: (this.required) ? ['', this.inputValidator] : ['']
      //inputField: [0, this.inputValidator]
    })

    if (this.required) {
      this.form.get('inputField').setValidators(this.inputValidator);
    }
  }

  public inputValidator(control: FormControl) {
    // console.log('inputValidator', control.value);
    
    if (!(control.value > 0)) {
      return {
        'required': true
      }
    }

    return null;
  }

  private loadContent() {
    if (this.url != "") {
      this.service.sendGetRequest(this.url, this, function(main, response){
        //console.log(main.url);
        //console.log(response);

        if (response.data != null) {
          console.log(response.data);

          main.dataList = JSON.parse(JSON.stringify(response.data));
          //main.dataList = new DropdownsModel().deserializes(response.data);

          if (!main.isSetDefault) {
            main.isSetDefault = true;

            if (main.value > 0) {
              main.form.get('inputField').setValue(main.value);
            }

            //console.log('DefaultValue = ' + main.form.get('inputField').value);
          } else {
            main.form.get('inputField').setValue(0);
          }
        }
      })
    }
  }

  public onSelect() {
    let input_value = this.form.get('inputField').value;
    //console.log(input_value);

    let result: IData;

    for (var i=0; i<this.dataList.length; i++) {
      let data = this.dataList[i];

      if (data.value == input_value) {
        result = data;
      }
    }

    if (result == null) {
      result = {
        value: 0,
        label: ''
      }
    }

    this.onValueUpdate.emit(result);
  }

  /*
  public markAsTouched() {
    console.log('markAsTouched');

    this.form.get('inputField').markAsTouched();
  }

  public invalid() {
    let value = this.form.get('inputField').value;

    let valid = false;

    if (value > 0) {
      valid = true;
    }

    return !valid;
  }
  */

}
