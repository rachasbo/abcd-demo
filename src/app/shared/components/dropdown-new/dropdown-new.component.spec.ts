import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownNewComponent } from './dropdown-new.component';

describe('DropdownNewComponent', () => {
  let component: DropdownNewComponent;
  let fixture: ComponentFixture<DropdownNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
