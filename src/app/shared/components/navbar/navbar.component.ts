import { NavbarModel } from './../../models/navbar.model';
import { Service } from 'src/app/shared/service';
import { Component, OnInit } from '@angular/core';
//import { Subscription } from 'rxjs';
//import { MsalService, BroadcastService } from '@azure/msal-angular';

//import { environment } from 'src/environments/environment';
import { Auth } from 'src/app/shared/auth';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserModel } from 'src/app/modules/user-management/models/user-model.model';

import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  environment = environment;
  
  show_login: boolean = false;
  logined: boolean = false;

  menuList: NavbarModel[] = [];

  user: UserModel;

  constructor(
    public auth: Auth,
    private dataService: Service,
    private router: Router,
    private location: Location
  ) {

    this.auth.updateLogin.subscribe(login_state => {
      console.log('login_state =', login_state);

      if (login_state == 1) {
        this.show_login = true;
        this.logined = true;

        this.user = this.auth.getUser();

        // check for update token
        // this.auth.checkUpdateToken();

        // this.loadUserAutorizedMenu();

      //} else if (login_state == 0) {
      } else {
        this.show_login = true;
        this.logined = false;

        this.user = null;
      }
    });
    
  }

  private loadUserAutorizedMenu() {
    this.dataService.sendGetRequest('/authen/v1/Users/permission', this, function(main, response){
      console.log(response);
      if(response != null){
        if(response.data != null){
          main.menuList = response.data;
        }
      }
    })
  }

  ngOnInit() {
  }

  public onLogin() {
    this.auth.login();
  }

  public onLogout() {
    this.auth.logout();
  }

  // public isActiveLink(link: string) {
  //   let currentURL = this.location.path();
  //   // console.log(link, currentURL);

  //   let url_arr = currentURL.split('/');
  //   // console.log(link, url_arr[1]);

  //   return (link == url_arr[1]);
  // }
  public isActiveLink(link: string) {
    let isActived = false;

    let currentURL = this.location.path();

    if (currentURL.indexOf(link) > -1) {
      isActived = true;
    }

    return isActived;
  }

  // public onMenuClick(event, link: string[], subMenu: any) {
  public onMenuClick(link: string[], subMenu: any) {
    // event.stopPropagation();
    // event.preventDefault();

    // console.log('onMenuClick :', link, subMenu);

    // if (link[0]=='dashboard') {
    //   link = ['/'];
    // }

    // if (this.auth.isLogined() || link[0]=='/' || link[0]=='dashboard') {
    if (this.auth.isLogined()) {
      // console.log('onMenuClick : isLogined=true');

      if (subMenu) {
        this.router.navigate(link, { state: { data: subMenu } });
      } else {
        this.router.navigate(link);
      }
    } else {
      // console.log('onMenuClick : isLogined=false');

      // this.auth.login();

      // this.menuList = [];
      // this.logined = false;

      // call any service for show session timeout alert
      this.loadUserAutorizedMenu();
    }
  }


  // public onAccountClick(accountId){
  //   this.dataService.sendPutRequestDisabledAlert('/authen/v1/Accounts/' + accountId+"/active", null, this, function (main, response) {
  //     if (response != null) {
  //       if (response.data != null) {
  //           window.location.reload();
  //           main.auth.loadUserData();
  //       }
  //     }
  //     main.showLoading = false;
  //   });
  // }

}


// export class NavbarComponent implements OnInit {

//   private subscription: Subscription;

//   logined: boolean = false;

//   userName: string = "";

//   isIE = window.navigator.userAgent.indexOf("MSIE ") > -1 || window.navigator.userAgent.indexOf("Trident/") > -1;

//   constructor(
//     private authService: MsalService,
//     private broadcastService: BroadcastService
//   ) {

//     if (this.authService.getUser()) {
//       this.logined = true;

//       /*
//       setInterval(() => {
//         console.log('acquireTokenSilent');

//         this.authService.acquireTokenSilent(environment.adScope).then(
//           accessToken => {
//             return accessToken;
//           },
//           error => {
//             console.log('error:\n' + error);

//             if (this.isIE) {
//               this.authService.acquireTokenRedirect(environment.adScope);
//             } else {
//               this.authService.acquireTokenPopup(environment.adScope)
//               .then(
//                 accessToken => {
//                   return accessToken;
//                 },
//                 err => {
//                   console.error(err);
//                 }
//               );
//             }

//           }
//         );
//       }, 60000);
//       */

//     } else {
//       this.logined = false;
//     }
//   }

//   ngOnInit() {
//     this.getUserInfo();

//     this.broadcastService.subscribe("msal:loginFailure", (payload) => {
//       console.log("login failure " + JSON.stringify(payload));
//       this.logined = false;
//     });

//     this.broadcastService.subscribe("msal:loginSuccess", (payload) => {
//       console.log("login success " + JSON.stringify(payload));
//       this.logined = true;

//       this.getUserInfo();
//     });

//     this.subscription = this.broadcastService.subscribe("msal:acquireTokenSuccess", (payload) => {
//       console.log("acquire token success " + JSON.stringify(payload));

//       this.getUserInfo();
//     });

//     //will work for acquireTokenSilent and acquireTokenPopup
//     this.subscription = this.broadcastService.subscribe("msal:acquireTokenFailure", (payload) => {
//       console.log("acquire token failure " + JSON.stringify(payload))
//       if (payload.errorDesc.indexOf("consent_required") !== -1 || payload.errorDesc.indexOf("interaction_required") != -1) {
//         /*
//         this.authService.acquireTokenPopup(environment.adScope).then((token) => {
//           this.getUserInfo();
//         }, (error) => {
//         });
//         */

//         if (this.isIE) {
//           this.authService.acquireTokenRedirect(environment.adScope);
//         } else {
//           this.authService.acquireTokenPopup(environment.adScope)
//           .then(
//             accessToken => {
//               return accessToken;
//             },
//             err => {
//               console.error(err);
//             }
//           );
//         }
//       }
//     });
//   }

//   ngOnDestroy() {
//     this.broadcastService.getMSALSubject().next(1);
//     if (this.subscription) {
//       this.subscription.unsubscribe();
//     }
//   }

//   getUserInfo() {
//     if (this.authService.getUser()) {
//       this.userName = this.authService.getUser().name
//     }
//   }

//   onLogin() {
//     //const isIE = window.navigator.userAgent.indexOf("MSIE ") > -1 || window.navigator.userAgent.indexOf("Trident/") > -1;

//     if (this.isIE) {
//       this.authService.loginRedirect();
//     } else {
//       this.authService.loginPopup();
//     }
//   }

//   onLogout() {
//     this.authService.logout();
//   }

// }
