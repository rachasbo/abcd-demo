import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesAutocompleteComponent } from './sales-autocomplete.component';

describe('SalesAutocompleteComponent', () => {
  let component: SalesAutocompleteComponent;
  let fixture: ComponentFixture<SalesAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
