import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import * as moment from 'moment';
import { of, Subject } from 'rxjs';

@Component({
  selector: 'app-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent implements OnInit, OnChanges {

  @Input('value') value: string;
  @Input() inputModel: any;
  @Input('format') format: string;
  @Input('minTime') minTime: Date;
  @Input('maxTime') maxTime: Date;
  @Input('disabled') disabled: boolean;
  @Input('placeholder') label: string;
  @Input('type') type: string;
  @Input('required') required: boolean;
  @Input('errorMessage') errorMessage: String;

  @Output('onValueUpdate') onValueUpdate: EventEmitter<number> = new EventEmitter<number>();
  @Output() inputModelChange = new EventEmitter<string>();

  form: FormGroup;
  time;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    // console.log(this.minTime)

    if (this.required) {
      if (this.label != '') {
        this.label = this.label + "*"
      }

      this.form = this.formBuilder.group({
        inputField: new FormControl('', [Validators.required])
      });
    } else {
      this.form = this.formBuilder.group({
        inputField: new FormControl('')
      });
    }
    setTimeout(() => {
      if (this.disabled === true) {

        this.form.get('inputField').disable();
      } else {
        this.form.get('inputField').enable();
      }
    }, 500);

    // async time
    of(this.time).subscribe(value => {
      this.time = value;
      if (this.value) {
        console.log('in value',this.value)
        let pipeValue = moment(this.value, ["HH:mm", "YYYY-MM-DDTHH:mm"]).format('HH:mm');
        this.value = pipeValue;
        this.form.controls.inputField.patchValue(this.value);
      }
      var event: any = {};
      event.value = this.value;
      // console.log('in time sub', event.value, this.value)
    });
  }

  ngOnChanges(value: any) {
    // console.log(value);
    if (value.value) {
      if (this.required) {
        this.form = this.formBuilder.group({
          inputField: [this.value, [Validators.required]]
        })
      } else {
        this.form = this.formBuilder.group({
          inputField: [this.value]
        })
      }

      if (this.disabled === true) {
        this.form.get('inputField').disable();
      } else {
        this.form.get('inputField').enable();
      }
    }

    if (value.disabled) {
      this.disabled = value.disabled.currentValue;
      if (this.disabled === true) {
        this.form.get('inputField').disable();
      } else {
        this.form.get('inputField').enable();
      }
    }

    // async time
    of(this.time).subscribe(value => {
      this.time = value;
      if (this.value) {
        console.log('in value',this.value)
        let pipeValue = moment(this.value, ["HH:mm", "YYYY-MM-DDTHH:mm"]).format('HH:mm');
        this.value = pipeValue;
        this.form.controls.inputField.patchValue(this.value);
      }
      var event: any = {};
      event.value = this.value;
      // console.log('in time sub', event.value, this.value)
    });
  }

  somethingChanged(event: any) {
    console.log(event, this.inputModel)
    // this.onValueUpdate.emit(event.target.value);
    this.inputModelChange.emit(this.inputModel);
  }

  onDateChange(date: any) {
    console.log("Moment:", moment(date.value).format('YYYY-MM-DD'));
    // let day = date.value.getDate();
    // let month = date.value.getMonth() + 1;
    // let year = date.value.getFullYear();
    // console.log(`${year}-${month}-${day}`);

    // this.form.get('inputField').setValue(moment(date.value).format('YYYY-MM-DD'));

    // this.dateModelChange.emit(moment(date.value).format('YYYY-MM-DD'));

    // this.onValue.emit({
    //   value: moment(date.value).format('YYYY-MM-DD'),
    //   label: ''
    // });
  }

  checkValidate() {
    this.form.get('inputField').markAsTouched();
    if (this.required) {
      return !this.form.get('inputField').hasError('required');
    }
  }
}
