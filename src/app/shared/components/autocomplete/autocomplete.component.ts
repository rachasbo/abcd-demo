import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { Service } from 'src/app/shared/service';
import { startWith, map } from 'rxjs/operators';
import { text } from '@angular/core/src/render3';

export interface IOption {
  label: string;
  value: number;
}

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit, OnChanges {

  @Input('url') url: string;
  @Input('additionalParams') additionalParams: string;

  @Input("defaultValue") defaultValue: number;
  @Input('errorMessage') errorMessage: string;
  @Input('placeholder') placeholder: string;
  //@Input('label') label: string;

  @Input('id') id: string;
  @Input('required') required: boolean;
  @Input('disabled') disabled: boolean;
  @Input('enableClearBtn') enableClearBtn: boolean = true;
  @Input('filterMode') filterMode: string = 'left'; // left, any
  @Input('limit') limit: number = 50;

  @Input('childsMarkAsTouchedTrigger') childsMarkAsTouchedTrigger: Subject<boolean>;
  //@Input('validator') validator: Boolean;

  @Output('onValueUpdate') onValueUpdate = new EventEmitter();

  isDataLoaded: boolean = false;
  form: FormGroup;
  // showLoading:boolean = false;
  options: IOption[] = [];
  filteredOptions: Observable<IOption[]>;
  minInputLength: number = 3;
  skipBlur: boolean = false;
  disabled_option = false;


  constructor(
    private builder: FormBuilder,
    private dataService: Service
  ) {

  }

  ngOnInit() {
    /*
    if (this.required && this.placeholder && this.placeholder!='') {
      this.placeholder = this.placeholder + "*";
    }
    */
    this.updatePlaceHolder();

    this.createFormGroup();

    if (this.childsMarkAsTouchedTrigger) {
      this.childsMarkAsTouchedTrigger.subscribe(() => {
        this.form.get('inputField').markAsTouched();
      })
    }
  }

  ngOnChanges(value: any) {
    /*
    console.log(value);
    console.log('URL - previousValue', value.url.previousValue);
    console.log('URL - currentValue', value.url.currentValue);
    console.log('additionalParams - previousValue', value.additionalParams.previousValue);
    console.log('additionalParams - currentValue', value.additionalParams.currentValue);
    */

    if (value.url && value.url.previousValue != value.url.currentValue) {

      this.isDataLoaded = false;
      this.loadOptions();
    } else if (value.additionalParams && value.additionalParams.previousValue != value.additionalParams.currentValue) {

      this.isDataLoaded = false;
      this.loadOptions();
    } else if (value.defaultValue && value.defaultValue.previousValue != value.defaultValue.currentValue) {

      // setTimeout(() => {
      // console.log("value.defaultValue.currentValue",value.defaultValue.currentValue);
      // this.loadOptions();
      if (this.isDataLoaded) {
        this.form.get('inputField').setValue(this.getOptionByValue(value.defaultValue.currentValue));
      } else {
        this.form.get('inputField').setValue({ label: "Loading", value: value.defaultValue.currentValue });
      }
      // this.form.get('inputField').setValue(this.getOptionByValue(value.defaultValue.currentValue));



      this.onValueChange(true);
      // },5000);
    } else if (value.required && value.required.previousValue != value.required.currentValue) {
      this.updatePlaceHolder();
    }

    //เพิ่มใหม่เรื่อง Disabled
    if (value.disabled) {
      if (value.disabled.previousValue != value.disabled.currentValue) {
        // console.log("test",this.disabled);

        this.createFormGroup();
      }
    }

  }

  private updatePlaceHolder() {
    if (this.placeholder && this.placeholder != '') {
      if (this.required && this.placeholder.indexOf('*') == -1) {
        this.placeholder = this.placeholder + '*';
      } else if (!this.required && this.placeholder.indexOf('*') > -1) {
        this.placeholder = this.placeholder.replace('*', '');
      }
    }
  }

  private createFormGroup() {
    this.loadOptions();
    let inputParams = [];

    inputParams.push({
      value: this.getOptionByValue(this.defaultValue),
      disabled: this.disabled
    });

    if (!this.disabled) {
      if (this.required) {
        // inputParams.push(Validators.required);
        inputParams.push(this.customValidator);
      }
    }


    this.form = this.builder.group({
      inputField: inputParams
    })
  }

  public customValidator(control: FormControl) {
    // if (control.value == '' || control.value == undefined || control.value == null || isNaN(control.value)) {
    if (!(control.value > 0 || control.value.value > 0)) {
      return {
        'required': true
      }
    }

    return null;
  }

  private loadOptions() {


    this.options = [];

    let url_path = this.url + "?";
    if (this.additionalParams) {
      url_path += "&" + this.additionalParams;
    }
    url_path += "&Limit=0";

    // console.log(url_path);
    this.dataService.sendGetRequest(url_path, this, function (main, response) {
      // console.log('loadOptions :', response);

      if (response.data != null) {
        if (response.data.length > 0) {
          // if (main.url == '/master/v1/Product/product-lv3') {   // temporary data transform for ProductLv3
          //   for (var i=0; i<response.data.length; i++) {
          //     main.options.push({
          //       value: response.data[i].value.productLevel3Id,
          //       label: response.data[i].label,
          //       isCustomise: response.data[i].value.isCustomize
          //     })

          //     /*
          //     if (response.data[i].value.productLevel3Id == 33) {
          //       console.log(response.data[i]);
          //       console.log({
          //         value: response.data[i].value.productLevel3Id,
          //         label: response.data[i].label,
          //         isCustomise: response.data[i].value.isCustomize
          //       });
          //     }
          //     */
          //   }
          // } else {
          //   main.options = response.data;
          // }

          main.options = response.data;
          // main.showLoading = false;
        }

        main.renderOptions();

        if (!main.isDataLoaded) {

          main.isDataLoaded = true;

          main.form.get('inputField').setValue(main.getOptionByValue(main.defaultValue));

          //console.log('defaultValue:', value.defaultValue.currentValue);
          //console.log('getOptionByValue:', this.getOptionByValue(value.defaultValue.currentValue));

        }
      }
    });
  }

  private renderOptions() {

    setTimeout(() => {
      this.filteredOptions = this.form.get('inputField').valueChanges.pipe(
        // startWith(this.defaultValue),
        startWith(this.getOptionByValue(this.defaultValue)),
        map(value => this.filtered(value))
      )

    }, 100);
  }

  private filtered(value: any): IOption[] {
    // console.log('filtered_value = ', value, typeof value);

    var filteredOptions: IOption[] = [];

    // console.log('value == undefined', value == undefined);
    // console.log('value == null', value == null);
    // console.log('value == ""', (typeof value === 'string' && value == ''));
    // console.log('value == 0', (typeof value === 'number' && value == 0));
    // console.log('value.value == 0', (typeof value === 'object' && value.value == 0));

    if (value.length >= this.minInputLength || this.minInputLength == 0) {
      if (value == undefined || value == null || (typeof value === 'string' && value == '') || (typeof value === 'number' && value == 0) || (typeof value === 'object' && value.value == 0)) {
        // console.log('step1');
        filteredOptions = this.options;

        // this.disabled_option = false;

        if (this.defaultValue > 0 && ((typeof value === 'string' && value == '') || (typeof value === 'object' && value.value == 0))) {
          // this.form.get('inputField').setValue(this.getOptionByValue(0));
          // this.onValueChange(true);
          this.defaultValue = 0;
        }
      } else {
        // console.log('step2');
        if (typeof value === 'string') {
          value = value.toLowerCase().trim();

          if (this.filterMode == 'any') {
            filteredOptions = this.options.filter(option => option.label && option.label.toLowerCase().includes(value));
          } else if (this.filterMode == 'left') {
            filteredOptions = this.options.filter(option => option.label && option.label.toLowerCase().indexOf(value) == 0);
          }
        } else if (typeof value === 'number') {
          filteredOptions = this.options.filter(option => option.value == value);
        } else if (typeof value === 'object') {
          filteredOptions = this.options.filter(option => option.value == value.value);
        }

        /*
        if (filteredOptions.length == 0) {
          filteredOptions = [{
            label: "No Matches",
            //value: ""
            value: 0
          }];
  
          this.disabled_option = true;
        } else {
          this.disabled_option = false;
        }
        */
      }

      if (filteredOptions.length == 0) {
        filteredOptions = [{
          label: "No Matches",
          // value: ""
          value: 0
        }];

        this.disabled_option = true;
      } else {
        filteredOptions = filteredOptions;
        // filteredOptions = filteredOptions.slice(0, 10);

        this.disabled_option = false;
      }

      if (this.limit > 0) {
        filteredOptions = filteredOptions.slice(0, this.limit);
      }
      // console.log('limit=', this.limit);
      // console.log('filteredOptions(length)=', filteredOptions.length);

      return filteredOptions;
    }
  }

  public getOptionByValue(value: number): IOption {
    let option: IOption = {
      label: '',
      value: 0
    };


    if (value > 0) {
      let filteredOptions = this.options.filter(option => option.value == value);

      if (filteredOptions.length > 0) {
        option = filteredOptions[0];
      }
    }

    return option;
  }

  public isSelected() {
    let selected = false;

    let value = this.form.get('inputField').value;
    // console.log('isSelected value = ', value);

    // if (value || value != undefined || value != null || !isNaN(value)) {
    if (value && value != undefined && value != null) {
      if (typeof value === 'string') {
        // if (value != '') selected = true;
      } else if (typeof value === 'number') {
        if (value > 0) selected = true;
      } else {
        if (value.value > 0) selected = true;
      }
    }

    return selected;
  }

  public displayFn(value: any): string {
    // console.log("displayFn value = ", value);
    if (value) {
      return typeof value === 'string' ? value : value.label;
    } else {
      return "";
    }
  }

  public checkValidate() {
    this.form.get('inputField').markAsTouched();
    return !this.form.get('inputField').hasError('required');
  }

  public onSelect() {
    this.skipBlur = true;
    this.onValueChange(true);
  }

  private onValueChange(save: boolean) {

    setTimeout(() => {
      let value = this.form.get('inputField').value as IOption;
      // console.log('selected_value', value);

      if (save) {
        this.defaultValue = value.value;
      }
      // console.log('defaultValue', this.defaultValue);

      this.skipBlur = false;

      // let result = value;
      // result['id'] = this.id;

      let result = {};
      for (let key in value) {
        result[key] = value[key];
      }
      result['id'] = this.id;
      // console.log("result auto",result);
      this.onValueUpdate.emit(result);

    }, 100);
  }

  public onBlur() {
    setTimeout(() => {
      if (!this.skipBlur) {
        this.form.get('inputField').setValue(this.getOptionByValue(this.defaultValue));
      }
    }, 300);
  }

  public onClear(event) {
    event.stopPropagation();

    this.form.get('inputField').setValue(this.getOptionByValue(0));

    this.onValueChange(true);
  }


  // form: FormGroup;

  // options: IOption[] = [

  // ];
  // temp_options: IOption[] = [

  // ];
  // filteredOptions: Observable<IOption[]>;

  // skipBlur: boolean = false;
  // disabled_option = false;


  // constructor(
  //   private builder: FormBuilder,
  //   private dataService: Service
  // ) { }

  // ngOnInit() {

  //   if (this.required && this.placeholder && this.placeholder!='') {
  //     this.placeholder = this.placeholder + "*";
  //   }

  //   setTimeout(() => {
  //     if (this.disabled === true) {

  //       this.form.get('inputField').disable();
  //     } else {
  //       this.form.get('inputField').enable();
  //     }
  //   },500);

  //   if (this.childsMarkAsTouchedTrigger) {
  //     this.childsMarkAsTouchedTrigger.subscribe(() => {
  //       this.form.get('inputField').markAsTouched();
  //     })
  //   }

  //   // this.getOption();

  //   this.getTempOption();
  // }

  // getTempOption() {
  //   // console.log("this.url",this.url);
  //   let url_path = this.url + "?Limit=0";

  //   this.dataService.sendGetRequest(url_path, this, function (main, data) {

  //     if (data != null) {
  //       main.temp_options = data.data;

  //       if (main.options.length === 0) {
  //         main.options = [{ label: "No Matches", value: "" }];
  //         main.disabled_option = true;
  //       } else {
  //         main.disabled_option = false;
  //       }

  //       if (main.required) {
  //         main.form = main.builder.group({
  //           inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
  //         })
  //       } else {

  //         main.form = main.builder.group({
  //           inputField: [main.getOptionByValue(main.defaultValue)]
  //         })
  //       }

  //       // console.log("Temp",main.temp_options );
  //     }
  //   });
  // }

  // getOption() {
  //   // console.log("this.url",this.url);
  //   let url_path = this.url + "?Limit=10";
  //   this.dataService.sendGetRequest(url_path, this, function (main, data) {

  //     if (data != null) {
  //       main.options = data.data;

  //       if (main.options.length === 0) {
  //         main.options = [{ label: "No Matches", value: "" }];
  //         main.disabled_option = true;
  //       } else {
  //         main.disabled_option = false;
  //       }

  //       // console.log("Temp",main.temp_options );
  //     }
  //   });
  // }


  // callLabel() {
  //   setTimeout(() => {
  //     this.filteredOptions = this.form.get('inputField').valueChanges.pipe(
  //       startWith(this.defaultValue),
  //       //map(value => typeof value === 'string' ? value : value.value),
  //       map(value => {
  //         if (typeof value === 'string') {
  //           return value;
  //         } else {
  //           if (value != null && value != undefined) {
  //             return value.value;
  //           }
  //         }
  //       }),
  //       map(label => label ? this.filterOptions(label) : this.options)
  //     )
  //   }, 100);
  // }


  // ngOnChanges(value: any) {
  //   // console.log("Test",this.temp_options);
  //   // console.log("onChange",this.id,value);
  //   if (value.url) {
  //     // console.log("in,url");
  //     // value.additionalParams

  //     // let url_path = this.url + "?TextSearch="+value+"&Limit=10";
  //     this.url = value.url.currentValue;
  //     // console.log("url",this.url)
  //     // this.getTempOption();
  //     let url_path = "";
  //     if (this.additionalParams) {
  //       url_path = this.url + "?Limit=10&" + this.additionalParams;
  //     } else {
  //       url_path = this.url + "?Limit=10";
  //     }

  //     // console.log("url path",url_path);
  //     let Main = this;
  //     this.dataService.sendGetRequest(url_path, this, function (main, data) {

  //       if (data != null) {
  //         main.options = data.data;

  //         if (main.options.length === 0) {
  //           main.options = [{ label: "No Matches", value: "" }];
  //           main.disabled_option = true;
  //         } else {
  //           main.disabled_option = false;
  //         }
  //         if (main.disabled === true) {
  //           main.form.get('inputField').disable();
  //         }


  //         main.callLabel();

  //         if (main.required) {
  //           main.form = main.builder.group({
  //             inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
  //           })
  //         } else {

  //           main.form = main.builder.group({
  //             inputField: [main.getOptionByValue(main.defaultValue)]
  //           })
  //         }


  //       }
  //     });
  //   }
  //   // console.log("value.defaultValue",value.defaultValue);
  //   if (value.defaultValue !== undefined) {
  //     // console.log("in,defaultValue",value.defaultValue.currentValue);
  //     // console.log("innn 2",this.id,value.defaultValue.currentValue);
  //     this.defaultValue = value.defaultValue.currentValue;

  //     // if(value.defaultValue.previousValue === undefined){

  //     //   this.getTempOption();

  //     // }

  //     if (this.required) {
  //       this.form = this.builder.group({
  //         inputField: [this.getOptionByValue(this.defaultValue), Validators.required]
  //       })
  //     } else {
  //       this.form = this.builder.group({
  //         inputField: [this.getOptionByValue(this.defaultValue)]
  //       })
  //     }


  //     let url_path = "";

  //     if (value.defaultValue.currentValue == "" || value.defaultValue.currentValue == 0) {
  //       // let url_path = this.url + "?Limit=10";
  //       // console.log("")

  //       if (this.additionalParams) {
  //         url_path = this.url + "?Limit=10&" + this.additionalParams;
  //       } else {
  //         url_path = this.url + "?Limit=10";
  //       }


  //       this.dataService.sendGetRequest(url_path, this, function (main, data) {
  //         // console.log("test1",value,main.url);
  //         if (data != null) {
  //           main.options = data.data;

  //           if (main.options.length === 0) {
  //             main.options = [{ label: "No Matches", value: "" }];
  //             main.disabled_option = true;
  //           } else {
  //             main.disabled_option = false;
  //           }
  //           // console.log("Data",data);
  //           // if (main.required) {
  //           //   main.form = main.builder.group({
  //           //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
  //           //   })
  //           // } else {

  //           //   main.form = main.builder.group({
  //           //     inputField: [main.getOptionByValue(main.defaultValue)]
  //           //   })
  //           // }

  //           main.callLabel();
  //           // return main.options;
  //         }

  //       });


  //     } else {
  //       // console.log("value.defaultValue.currentValue",value.defaultValue.currentValue);
  //       let label = this.getOptionLabelByValue(value.defaultValue.currentValue);
  //       // let label = "test";
  //       // console.log("label",label);
  //       if (this.additionalParams) {
  //         url_path = this.url + "?TextSearch=" + encodeURIComponent(label) + "&Limit=10&" + this.additionalParams;
  //       } else {
  //         url_path = this.url + "?TextSearch=" + encodeURIComponent(label) + "&Limit=10";
  //       }


  //       this.dataService.sendGetRequest(url_path, this, function (main, data) {
  //         // console.log("test1",value,main.url);
  //         if (data != null) {
  //           main.options = data.data;
  //           // console.log("Data",data);
  //           // if (main.required) {
  //           //   main.form = main.builder.group({
  //           //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
  //           //   })
  //           // } else {

  //           //   main.form = main.builder.group({
  //           //     inputField: [main.getOptionByValue(main.defaultValue)]
  //           //   })
  //           // }

  //           main.callLabel();
  //           // return main.options;
  //         }

  //       });

  //       // this.callLabel();
  //     }


  //   }
  //   // this.filteredOptions = this.form.get('inputField').valueChanges
  //   //   .pipe(
  //   //     startWith(this.defaultValue),
  //   //     map(value => typeof value === 'string' ? value : value.value),
  //   //     map(label => label ? this.filterOptions(label) : this.options)
  //   //   )




  //   if (value.additionalParams) {

  //     let url_path = "";
  //     this.additionalParams = value.additionalParams.currentValue;
  //     // console.log("additionalParams",this.additionalParams)
  //     // let label = "test";
  //     // console.log("label",label);
  //     // this.getTempOption();
  //     let input_value = this.form.get('inputField').value;
  //     // console.log("input_value",input_value);
  //     if (input_value === "" || input_value === undefined || input_value === null) {

  //       if (this.additionalParams) {
  //         url_path = this.url + "?Limit=10&" + this.additionalParams;
  //       } else {
  //         url_path = this.url + "?Limit=10";
  //       }

  //     } else {
  //       // console.log("input_value",input_value);

  //       if (this.additionalParams) {
  //         url_path = this.url + "?TextSearch=" + encodeURIComponent(input_value.label) + "&Limit=10&" + this.additionalParams;
  //       } else {
  //         url_path = this.url + "?TextSearch=" + encodeURIComponent(input_value.label) + "&Limit=10";
  //       }
  //     }

  //     this.dataService.sendGetRequest(url_path, this, function (main, data) {
  //       // console.log("test1",value,main.url);
  //       if (data != null) {
  //         main.options = data.data;
  //         if (main.options.length === 0) {
  //           main.options = [{ label: "No Matches", value: "" }];
  //           main.disabled_option = true;
  //         } else {
  //           main.disabled_option = false;
  //         }
  //         // console.log("Data",data);
  //         // if (main.required) {
  //         //   main.form = main.builder.group({
  //         //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
  //         //   })
  //         // } else {

  //         //   main.form = main.builder.group({
  //         //     inputField: [main.getOptionByValue(main.defaultValue)]
  //         //   })
  //         // }

  //         main.callLabel();
  //         // return main.options;
  //       }

  //     });

  //   }



  //   if (value.disabled) {

  //     this.disabled = value.disabled.currentValue;

  //     // setTimeout(() => {
  //       if (this.disabled === true) {

  //         this.form.get('inputField').disable();
  //       } else {
  //         this.form.get('inputField').enable();
  //       }
  //     // },500);

  //   }



  // }
  // private filterOptions(value: any): IOption[] {
  //   // console.log("in filter",);


  //   if (typeof value === 'string') {
  //     let url_path = "";
  //     if (this.additionalParams) {
  //       url_path = this.url + "?TextSearch=" + encodeURIComponent(value) + "&Limit=10&" + this.additionalParams;
  //     } else {
  //       url_path = this.url + "?TextSearch=" + encodeURIComponent(value) + "&Limit=10";
  //     }

  //     // let url_path = this.url + "?TextSearch="+value+"&Limit=10";
  //     this.dataService.sendGetRequest(url_path, this, function (main, data) {
  //       // console.log("test1",value,main.url);
  //       if (data != null) {
  //         main.options = data.data;
  //         if (main.options.length === 0) {
  //           main.options = [{ label: "No Matches", value: "" }];
  //           main.disabled_option = true;
  //         } else {
  //           main.disabled_option = false;
  //         }
  //         // console.log("Data",data);
  //         // if (main.required) {
  //         //   main.form = main.builder.group({
  //         //     inputField: [main.getOptionByValue(main.defaultValue), Validators.required]
  //         //   })
  //         // } else {

  //         //   main.form = main.builder.group({
  //         //     inputField: [main.getOptionByValue(main.defaultValue)]
  //         //   })
  //         // }

  //         main.callLabel();
  //         // return main.options;
  //       }

  //     });

  //   }




  //   // let filterValue = '';

  //   // if (typeof value === 'string') {
  //   //   filterValue = value;
  //   // } else if (typeof value === 'number') {
  //   //   for (var i = 0; i < this.options.length; i++) {
  //   //     if (this.options[i].value == value) {
  //   //       filterValue = this.options[i].label;
  //   //     }
  //   //   }
  //   // } else {
  //   //   let option: IOption = value;

  //   //   filterValue = option.label;
  //   // }

  //   // filterValue = filterValue.trim().toLowerCase();
  //   // console.log("this.options",this.options.filter(option => option.label.toLowerCase().includes(filterValue)));
  //   // // console.log("this.option",this.options.filter(option => option.label.toLowerCase().includes(filterValue)));
  //   // return this.options.filter(option => option.label.toLowerCase().includes(filterValue));
  //   // // console.log("test2");


  //   // let optiontest:IOption[] = [{value: 1, label: "AP (Thailand) Public Company Limited/บริษัท เอพี (ไทยแลนด์) จำกัด"}];
  //   return this.options;
  // }

  // private onSelect() {
  //   this.skipBlur = true;
  //   this.onValueChange(true);
  // }

  // private onValueChange(save: boolean) {


  //   setTimeout(() => {
  //     let value = 0;
  //     let input_value = this.form.get('inputField').value;
  //     // console.log("Test",input_value);
  //     if (typeof input_value === 'string') {
  //       value = 0;
  //     } else {
  //       let option: IOption = input_value;
  //       value = option.value;
  //     }


  //     // console.log("value",value)
  //     if (save) {
  //       this.defaultValue = value;
  //     }

  //     this.skipBlur = false;

  //     let result = {
  //       label: input_value.label,
  //       value: value,
  //       id: this.id
  //     }
  //     this.onValueUpdate.emit(result);

  //   }, 100);
  // }

  // onBlur() {
  //   setTimeout(() => {
  //     if (!this.skipBlur) {

  //       if (this.defaultValue > 0) {
  //         this.form.get('inputField').patchValue(this.getOptionByValue(this.defaultValue));

  //       } else {
  //         this.form.get('inputField').patchValue('');

  //         let url_path = "";
  //         let input_value = this.form.get('inputField').value;
  //         if (input_value === "" || input_value === undefined || input_value === null) {

  //           if (this.additionalParams) {
  //             url_path = this.url + "?Limit=10&" + this.additionalParams;
  //           } else {
  //             url_path = this.url + "?Limit=10";
  //           }

  //         }

  //         this.dataService.sendGetRequest(url_path, this, function (main, data) {
  //           if (data != null) {
  //             main.options = data.data;
  //             if (main.options.length === 0) {
  //               main.options = [{ label: "No Matches", value: "" }];
  //               main.disabled_option = true;
  //             } else {
  //               main.disabled_option = false;
  //             }

  //             main.callLabel();
  //           }

  //         });

  //       }
  //     }
  //   }, 300);
  // }

  // public getOptionLabelByValue(value: number) {
  //   let label = '';
  //   // console.log('getOptionLabelByValue',this.temp_options,this.options);
  //   for (var i = 0; i < this.temp_options.length; i++) {
  //     if (this.temp_options[i].value == this.defaultValue) {
  //       label = this.temp_options[i].label;
  //     }
  //   }

  //   return label;
  // }

  // public getOptionByValue(value: number): IOption {

  //   let option: IOption;
  //   // console.log("getOptionByValue",this.options);
  //   for (var i = 0; i < this.temp_options.length; i++) {

  //     if (this.temp_options[i].value == this.defaultValue) {
  //       option = this.temp_options[i];
  //     }
  //   }

  //   return option;
  // }

  // private onClear() {
  //   this.form.get('inputField').patchValue('');

  //   let url_path = "";
  //   let input_value = this.form.get('inputField').value;
  //   if (input_value === "" || input_value === undefined || input_value === null) {

  //     if (this.additionalParams) {
  //       url_path = this.url + "?Limit=10&" + this.additionalParams;
  //     } else {
  //       url_path = this.url + "?Limit=10";
  //     }

  //   }

  //   this.dataService.sendGetRequest(url_path, this, function (main, data) {
  //     if (data != null) {
  //       main.options = data.data;
  //       if (main.options.length === 0) {
  //         main.options = [{ label: "No Matches", value: "" }];
  //         main.disabled_option = true;
  //       } else {
  //         main.disabled_option = false;
  //       }

  //       main.callLabel();
  //     }

  //   });


  //   this.onValueChange(true);
  // }

  // displayFn(value: any): string {
  //   // console.log("value",value);
  //   if (value) {
  //     return typeof value === 'string' ? value : value.label;
  //   } else {
  //     return "";
  //   }
  // }

  // checkValidate() {
  //   this.form.get('inputField').markAsTouched();
  //   return !this.form.get('inputField').hasError('required');
  // }

}
