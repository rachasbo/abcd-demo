// import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from "@angular/material";
import { NativeDateAdapter } from "@angular/material";
import * as moment from 'moment'; // add this 1 of 4

export class AppDateAdapter extends NativeDateAdapter {

    // parse(value: any): Date | null {
    //     if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
    //         const str = value.split('/');
    //         const year = Number(str[2]);
    //         const month = Number(str[1]) - 1;
    //         const date = Number(str[0]);
    //         return new Date(year, month, date);
    //     }
    //     const timestamp = typeof value === 'number' ? value : Date.parse(value);
    //     return isNaN(timestamp) ? null : new Date(timestamp);
    // }
    format(date: Date, displayFormat: any): string {
        if (displayFormat == "input") {
            //console.log(moment(date).format('D MMM YY'));
            let acDate = moment(date);
            acDate.set('year', acDate.year());
            return moment(acDate).format('YYYY/MM/DD');
        } else {
            let acDate = moment(new Date());
            let dow = moment(new Date).set('day', acDate.day());
            acDate.set('year', acDate.year());
            return dow.format('dddd, ') + acDate.format('YYYY MM DD');
        }
    }

    // private _to2digit(n: number) {
    //     return ('00' + n).slice(-2);
    // }
}

export const APP_DATE_FORMATS =
{
    parse: {
        dateInput: 'input',
    },
    display: {
        dateInput: 'input',
        monthYearLabel: 'DD/MMM/YY',
        dateA11yLabel: 'DD/MMM/YY',
        monthYearA11yLabel: 'DD/MMM/YY',
    }
}