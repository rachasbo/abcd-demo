import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyFreeFormComponent } from './company-freeform.component';


describe('CompanyFreeFormComponent', () => {
  let component: CompanyFreeFormComponent;
  let fixture: ComponentFixture<CompanyFreeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyFreeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyFreeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
