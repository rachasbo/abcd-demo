import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Service } from '../../service';
import { MatDialog } from '@angular/material';
import { DialogWarningComponent } from '../dialog-warning/dialog-warning.component';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';

/*
export interface ICompanyFreeForm {
  tax_id: string;
  company_th: string;
  company_en: string;
  billing: IAddressBilling;
  tax: IAddressTax;
}
interface IAddressBilling {
  type: string;
  branch_name: string;
  address_th: string;
  address_en: string;
  country_th: string;
  country_en: string;
  province_th: string;
  province_en: string;
  district_th: string;
  district_en: string;
  subdistrict_th: string;
  subdistrict_en: string;
  zipcode: string;
}
interface IAddressTax {
  address_th: string;
  address_en: string;
  country_th: string;
  country_en: string;
  province_th: string;
  province_en: string;
  district_th: string;
  district_en: string;
  subdistrict_th: string;
  subdistrict_en: string;
  zipcode: string;
}
*/

export interface ICompanyFreeFormResult {
  action: string;
  type: string;
  data: ICompanyFreeFormAddr;
}
export interface ICompanyFreeFormAddr {
  isCreatedAddress: boolean;
  customerId: number;
  taxId: string;
  customerNameEn: string;
  customerNameTh: string;
  isHeadOffice: boolean;
  // branchNo: number;
  branchNo: string;
  billAddrTh: string;
  billAddrEn: string;
  billSubDistrictNameEn: string;
  billSubDistrictNameTh: string;
  billDistrictNameEn: string;
  billDistrictNameTh: string;
  billProvinceNameEn: string;
  billProvinceNameTh: string;
  billCountryNameEn: string;
  billCountryNameTh: string;
  billZipcode: string;
  taxAddrEn: string;
  taxAddrTh: string;
  taxSubDistrictNameEn: string;
  taxSubDistrictNameTh: string;
  taxDistrictNameEn: string;
  taxDistrictNameTh: string;
  taxProvinceNameEn: string;
  taxProvinceNameTh: string;
  taxCountryNameEn: string;
  taxCountryNameTh: string;
  taxZipcode: string;
  files: ICompanyFreeFormFile[];
}

export interface ICompanyFreeFormFile {
  fileId: number;
  fileName: string;
}

@Component({
  selector: 'app-company-freeform',
  templateUrl: './company-freeform.component.html',
  styleUrls: ['./company-freeform.component.scss']
})
export class CompanyFreeFormComponent implements OnInit {

  address: ICompanyFreeFormAddr = {
    isCreatedAddress: false,
    customerId: 0,
    taxId: '',
    customerNameEn: '',
    customerNameTh: '',
    isHeadOffice: true,
    // branchNo: 0,
    branchNo: '',
    billAddrEn: '',
    billAddrTh: '',
    billSubDistrictNameEn: '',
    billSubDistrictNameTh: '',
    billDistrictNameEn: '',
    billDistrictNameTh: '',
    billProvinceNameEn: '',
    billProvinceNameTh: '',
    billCountryNameEn: '',
    billCountryNameTh: '',
    billZipcode: '',
    taxAddrEn: '',
    taxAddrTh: '',
    taxSubDistrictNameEn: '',
    taxSubDistrictNameTh: '',
    taxDistrictNameEn: '',
    taxDistrictNameTh: '',
    taxProvinceNameEn: '',
    taxProvinceNameTh: '',
    taxCountryNameEn: '',
    taxCountryNameTh: '',
    taxZipcode: '',
    files: []
  }

  public onClose: BehaviorSubject<ICompanyFreeFormResult> = new BehaviorSubject<ICompanyFreeFormResult>({
    action: '',
    type: '',
    data: this.address
  });

  type: string;
  title: string;
  maxSize: number = 5 * 1024 * 1024// byte
  //enableBranch: boolean;
  //enableUploadFile: boolean;

  /*
  address: ICompanyFreeForm = {
    tax_id: '',
    company_th: '',
    company_en: '',
    billing: {
      type: 'head',
      branch_name: '',
      address_th: '',
      address_en: '',
      country_th: '',
      country_en: '',
      province_th: '',
      province_en: '',
      district_th: '',
      district_en: '',
      subdistrict_th: '',
      subdistrict_en: '',
      zipcode: ''
    },
    tax: {
      address_th: '',
      address_en: '',
      country_th: '',
      country_en: '',
      province_th: '',
      province_en: '',
      district_th: '',
      district_en: '',
      subdistrict_th: '',
      subdistrict_en: '',
      zipcode: ''
    }
  };
  */

  form: FormGroup;

  enableUploadFile: boolean;

  canUpload = true;
  canDownload = true;
  canDelete = true;

  uploadUrl: string = '';

  isLoading = false;

  constructor(
    // private service: Service,
    private builder: FormBuilder,
    public dialogError: MatDialog
  ) {

  }

  ngOnInit() {

  }

  public setData(type: string, title: string, address: ICompanyFreeFormAddr, enableUploadFile: boolean, uploadUrl: string) {
    this.type = type;
    this.title = title;
    this.address = address;
    //this.enableBranch = enableBranch;
    this.enableUploadFile = enableUploadFile;
    this.uploadUrl = uploadUrl;

    this.createFormData();

    console.log('address:', this.address);
  }

  public setLoading(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  public createFormData() {
    /*
    this.form = this.builder.group({
      tax_id: [this.address.tax_id, Validators.required],
      company_th: [this.address.company_th, Validators.required],
      company_en: [this.address.company_en, Validators.required],
      billing: this.builder.group({
        type: [this.address.billing.type, Validators.required],
        branch_name: [this.address.billing.branch_name, Validators.required],
        address_th: [this.address.billing.address_th, Validators.required],
        address_en: [this.address.billing.address_en, Validators.required],
        country_th: [this.address.billing.country_th, Validators.required],
        country_en: [this.address.billing.country_en, Validators.required],
        province_th: [this.address.billing.province_th, Validators.required],
        province_en: [this.address.billing.province_en, Validators.required],
        district_th: [this.address.billing.district_th, Validators.required],
        district_en: [this.address.billing.district_en, Validators.required],
        subdistrict_th: [this.address.billing.subdistrict_th, Validators.required],
        subdistrict_en: [this.address.billing.subdistrict_en, Validators.required],
        zipcode: [this.address.billing.zipcode, Validators.required]
      }),
      tax: this.builder.group({
        address_th: [this.address.tax.address_th, Validators.required],
        address_en: [this.address.tax.address_en, Validators.required],
        country_th: [this.address.tax.country_th, Validators.required],
        country_en: [this.address.tax.country_en, Validators.required],
        province_th: [this.address.tax.province_th, Validators.required],
        province_en: [this.address.tax.province_en, Validators.required],
        district_th: [this.address.tax.district_th, Validators.required],
        district_en: [this.address.tax.district_en, Validators.required],
        subdistrict_th: [this.address.tax.subdistrict_th, Validators.required],
        subdistrict_en: [this.address.tax.subdistrict_en, Validators.required],
        zipcode: [this.address.tax.zipcode, Validators.required]
      })
    });
    */

    this.form = this.builder.group({
      // taxId: [this.address.taxId, Validators.required],
      taxId: [this.address.taxId],
      // customerNameEn: [this.address.customerNameEn, Validators.required],
      customerNameEn: [this.address.customerNameEn],
      // customerNameTh: [this.address.customerNameTh, Validators.required],
      customerNameTh: [this.address.customerNameTh],
      isHeadOffice: [(this.address.isHeadOffice ? '1' : '0'), Validators.required],
      branchNo: [this.address.branchNo],
      billAddrEn: [this.address.billAddrEn],
      billAddrTh: [this.address.billAddrTh],
      billSubDistrictNameEn: [this.address.billSubDistrictNameEn],
      billSubDistrictNameTh: [this.address.billSubDistrictNameTh],
      billDistrictNameEn: [this.address.billDistrictNameEn],
      billDistrictNameTh: [this.address.billDistrictNameTh],
      billProvinceNameEn: [this.address.billProvinceNameEn],
      billProvinceNameTh: [this.address.billProvinceNameTh],
      billCountryNameEn: [this.address.billCountryNameEn],
      billCountryNameTh: [this.address.billCountryNameTh],
      billZipcode: [this.address.billZipcode],
      taxAddrEn: [this.address.taxAddrEn],
      taxAddrTh: [this.address.taxAddrTh],
      taxSubDistrictNameEn: [this.address.taxSubDistrictNameEn],
      taxSubDistrictNameTh: [this.address.taxSubDistrictNameTh],
      taxDistrictNameEn: [this.address.taxDistrictNameEn],
      taxDistrictNameTh: [this.address.taxDistrictNameTh],
      taxProvinceNameEn: [this.address.taxProvinceNameEn],
      taxProvinceNameTh: [this.address.taxProvinceNameTh],
      taxCountryNameEn: [this.address.taxCountryNameEn],
      taxCountryNameTh: [this.address.taxCountryNameTh],
      taxZipcode: [this.address.taxZipcode],
    });

    if (this.address.customerId == 0) {
      this.form.get('taxId').setValidators(Validators.required);
      this.form.get('customerNameEn').setValidators(Validators.required);
      this.form.get('customerNameTh').setValidators(Validators.required);
    }

    this.branchNo_toggleValidator();
    this.form.get('isHeadOffice').valueChanges.subscribe(value => {
      this.branchNo_toggleValidator();
    });
  }

  private branchNo_toggleValidator() {
    if (this.form.get('isHeadOffice').value === '0') {
      // this.form.get('branchNo').setValidators(this.branchNoValidator);
      this.form.get('branchNo').setValidators([this.branchNoValidator, Validators.required]);
      // this.form.get('branchNo').setValidators(Validators.pattern('^[1-9]([0-9]{1,4})+$'));
    } else {
      this.form.get('branchNo').setValidators(null);
    }

    this.form.get('branchNo').updateValueAndValidity();
  }

  private branchNoValidator(control: FormControl) {
    // let pattern = new RegExp('^[1-9]([0-9]{1,4})?$');
    let pattern = new RegExp('^[0-9]{1,5}$');

    if (!(control.value > 0) || !pattern.test(control.value.toString())) {
      return {
        // 'required': true
        'invalided': true
      }
    }

    return null;
  }

  public onAddressChange(value) {

  }

  public onShowLoading(isShowLoading) {
    if (isShowLoading) {
      this.isLoading = true;
    } else {
      this.isLoading = false;
    }
  }

  public onCancel() {
    this.dialogError.open(DialogConfirmComponent, {
      data: ""
    }).afterClosed().subscribe(result => {
      if (result == 1) {
        this.onClose.next({
          action: 'close',
          type: this.type,
          data: this.address
        });
      }
    });
  }

  public onSave() {
    if (this.form.get('isHeadOffice').value == '1') {
      this.form.get('branchNo').setValue(0);
    }

    if (this.form.invalid)  {
      console.log('form => invalid');

      for(let i in this.form.controls) {
        this.form.controls[i].markAsTouched();
      }
    } else if (this.enableUploadFile && this.address.customerId > 0 && this.address.files.length == 0) {
      console.log('form => invalid (require upload file)');

      // this.isRequiredFileUpload = true;

      this.dialogError.open(DialogWarningComponent, {
        data: {
          title: 'Warning',
          content: 'Please upload file before save.',
          okBtnText: 'OK',
          isOneButton: true
        }
      }).afterClosed().subscribe(result => {

      });
    } else {
      console.log('form => valid');

      let data = this.form.value;
      data.isCreatedAddress = this.address.isCreatedAddress;

      console.log(data);

      // this.onClose.next('save');
      this.onClose.next({
        action: 'save',
        type: this.type,
        data: data
      });
    }
  }

}
