import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-common-label',
  templateUrl: './common-label.component.html',
  styleUrls: ['./common-label.component.scss']
})
export class CommonLabelComponent implements OnInit {

  @Input('label') label:string;
  @Input('value') value:string;
  @Input('styleValue') styleValue:string;

  constructor() { }

  ngOnInit() {
    if(this.styleValue){
      this.styleValue = this.styleValue+" word-break";
    }else{
      this.styleValue = "word-break";
    }
    
  }

}
