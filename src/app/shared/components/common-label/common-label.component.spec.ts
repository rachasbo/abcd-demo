import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonLabelComponent } from './common-label.component';

describe('CommonLabelComponent', () => {
  let component: CommonLabelComponent;
  let fixture: ComponentFixture<CommonLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
