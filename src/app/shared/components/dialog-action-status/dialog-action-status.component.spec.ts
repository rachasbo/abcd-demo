import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionStatusComponent } from './dialog-action-status.component';

describe('DialogActionStatusComponent', () => {
  let component: DialogActionStatusComponent;
  let fixture: ComponentFixture<DialogActionStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
