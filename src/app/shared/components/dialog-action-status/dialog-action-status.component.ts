import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-action-status',
  templateUrl: './dialog-action-status.component.html',
  styleUrls: ['./dialog-action-status.component.scss']
})
export class DialogActionStatusComponent implements OnInit {
  public pass: boolean;
  public title: string;
  public description: string;
  constructor(
    public dialogRef: MatDialogRef<DialogActionStatusComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: IDialogActionStatusComponent) {
    this.title = this.data.title;
    this.description = this.data.description;
    this.pass = this.data.pass
  }
  ngOnInit() {
  }

  onSubmit(): void {
    this.dialogRef.close();
  }

}

type IDialogActionStatusComponent = {
  title: string;
  description: string;
  pass: boolean
}
