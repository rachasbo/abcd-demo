import {Component, OnInit , Input, Output,EventEmitter} from '@angular/core';
// import {FormControl} from '@angular/forms';

import { FormBuilder, FormGroup, FormArray, Validators, AbstractControl ,FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
// import { url } from 'inspector';

export interface User {
  label: string;
  value: number;
}

@Component({
  selector: 'app-common-autocomplete',
  templateUrl: './common-autocomplete.component.html',
  styleUrls: ['./common-autocomplete.component.scss']
})
export class CommonAutocompleteComponent implements OnInit {
  myControl = new FormControl();
  options: User[] = [
    {label: 'Mary' , value: 1},
    {label: 'Shelley' , value: 2},
    {label: 'Igor' , value: 3}
  ];
  value_input = "";
  filteredOptions: Observable<User[]>;
  form: FormGroup;
  // validator_input:boolean = true;
  // myControl = new FormControl('',[Validators.required])
  constructor(
    private builder:FormBuilder
  ) {
  }

  @Input('disabled') disabled:boolean;
  @Input('required') required:string;
  @Input('label') label:string;
  @Input('placeholder') placeholder:string;
  @Input('url') url:string;
  @Input('value') value_autocomplete;
  @Output('onValueUpdate') onValueUpdate : EventEmitter<number> = new EventEmitter<number>();
  @Input('errorMessage') errorMessage: String;
  ngOnInit() {
    this.form = this.builder.group({
       myControl: ['', Validators.required]
      // myControl: new FormControl({ value: '', disabled: true })
    })
  //   if(this.required){
  //   this.form = this.builder.group({
  //     myControl: ['',Validators.required],

  //   });
  // }else{
  //   this.form = this.builder.group({
  //     myControl: [''],

  //   });
  // }
    // this.form = new FormGroup({
    //   'myControl'
    // })
    // console.log("this.disabled",this.disabled)
    if(this.disabled){
      this.myControl.disable();
    }


    this.options.forEach(element => {
      if(element.value === this.value_autocomplete){
        console.log("value_autocomplete Default",element.label);

        this.value_input = element.label;
        this.myControl.setValue(element);
      }
    });


    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.label),
        map(label => label ? this._filter(label) : this.options.slice())
      );
  }


  customValidator(control: AbstractControl) {
    if (control.value == "") {
      return { required: true }
    }
  }

    SubmitValue(value){
    // console.log("SubmitValue");
  this.value_autocomplete = value;
  this.onValueUpdate.emit(this.value_autocomplete);
  }


  displayFn(user?: User): string | undefined {
    return user ? user.label : undefined;

  }

  onClear(){
    // console.log("test");
    this.value_autocomplete = 0;
    this.value_input = "";


    // this.myControl =
    // this.myControl.invalid;

  }

  public _filter(label: string): User[] {
    // console.log("test");
    this.value_input = label;

    const filterValue = label.toLowerCase();

    return this.options.filter(option => option.label.toLowerCase().indexOf(filterValue) === 0);
  }
  handleChange(value){
  this.value_input = value;
  }


  //1
  onBlur(){

    let some = false;
    let index_for = 0;
    this.options.forEach((element,index) => {
      if(element.label === this.value_input){
        some = true;
        index_for = index;
      }
    });

    if(!some){

      if(this.value_autocomplete === 0 ){
        this.value_input = "";
        // this.myControl.setValue("");
        // this.myControl.updateValueAndValidity();
      }else{
        this.options.forEach((item,index) => {
          if(item.value === this.value_autocomplete){

            this.value_input = item.label;
            // this.myControl.setValue(item);
          }
        });
      }


    //  this.SubmitValue(0);
    }else{
      // this.value_input = "";
      // this.SubmitValue(this.options[index_for].value);
    }

    console.log("onBlur",this.value_autocomplete,this.value_input);
    this.myControl.markAsTouched();
  }
}
