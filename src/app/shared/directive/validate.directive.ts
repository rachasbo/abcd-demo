import { Directive, HostBinding, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appValidate]'
})
export class ValidateDirective {

  @Input('appValidate') control: AbstractControl;
  @HostBinding('class') class: string = 'invalid-feedback';
  @HostBinding('innerText') text: string = '';

  errorMessage = {
    required: 'The field is required.',
    pattern: 'The field is not matched "{0}"',
    error: 'Test custom validate'
  }

  constructor() {
    
  }

  ngOnChanges() {
    if (!this.control) {
      return;
    }
    //console.log(this.control);

    this.text = this.getErrorMessage();
    this.control.valueChanges.subscribe(() => {
      this.text = this.getErrorMessage();
      //console.log(this.control.value);
    })
  }

  private getErrorMessage() {
    const control = this.control;

    if (control && control.invalid) {
      //console.log(control);

      //const errors = control.errors;
      //console.log(errors);

      //console.log(control.errors);

      const errorKey = Object.keys(control.errors)[0];
      //console.log(errorKey);

      //let errorMessage = this.errorMessage[errorKey]
      let errorMessage = '';

      switch (errorKey) {
        case 'pattern':
          errorMessage = this.errorMessage[errorKey].replace('{0}', control.errors.pattern.requiredPattern);
          break;
        case 'message':
          errorMessage = control.errors[errorKey]
          break;
        default:
          errorMessage = this.errorMessage[errorKey]
      }

      return errorMessage;
    }
  }

}
