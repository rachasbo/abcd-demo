import { CoreModel } from 'src/app/shared/models/core.model';

export class NavbarModel extends CoreModel{
    public menuId: number;
    public menuCode: string;
    public menuName: string;
    public isIndex: boolean;
    public isTap: boolean;
    public permission: string[]; 
    public child: ChildSubMenu[];
}

export class ChildSubMenu extends CoreModel{
    public menuId: number;
    public menuCode: string;
    public menuName: string;
    public isIndex: boolean;
    public isTap: boolean;
    public permission: string[]; 
}
