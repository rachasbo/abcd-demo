import { CoreModel } from './core.model';

export class UploadFileModel extends CoreModel {
}

export class Files extends CoreModel{
    public fileId: number = 0;
    public fileName: string = "";
}