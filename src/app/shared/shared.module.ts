import { NgxMatDatetimePickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { DateTimePickerComponent } from './components/date-time-picker/date-time-picker.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { Service } from './service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { Auth } from './auth';

import { ValidateDirective } from './directive/validate.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {MatBadgeModule} from '@angular/material/badge';
//import { NavbarComponent } from './components/navbar/navbar.component';

import { MatFormFieldModule, MatBadgeModule, MatAutocompleteModule, MatProgressSpinnerModule, MatSelectModule, MatOptionModule, MatCardModule, MatInputModule, MatIconModule, MatButtonModule, MatCheckboxModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatRadioModule, MatChipsModule } from '@angular/material';

import { MultiSelectComponent } from './components/multi-select/multi-select.component';
import { MultiSelectAutocompleteComponent } from './components/multi-select-autocomplete/multi-select-autocomplete.component';
import { CommonLabelComponent } from './components/common-label/common-label.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { DropdownNewComponent } from './components/dropdown-new/dropdown-new.component';
import { CommonAutocompleteComponent } from './components/common-autocomplete/common-autocomplete.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { SalesAutocompleteComponent } from './components/sales-autocomplete/sales-autocomplete.component';
import { AddressComponent } from './components/address/address.component';
import { DialogErrorComponent } from './components/dialog-error/dialog-error.component';
import { InputFieldComponent } from './components/input-field/input-field.component';
import { from } from 'rxjs';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { LoadingComponent } from './components/loading/loading.component';
import { LabelComponent } from './label/label.component';
import { ProductLv3AutocompleteComponent } from './components/product-lv3-autocomplete/product-lv3-autocomplete.component';
import { DialogSuccessComponent } from './components/dialog-success/dialog-success.component';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';
import { AddressPopupManageComponent } from './components/address-popup-manage/address-popup-manage.component';
import { AddressFreeFormComponent } from './components/address-freeform/address-freeform.component';
import { CompanyFreeFormComponent } from './components/company-freeform/company-freeform.component';
import { DialogDeleteComponent } from './components/dialog-delete/dialog-delete.component';
import { DialogInformComponent } from './components/dialog-inform/dialog-inform.component';
import { DialogWarningComponent } from './components/dialog-warning/dialog-warning.component';
import { TimepickerComponent } from './components/timepicker/timepicker.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { TextAreaComponent } from './components/text-area/text-area.component';
import { DialogDynamicConfirmComponent } from './components/dialog-dynamic-confirm/dialog-dynamic-confirm.component';
import { DialogActionStatusComponent } from './components/dialog-action-status/dialog-action-status.component';

@NgModule({
  declarations: [
    //NavbarComponent,
    ValidateDirective,
    MultiSelectComponent,
    MultiSelectAutocompleteComponent,
    CommonLabelComponent,
    DropdownComponent,
    DropdownNewComponent,
    CommonAutocompleteComponent,
    AutocompleteComponent,
    DatepickerComponent,
    SalesAutocompleteComponent,
    AddressComponent,
    InputFieldComponent,
    UploadFileComponent,
    LoadingComponent,
    LabelComponent,
    ProductLv3AutocompleteComponent,
    AddressPopupManageComponent,
    AddressFreeFormComponent,
    CompanyFreeFormComponent,
    TimepickerComponent,
    TextAreaComponent,
    DialogDynamicConfirmComponent,
    DateTimePickerComponent,
    DialogActionStatusComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatChipsModule,
    NgxMaterialTimepickerModule,
    NgxMatNativeDateModule,
    // NgxMatMomentModule,
    NgxMatDatetimePickerModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    ValidateDirective,
    CommonLabelComponent,
    DropdownComponent,
    DropdownNewComponent,
    MultiSelectComponent,
    MultiSelectAutocompleteComponent,
    CommonAutocompleteComponent,
    AutocompleteComponent,
    DatepickerComponent,
    AddressComponent,
    SalesAutocompleteComponent,
    InputFieldComponent,
    UploadFileComponent,
    LoadingComponent,
    LabelComponent,
    ProductLv3AutocompleteComponent,
    AddressPopupManageComponent,
    AddressFreeFormComponent,
    CompanyFreeFormComponent,
    TimepickerComponent,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCheckboxModule,
    MatRadioModule,
    TextFieldModule,
    TextAreaComponent,
    DateTimePickerComponent
  ],
  entryComponents: [
    DialogDynamicConfirmComponent,
    DialogActionStatusComponent
  ]
})
export class SharedModule { }
