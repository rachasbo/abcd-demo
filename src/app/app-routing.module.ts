import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './shared/auth-guard';

import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ChangePwdComponent } from './pages/change-pwd/change-pwd.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'dashboard',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'change_pwd',
    component: ChangePwdComponent,
    // canActivate : [AuthGuard]
  },
  {
    path: 'lead-demo',
    loadChildren: "./modules/lead-demo/lead-demo.module#LeadDemoModule",
    canActivate: [AuthGuard]
  },
  {
    path: 'config',
    loadChildren: "./modules/config/config.module#ConfigModule",
    canActivate: [AuthGuard]
  },
  {
    path: 'message',
    loadChildren: "./modules/message/message.module#MessageModule",
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    loadChildren: "./modules/administrator/administrator.module#AdministratorModule",
    canActivate: [AuthGuard]
  }
  /*
  {
    path: 'user',
    loadChildren: "./modules/user-management/user-management.module#UserManagementModule",
    canActivate : [AuthGuard]
  }
  */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
